-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(-- clock
       clk    : in filters_types.clk_system;
       -- reset
       rst    : in filters_types.rst_system;
       x      : in signed(7 downto 0);
       result : out signed(7 downto 0));
end;

architecture structural of topentity is
  signal result_0              : filters_types.tup2;
  signal \c$app_arg\           : filters_types.array_of_signed_8(0 to 2);
  signal \c$app_arg_0\         : filters_types.array_of_signed_8(0 to 2);
  signal \c$app_arg_1\         : filters_types.array_of_signed_8(0 to 2);
  signal \c$app_arg_2\         : filters_types.array_of_signed_8(0 to 2);
  signal \c$app_arg_3\         : filters_types.array_of_signed_8(0 to 2);
  -- Filters.hs:138:1-7
  signal x1                    : filters_types.array_of_signed_8(0 to 2);
  signal \c$app_arg_4\         : filters_types.array_of_signed_8(0 to 2);
  signal \c$app_arg_5\         : filters_types.array_of_signed_8(0 to 2);
  -- Filters.hs:223:1-9
  signal \c$tup_app_arg\       : filters_types.array_of_signed_8(0 to 5);
  signal \c$app_arg_1_res\     : filters_types.array_of_signed_8(0 to 2);
  signal \c$vec\               : filters_types.array_of_signed_8(0 to 5);
  signal \c$app_arg_3_res\     : filters_types.array_of_signed_8(0 to 2);
  signal \c$app_arg_3_res_res\ : filters_types.array_of_signed_8(0 to 2);
  signal \c$vec_0\             : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec_1\             : filters_types.array_of_signed_8(0 to 3);
  signal x1_projection         : filters_types.tup2_0;

begin
  result <= result_0.tup2_sel1_signed;

  result_0 <= ( tup2_sel0_array_of_signed_8 => filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(\c$app_arg_4\) & filters_types.array_of_signed_8'(\c$app_arg_5\))
              , tup2_sel1_signed =>  \c$tup_app_arg\(\c$tup_app_arg\'high)  );

  -- select begin
  select_r : for i in \c$app_arg\'range generate
    \c$app_arg\(i) <= \c$tup_app_arg\(0+(1*i));
  end generate;
  -- select end

  \c$app_arg_0\ <= filters_types.array_of_signed_8'(0 to 3-1 =>  x );

  \c$vec\ <= filters_types.array_of_signed_8'( to_signed(2,8)
                                             , to_signed(4,8)
                                             , to_signed(3,8)
                                             , to_signed(2,8)
                                             , to_signed(7,8)
                                             , to_signed(6,8) );

  -- select begin
  select_r_0 : for i_0 in \c$app_arg_1_res\'range generate
    \c$app_arg_1_res\(i_0) <= \c$vec\(0+(1*i_0));
  end generate;
  -- select end

  -- zipWith begin
  zipwith : for i_1 in \c$app_arg_1\'range generate
  begin
    \c$app_arg_1\(i_1) <= resize(\c$app_arg_0\(i_1) * \c$app_arg_1_res\(i_1), 8);


  end generate;
  -- zipWith end

  -- select begin
  select_r_1 : for i_2 in \c$app_arg_2\'range generate
    \c$app_arg_2\(i_2) <= \c$tup_app_arg\(2+(1*i_2));
  end generate;
  -- select end

  \c$vec_0\ <= filters_types.array_of_signed_8'( to_signed(2,8)
                                               , to_signed(4,8)
                                               , to_signed(3,8)
                                               , to_signed(2,8)
                                               , to_signed(7,8)
                                               , to_signed(6,8) );

  -- select begin
  select_r_2 : for i_3 in \c$app_arg_3_res_res\'range generate
    \c$app_arg_3_res_res\(i_3) <= \c$vec_0\(0+(1*i_3));
  end generate;
  -- select end

  -- reverse begin
  reverse_loop : for i_4 in 0 to (3 - 1) generate
    \c$app_arg_3_res\(\c$app_arg_3_res_res\'high - i_4) <= \c$app_arg_3_res_res\(i_4);
  end generate;
  -- reverse end

  -- zipWith begin
  zipwith_0 : for i_5 in \c$app_arg_3\'range generate
  begin
    \c$app_arg_3\(i_5) <= resize(\c$app_arg_0\(i_5) * \c$app_arg_3_res\(i_5), 8);


  end generate;
  -- zipWith end

  \c$vec_1\ <= (filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(0 => to_signed(0,8))) & filters_types.array_of_signed_8'(\c$app_arg\)));

  x1_projection <= (\c$vec_1\(0 to 3-1),\c$vec_1\(3 to \c$vec_1\'high));

  x1 <= x1_projection.tup2_0_sel0_array_of_signed_8_0;

  -- zipWith begin
  zipwith_1 : for i_6 in \c$app_arg_4\'range generate
  begin
    \c$app_arg_4\(i_6) <= x1(i_6) + \c$app_arg_1\(i_6);


  end generate;
  -- zipWith end

  -- zipWith begin
  zipwith_2 : for i_7 in \c$app_arg_5\'range generate
  begin
    \c$app_arg_5\(i_7) <= \c$app_arg_2\(i_7) + \c$app_arg_3\(i_7);


  end generate;
  -- zipWith end

  -- register begin
  topentity_register : block
    signal ctup_app_arg_reg : filters_types.array_of_signed_8(0 to 5) := filters_types.array_of_signed_8'( to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8) );
  begin
    \c$tup_app_arg\ <= ctup_app_arg_reg; 
    ctup_app_arg_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ctup_app_arg_reg <= filters_types.array_of_signed_8'( to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ctup_app_arg_reg <= result_0.tup2_sel0_array_of_signed_8
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end


end;

