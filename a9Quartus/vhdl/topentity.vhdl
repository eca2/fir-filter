-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(-- clock
       clk    : in filters_types.clk_system;
       -- reset
       rst    : in filters_types.rst_system;
       x      : in signed(17 downto 0);
       result : out signed(17 downto 0));
end;

architecture structural of topentity is
  signal result_0                   : filters_types.tup2;
  signal r                          : signed(18 downto 0);
  signal \c$app_arg\                : std_logic_vector(18 downto 0);
  signal \r'\                       : std_logic_vector(17 downto 0);
  signal \c$case_alt\               : signed(17 downto 0);
  signal result_1                   : signed(17 downto 0);
  signal \c$app_arg_0\              : signed(17 downto 0);
  signal \c$app_arg_1\              : filters_types.array_of_signed_18(0 to 3);
  signal \c$app_arg_2\              : filters_types.array_of_signed_18(0 to 2);
  -- Filters.hs:176:1-4
  signal ws1                        : filters_types.array_of_signed_18(0 to 3);
  -- Filters.hs:176:1-4
  signal ws2                        : filters_types.array_of_signed_18(0 to 2);
  -- Filters.hs:176:1-4
  signal \c$ws2_app_arg\            : filters_types.array_of_signed_18(0 to 2);
  -- Filters.hs:239:1-9
  signal \c$tup_app_arg\            : filters_types.array_of_signed_18(0 to 2);
  signal \c$r'_projection\          : filters_types.tup2_0;
  signal \c$case_alt_selection_res\ : boolean;
  signal \c$bv\                     : std_logic_vector(17 downto 0);
  signal \c$bv_0\                   : std_logic_vector(17 downto 0);
  signal result_1_selection_res     : boolean;
  signal \c$vec1\                   : filters_types.array_of_signed_18(0 to 3);
  signal \c$vec2\                   : filters_types.array_of_signed_18(0 to 3);
  signal \c$vec\                    : filters_types.array_of_signed_18(0 to 3);
  signal \c$app_arg_2_projection\   : filters_types.tup2_1;
  signal \c$vec2_0\                 : filters_types.array_of_signed_18(0 to 2);
  signal \c$vec1_0\                 : filters_types.array_of_signed_18(0 to 2);

begin
  result <= result_0.tup2_sel1_signed;

  result_0 <= ( tup2_sel0_array_of_signed_18 => \c$app_arg_2\
              , tup2_sel1_signed => \c$app_arg_0\ );

  r <= resize(x,19) + resize(( ws1(ws1'high) ),19);

  \c$app_arg\ <= std_logic_vector(r);

  \c$r'_projection\ <= (\c$app_arg\(\c$app_arg\'high downto 18),\c$app_arg\(18-1 downto 0));

  \r'\ <= \c$r'_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv\ <= (std_logic_vector(x));

  \c$bv_0\ <= (std_logic_vector(( ws1(ws1'high) )));

  \c$case_alt_selection_res\ <= (( \c$bv\(\c$bv\'high) ) and ( \c$bv_0\(\c$bv_0\'high) )) = '0';

  \c$case_alt\ <= to_signed(131071,18) when \c$case_alt_selection_res\ else
                  to_signed(-131072,18);

  result_1_selection_res <= (( \c$app_arg\(\c$app_arg\'high) ) xor ( \r'\(\r'\'high) )) = '0';

  result_1 <= signed(\r'\) when result_1_selection_res else
              \c$case_alt\;

  -- foldr begin
  foldr : block
    type foldr_res_type is array (natural range <>) of signed(17 downto 0);
    signal intermediate : foldr_res_type (0 to 4);
  begin
    intermediate(4) <= to_signed(0,18);

    foldr_loop : for i in 0 to (4 - 1) generate
    begin
      fun : block
      signal r_0                          : signed(18 downto 0);
      signal \c$app_arg_3\                : std_logic_vector(18 downto 0);
      signal \c$r'_0\                     : std_logic_vector(17 downto 0);
      signal \c$case_alt_0\               : signed(17 downto 0);
      signal \c$r'_0_projection\          : filters_types.tup2_0;
      signal \c$case_alt_0_selection_res\ : boolean;
      signal \c$bv_1\                     : std_logic_vector(17 downto 0);
      signal \c$bv_2\                     : std_logic_vector(17 downto 0);
      signal \c$intermediate(i)_selection_res\   : boolean;
    begin
      r_0 <= resize(\c$app_arg_1\(i),19) + resize(intermediate(i+1),19);

      \c$app_arg_3\ <= std_logic_vector(r_0);

      \c$r'_0_projection\ <= (\c$app_arg_3\(\c$app_arg_3\'high downto 18),\c$app_arg_3\(18-1 downto 0));

      \c$r'_0\ <= \c$r'_0_projection\.tup2_0_sel1_std_logic_vector_1;

      \c$bv_1\ <= (std_logic_vector(\c$app_arg_1\(i)));

      \c$bv_2\ <= (std_logic_vector(intermediate(i+1)));

      \c$case_alt_0_selection_res\ <= (( \c$bv_1\(\c$bv_1\'high) ) and ( \c$bv_2\(\c$bv_2\'high) )) = '0';

      \c$case_alt_0\ <= to_signed(131071,18) when \c$case_alt_0_selection_res\ else
                        to_signed(-131072,18);

      \c$intermediate(i)_selection_res\ <= (( \c$app_arg_3\(\c$app_arg_3\'high) ) xor ( \c$r'_0\(\c$r'_0\'high) )) = '0';

      intermediate(i) <= signed(\c$r'_0\) when \c$intermediate(i)_selection_res\ else
                 \c$case_alt_0\;


    end block;
    end generate;

    \c$app_arg_0\ <= intermediate(0);
  end block;
  -- foldr end

  \c$vec1\ <= filters_types.array_of_signed_18'( to_signed(510,18)
                                               , to_signed(1531,18)
                                               , to_signed(1531,18)
                                               , to_signed(510,18) );

  \c$vec2\ <= (filters_types.array_of_signed_18'(filters_types.array_of_signed_18'(\c$tup_app_arg\) & filters_types.array_of_signed_18'(filters_types.array_of_signed_18'(0 => result_1))));

  -- zipWith begin
  zipwith : for i_0 in \c$app_arg_1\'range generate
  begin
    fun_0 : block
      signal \c$case_alt_1\               : signed(17 downto 0);
      signal \c$app_arg_4\                : std_logic;
      signal \rL\                         : std_logic_vector(4 downto 0);
      signal \rR\                         : std_logic_vector(30 downto 0);
      signal \c$app_arg_5\                : std_logic;
      signal ds3                          : filters_types.tup2_2;
      signal x_0                          : std_logic_vector(5 downto 0);
      signal \c$c$app_arg_1(i_0)_selection_res_0\ : boolean;
      signal \c$case_alt_1_selection_res\ : boolean;
      signal \c$bv_3\                     : std_logic_vector(35 downto 0);
    begin
      \c$c$app_arg_1(i_0)_selection_res_0\ <= ((not \c$app_arg_5\) or \c$app_arg_4\) = '1';

      \c$app_arg_1\(i_0) <= signed((std_logic_vector(resize(unsigned((std_logic_vector(shift_right(unsigned(\rR\),to_integer(to_signed(13,64)))))),18)))) when \c$c$app_arg_1(i_0)_selection_res_0\ else
                 \c$case_alt_1\;

      \c$case_alt_1_selection_res\ <= ( \rL\(\rL\'high) ) = '0';

      \c$case_alt_1\ <= to_signed(131071,18) when \c$case_alt_1_selection_res\ else
                        to_signed(-131072,18);

      -- reduceAnd begin,

      reduceand : block
        function and_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '1';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := and_reduce (argi (argi'left downto half));
              lower  := and_reduce (argi (half - 1 downto argi'right));
              result := upper and lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_4\ <= and_reduce(x_0);
      end block;
      -- reduceAnd end

      \rL\ <= ds3.tup2_2_sel0_std_logic_vector_0;

      \rR\ <= ds3.tup2_2_sel1_std_logic_vector_1;

      -- reduceOr begin 
      reduceor : block
        function or_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '0';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := or_reduce (argi (argi'left downto half));
              lower  := or_reduce (argi (half - 1 downto argi'right));
              result := upper or lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_5\ <= or_reduce(x_0);
      end block;
      -- reduceOr end

      \c$bv_3\ <= (std_logic_vector((\c$vec1\(i_0) * \c$vec2\(i_0))));

      ds3 <= (\c$bv_3\(\c$bv_3\'high downto 31),\c$bv_3\(31-1 downto 0));

      x_0 <= std_logic_vector'(std_logic_vector'((std_logic_vector'(0 => ( \rR\(\rR\'high) )))) & std_logic_vector'(\rL\));


    end block;
  end generate;
  -- zipWith end

  \c$vec\ <= (filters_types.array_of_signed_18'(filters_types.array_of_signed_18'(\c$tup_app_arg\) & filters_types.array_of_signed_18'(filters_types.array_of_signed_18'(0 => result_1))));

  \c$app_arg_2_projection\ <= (\c$vec\(0 to 1-1),\c$vec\(1 to \c$vec\'high));

  \c$app_arg_2\ <= \c$app_arg_2_projection\.tup2_1_sel1_array_of_signed_18_1;

  ws1 <= filters_types.array_of_signed_18'(signed'(to_signed(0,18)) & ws2);

  \c$vec2_0\ <= (ws1(0 to ws1'high - 1));

  -- zipWith begin
  zipwith_0 : for i_1 in ws2'range generate
  begin
    fun_1 : block
      signal r_1                          : signed(18 downto 0);
      signal \c$app_arg_6\                : std_logic_vector(18 downto 0);
      signal \c$r'_1\                     : std_logic_vector(17 downto 0);
      signal \c$case_alt_2\               : signed(17 downto 0);
      signal \c$r'_1_projection\          : filters_types.tup2_0;
      signal \c$case_alt_2_selection_res\ : boolean;
      signal \c$bv_4\                     : std_logic_vector(17 downto 0);
      signal \c$bv_5\                     : std_logic_vector(17 downto 0);
      signal \c$ws2(i_1)_selection_res_1\ : boolean;
    begin
      r_1 <= resize(\c$vec2_0\(i_1),19) + resize(\c$ws2_app_arg\(i_1),19);

      \c$app_arg_6\ <= std_logic_vector(r_1);

      \c$r'_1_projection\ <= (\c$app_arg_6\(\c$app_arg_6\'high downto 18),\c$app_arg_6\(18-1 downto 0));

      \c$r'_1\ <= \c$r'_1_projection\.tup2_0_sel1_std_logic_vector_1;

      \c$bv_4\ <= (std_logic_vector(\c$vec2_0\(i_1)));

      \c$bv_5\ <= (std_logic_vector(\c$ws2_app_arg\(i_1)));

      \c$case_alt_2_selection_res\ <= (( \c$bv_4\(\c$bv_4\'high) ) and ( \c$bv_5\(\c$bv_5\'high) )) = '0';

      \c$case_alt_2\ <= to_signed(131071,18) when \c$case_alt_2_selection_res\ else
                        to_signed(-131072,18);

      \c$ws2(i_1)_selection_res_1\ <= (( \c$app_arg_6\(\c$app_arg_6\'high) ) xor ( \c$r'_1\(\c$r'_1\'high) )) = '0';

      ws2(i_1) <= signed(\c$r'_1\) when \c$ws2(i_1)_selection_res_1\ else
                 \c$case_alt_2\;


    end block;
  end generate;
  -- zipWith end

  \c$vec1_0\ <= filters_types.array_of_signed_18'( to_signed(892,18)
                                                 , to_signed(-4857,18)
                                                 , to_signed(8071,18) );

  -- zipWith begin
  zipwith_1 : for i_2 in \c$ws2_app_arg\'range generate
  begin
    fun_2 : block
      signal \c$case_alt_3\               : signed(17 downto 0);
      signal \c$app_arg_7\                : std_logic;
      signal \c$rL_0\                     : std_logic_vector(4 downto 0);
      signal \c$rR_0\                     : std_logic_vector(30 downto 0);
      signal \c$app_arg_8\                : std_logic;
      signal ds3_0                        : filters_types.tup2_2;
      signal x_1                          : std_logic_vector(5 downto 0);
      signal \c$c$ws2_app_arg(i_2)_selection_res_2\ : boolean;
      signal \c$case_alt_3_selection_res\ : boolean;
      signal \c$bv_6\                     : std_logic_vector(35 downto 0);
    begin
      \c$c$ws2_app_arg(i_2)_selection_res_2\ <= ((not \c$app_arg_8\) or \c$app_arg_7\) = '1';

      \c$ws2_app_arg\(i_2) <= signed((std_logic_vector(resize(unsigned((std_logic_vector(shift_right(unsigned(\c$rR_0\),to_integer(to_signed(13,64)))))),18)))) when \c$c$ws2_app_arg(i_2)_selection_res_2\ else
                 \c$case_alt_3\;

      \c$case_alt_3_selection_res\ <= ( \c$rL_0\(\c$rL_0\'high) ) = '0';

      \c$case_alt_3\ <= to_signed(131071,18) when \c$case_alt_3_selection_res\ else
                        to_signed(-131072,18);

      -- reduceAnd begin,

      reduceand_0 : block
        function and_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '1';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := and_reduce (argi (argi'left downto half));
              lower  := and_reduce (argi (half - 1 downto argi'right));
              result := upper and lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_7\ <= and_reduce(x_1);
      end block;
      -- reduceAnd end

      \c$rL_0\ <= ds3_0.tup2_2_sel0_std_logic_vector_0;

      \c$rR_0\ <= ds3_0.tup2_2_sel1_std_logic_vector_1;

      -- reduceOr begin 
      reduceor_0 : block
        function or_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '0';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := or_reduce (argi (argi'left downto half));
              lower  := or_reduce (argi (half - 1 downto argi'right));
              result := upper or lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_8\ <= or_reduce(x_1);
      end block;
      -- reduceOr end

      \c$bv_6\ <= (std_logic_vector((\c$vec1_0\(i_2) * \c$tup_app_arg\(i_2))));

      ds3_0 <= (\c$bv_6\(\c$bv_6\'high downto 31),\c$bv_6\(31-1 downto 0));

      x_1 <= std_logic_vector'(std_logic_vector'((std_logic_vector'(0 => ( \c$rR_0\(\c$rR_0\'high) )))) & std_logic_vector'(\c$rL_0\));


    end block;
  end generate;
  -- zipWith end

  -- register begin
  topentity_register : block
    signal ctup_app_arg_reg : filters_types.array_of_signed_18(0 to 2) := filters_types.array_of_signed_18'( to_signed(0,18), to_signed(0,18), to_signed(0,18) );
  begin
    \c$tup_app_arg\ <= ctup_app_arg_reg; 
    ctup_app_arg_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ctup_app_arg_reg <= filters_types.array_of_signed_18'( to_signed(0,18), to_signed(0,18), to_signed(0,18) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ctup_app_arg_reg <= result_0.tup2_sel0_array_of_signed_18
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end


end;

