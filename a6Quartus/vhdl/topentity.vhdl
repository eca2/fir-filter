-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(-- clock
       clk    : in filters_types.clk_system;
       -- reset
       rst    : in filters_types.rst_system;
       x      : in signed(17 downto 0);
       result : out signed(17 downto 0));
end;

architecture structural of topentity is
  signal r                             : signed(18 downto 0);
  signal \c$app_arg\                   : std_logic_vector(18 downto 0);
  signal \r'\                          : std_logic_vector(17 downto 0);
  signal \c$case_alt\                  : signed(17 downto 0);
  signal result_0                      : signed(17 downto 0);
  signal r_0                           : signed(18 downto 0);
  signal \c$app_arg_0\                 : std_logic_vector(18 downto 0);
  signal \c$r'_0\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_0\                : signed(17 downto 0);
  signal result_1                      : signed(17 downto 0);
  signal r_1                           : signed(18 downto 0);
  signal \c$app_arg_1\                 : std_logic_vector(18 downto 0);
  signal \c$r'_1\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_1\                : signed(17 downto 0);
  signal result_2                      : signed(17 downto 0);
  signal r_2                           : signed(18 downto 0);
  signal \c$app_arg_2\                 : std_logic_vector(18 downto 0);
  signal \c$r'_2\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_2\                : signed(17 downto 0);
  signal result_3                      : signed(17 downto 0);
  signal r_3                           : signed(18 downto 0);
  signal \c$app_arg_3\                 : std_logic_vector(18 downto 0);
  signal \c$r'_3\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_3\                : signed(17 downto 0);
  signal result_4                      : signed(17 downto 0);
  signal r_4                           : signed(18 downto 0);
  signal \c$app_arg_4\                 : std_logic_vector(18 downto 0);
  signal \c$r'_4\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_4\                : signed(17 downto 0);
  signal result_5                      : signed(17 downto 0);
  signal r_5                           : signed(18 downto 0);
  signal \c$app_arg_5\                 : std_logic_vector(18 downto 0);
  signal \c$r'_5\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_5\                : signed(17 downto 0);
  signal result_6                      : signed(17 downto 0);
  signal r_6                           : signed(18 downto 0);
  signal \c$app_arg_6\                 : std_logic_vector(18 downto 0);
  signal \c$r'_6\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_6\                : signed(17 downto 0);
  signal result_7                      : signed(17 downto 0);
  signal r_7                           : signed(18 downto 0);
  signal \c$app_arg_7\                 : std_logic_vector(18 downto 0);
  signal \c$r'_7\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_7\                : signed(17 downto 0);
  signal result_8                      : signed(17 downto 0);
  signal r_8                           : signed(18 downto 0);
  signal \c$app_arg_8\                 : std_logic_vector(18 downto 0);
  signal \c$r'_8\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_8\                : signed(17 downto 0);
  signal result_9                      : signed(17 downto 0);
  signal r_9                           : signed(18 downto 0);
  signal \c$app_arg_9\                 : std_logic_vector(18 downto 0);
  signal \c$r'_9\                      : std_logic_vector(17 downto 0);
  signal \c$case_alt_9\                : signed(17 downto 0);
  signal result_10                     : signed(17 downto 0);
  signal r_10                          : signed(18 downto 0);
  signal \c$app_arg_10\                : std_logic_vector(18 downto 0);
  signal \c$r'_10\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_10\               : signed(17 downto 0);
  signal result_11                     : signed(17 downto 0);
  signal r_11                          : signed(18 downto 0);
  signal \c$app_arg_11\                : std_logic_vector(18 downto 0);
  signal \c$r'_11\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_11\               : signed(17 downto 0);
  signal result_12                     : signed(17 downto 0);
  signal r_12                          : signed(18 downto 0);
  signal \c$app_arg_12\                : std_logic_vector(18 downto 0);
  signal \c$r'_12\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_12\               : signed(17 downto 0);
  signal result_13                     : signed(17 downto 0);
  signal r_13                          : signed(18 downto 0);
  signal \c$app_arg_13\                : std_logic_vector(18 downto 0);
  signal \c$r'_13\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_13\               : signed(17 downto 0);
  signal result_14                     : signed(17 downto 0);
  signal r_14                          : signed(18 downto 0);
  signal \c$app_arg_14\                : std_logic_vector(18 downto 0);
  signal \c$r'_14\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_14\               : signed(17 downto 0);
  signal result_15                     : signed(17 downto 0);
  signal r_15                          : signed(18 downto 0);
  signal \c$app_arg_15\                : std_logic_vector(18 downto 0);
  signal \c$r'_15\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_15\               : signed(17 downto 0);
  signal result_16                     : signed(17 downto 0);
  signal r_16                          : signed(18 downto 0);
  signal \c$app_arg_16\                : std_logic_vector(18 downto 0);
  signal \c$r'_16\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_16\               : signed(17 downto 0);
  signal result_17                     : signed(17 downto 0);
  signal r_17                          : signed(18 downto 0);
  signal \c$app_arg_17\                : std_logic_vector(18 downto 0);
  signal \c$r'_17\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_17\               : signed(17 downto 0);
  signal result_18                     : signed(17 downto 0);
  signal r_18                          : signed(18 downto 0);
  signal \c$app_arg_18\                : std_logic_vector(18 downto 0);
  signal \c$r'_18\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_18\               : signed(17 downto 0);
  signal result_19                     : signed(17 downto 0);
  signal r_19                          : signed(18 downto 0);
  signal \c$app_arg_19\                : std_logic_vector(18 downto 0);
  signal \c$r'_19\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_19\               : signed(17 downto 0);
  signal result_20                     : signed(17 downto 0);
  signal r_20                          : signed(18 downto 0);
  signal \c$app_arg_20\                : std_logic_vector(18 downto 0);
  signal \c$r'_20\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_20\               : signed(17 downto 0);
  signal result_21                     : signed(17 downto 0);
  signal r_21                          : signed(18 downto 0);
  signal \c$app_arg_21\                : std_logic_vector(18 downto 0);
  signal \c$r'_21\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_21\               : signed(17 downto 0);
  signal result_22                     : signed(17 downto 0);
  signal r_22                          : signed(18 downto 0);
  signal \c$app_arg_22\                : std_logic_vector(18 downto 0);
  signal \c$r'_22\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_22\               : signed(17 downto 0);
  signal result_23                     : signed(17 downto 0);
  signal r_23                          : signed(18 downto 0);
  signal \c$app_arg_23\                : std_logic_vector(18 downto 0);
  signal \c$r'_23\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_23\               : signed(17 downto 0);
  signal result_24                     : signed(17 downto 0);
  signal r_24                          : signed(18 downto 0);
  signal \c$app_arg_24\                : std_logic_vector(18 downto 0);
  signal \c$r'_24\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_24\               : signed(17 downto 0);
  signal result_25                     : signed(17 downto 0);
  signal r_25                          : signed(18 downto 0);
  signal \c$app_arg_25\                : std_logic_vector(18 downto 0);
  signal \c$r'_25\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_25\               : signed(17 downto 0);
  signal result_26                     : signed(17 downto 0);
  signal r_26                          : signed(18 downto 0);
  signal \c$app_arg_26\                : std_logic_vector(18 downto 0);
  signal \c$r'_26\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_26\               : signed(17 downto 0);
  signal result_27                     : signed(17 downto 0);
  signal r_27                          : signed(18 downto 0);
  signal \c$app_arg_27\                : std_logic_vector(18 downto 0);
  signal \c$r'_27\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_27\               : signed(17 downto 0);
  signal result_28                     : signed(17 downto 0);
  signal r_28                          : signed(18 downto 0);
  signal \c$app_arg_28\                : std_logic_vector(18 downto 0);
  signal \c$r'_28\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_28\               : signed(17 downto 0);
  signal result_29                     : signed(17 downto 0);
  signal r_29                          : signed(18 downto 0);
  signal \c$app_arg_29\                : std_logic_vector(18 downto 0);
  signal \c$r'_29\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_29\               : signed(17 downto 0);
  signal result_30                     : signed(17 downto 0);
  signal r_30                          : signed(18 downto 0);
  signal \c$app_arg_30\                : std_logic_vector(18 downto 0);
  signal \c$r'_30\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_30\               : signed(17 downto 0);
  signal result_31                     : signed(17 downto 0);
  signal r_31                          : signed(18 downto 0);
  signal \c$app_arg_31\                : std_logic_vector(18 downto 0);
  signal \c$r'_31\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_31\               : signed(17 downto 0);
  signal result_32                     : signed(17 downto 0);
  signal r_32                          : signed(18 downto 0);
  signal \c$app_arg_32\                : std_logic_vector(18 downto 0);
  signal \c$r'_32\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_32\               : signed(17 downto 0);
  signal result_33                     : signed(17 downto 0);
  signal r_33                          : signed(18 downto 0);
  signal \c$app_arg_33\                : std_logic_vector(18 downto 0);
  signal \c$r'_33\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_33\               : signed(17 downto 0);
  signal result_34                     : signed(17 downto 0);
  signal r_34                          : signed(18 downto 0);
  signal \c$app_arg_34\                : std_logic_vector(18 downto 0);
  signal \c$r'_34\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_34\               : signed(17 downto 0);
  signal result_35                     : signed(17 downto 0);
  signal r_35                          : signed(18 downto 0);
  signal \c$app_arg_35\                : std_logic_vector(18 downto 0);
  signal \c$r'_35\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_35\               : signed(17 downto 0);
  signal result_36                     : signed(17 downto 0);
  signal r_36                          : signed(18 downto 0);
  signal \c$app_arg_36\                : std_logic_vector(18 downto 0);
  signal \c$r'_36\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_36\               : signed(17 downto 0);
  signal result_37                     : signed(17 downto 0);
  signal r_37                          : signed(18 downto 0);
  signal \c$app_arg_37\                : std_logic_vector(18 downto 0);
  signal \c$r'_37\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_37\               : signed(17 downto 0);
  signal result_38                     : signed(17 downto 0);
  signal r_38                          : signed(18 downto 0);
  signal \c$app_arg_38\                : std_logic_vector(18 downto 0);
  signal \c$r'_38\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_38\               : signed(17 downto 0);
  signal result_39                     : signed(17 downto 0);
  signal r_39                          : signed(18 downto 0);
  signal \c$app_arg_39\                : std_logic_vector(18 downto 0);
  signal \c$r'_39\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_39\               : signed(17 downto 0);
  signal result_40                     : signed(17 downto 0);
  signal r_40                          : signed(18 downto 0);
  signal \c$app_arg_40\                : std_logic_vector(18 downto 0);
  signal \c$r'_40\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_40\               : signed(17 downto 0);
  signal result_41                     : signed(17 downto 0);
  signal r_41                          : signed(18 downto 0);
  signal \c$app_arg_41\                : std_logic_vector(18 downto 0);
  signal \c$r'_41\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_41\               : signed(17 downto 0);
  signal result_42                     : signed(17 downto 0);
  signal r_42                          : signed(18 downto 0);
  signal \c$app_arg_42\                : std_logic_vector(18 downto 0);
  signal \c$r'_42\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_42\               : signed(17 downto 0);
  signal result_43                     : signed(17 downto 0);
  signal r_43                          : signed(18 downto 0);
  signal \c$app_arg_43\                : std_logic_vector(18 downto 0);
  signal \c$r'_43\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_43\               : signed(17 downto 0);
  signal result_44                     : signed(17 downto 0);
  signal r_44                          : signed(18 downto 0);
  signal \c$app_arg_44\                : std_logic_vector(18 downto 0);
  signal \c$r'_44\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_44\               : signed(17 downto 0);
  signal result_45                     : signed(17 downto 0);
  signal r_45                          : signed(18 downto 0);
  signal \c$app_arg_45\                : std_logic_vector(18 downto 0);
  signal \c$r'_45\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_45\               : signed(17 downto 0);
  signal result_46                     : signed(17 downto 0);
  signal r_46                          : signed(18 downto 0);
  signal \c$app_arg_46\                : std_logic_vector(18 downto 0);
  signal \c$r'_46\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_46\               : signed(17 downto 0);
  signal result_47                     : signed(17 downto 0);
  signal r_47                          : signed(18 downto 0);
  signal \c$app_arg_47\                : std_logic_vector(18 downto 0);
  signal \c$r'_47\                     : std_logic_vector(17 downto 0);
  signal \c$case_alt_47\               : signed(17 downto 0);
  signal result_48                     : signed(17 downto 0);
  signal \c$restF0_case_scrut\         : filters_types.array_of_signed_18(0 to 49);
  signal \c$elF0_app_arg\              : filters_types.array_of_signed_18(0 to 49);
  signal \c$restF0_app_arg\            : filters_types.array_of_signed_18(0 to 49);
  signal \c$elF0_app_arg_0\            : filters_types.array_of_signed_18(0 to 49);
  signal \c$restF0_app_arg_0\          : filters_types.array_of_signed_18(0 to 49);
  signal result_49                     : filters_types.tup2;
  -- Filters.hs:219:1-9
  signal \c$tup_app_arg\               : filters_types.array_of_signed_18(0 to 99);
  signal \c$r'_projection\             : filters_types.tup2_0;
  signal \c$case_alt_selection_res\    : boolean;
  signal \c$bv\                        : std_logic_vector(17 downto 0);
  signal \c$bv_0\                      : std_logic_vector(17 downto 0);
  signal result_0_selection_res        : boolean;
  signal \c$r'_0_projection\           : filters_types.tup2_0;
  signal \c$case_alt_0_selection_res\  : boolean;
  signal \c$bv_1\                      : std_logic_vector(17 downto 0);
  signal \c$bv_2\                      : std_logic_vector(17 downto 0);
  signal result_1_selection_res        : boolean;
  signal \c$r'_1_projection\           : filters_types.tup2_0;
  signal \c$case_alt_1_selection_res\  : boolean;
  signal \c$bv_3\                      : std_logic_vector(17 downto 0);
  signal \c$bv_4\                      : std_logic_vector(17 downto 0);
  signal result_2_selection_res        : boolean;
  signal \c$r'_2_projection\           : filters_types.tup2_0;
  signal \c$case_alt_2_selection_res\  : boolean;
  signal \c$bv_5\                      : std_logic_vector(17 downto 0);
  signal \c$bv_6\                      : std_logic_vector(17 downto 0);
  signal result_3_selection_res        : boolean;
  signal \c$r'_3_projection\           : filters_types.tup2_0;
  signal \c$case_alt_3_selection_res\  : boolean;
  signal \c$bv_7\                      : std_logic_vector(17 downto 0);
  signal \c$bv_8\                      : std_logic_vector(17 downto 0);
  signal result_4_selection_res        : boolean;
  signal \c$r'_4_projection\           : filters_types.tup2_0;
  signal \c$case_alt_4_selection_res\  : boolean;
  signal \c$bv_9\                      : std_logic_vector(17 downto 0);
  signal \c$bv_10\                     : std_logic_vector(17 downto 0);
  signal result_5_selection_res        : boolean;
  signal \c$r'_5_projection\           : filters_types.tup2_0;
  signal \c$case_alt_5_selection_res\  : boolean;
  signal \c$bv_11\                     : std_logic_vector(17 downto 0);
  signal \c$bv_12\                     : std_logic_vector(17 downto 0);
  signal result_6_selection_res        : boolean;
  signal \c$r'_6_projection\           : filters_types.tup2_0;
  signal \c$case_alt_6_selection_res\  : boolean;
  signal \c$bv_13\                     : std_logic_vector(17 downto 0);
  signal \c$bv_14\                     : std_logic_vector(17 downto 0);
  signal result_7_selection_res        : boolean;
  signal \c$r'_7_projection\           : filters_types.tup2_0;
  signal \c$case_alt_7_selection_res\  : boolean;
  signal \c$bv_15\                     : std_logic_vector(17 downto 0);
  signal \c$bv_16\                     : std_logic_vector(17 downto 0);
  signal result_8_selection_res        : boolean;
  signal \c$r'_8_projection\           : filters_types.tup2_0;
  signal \c$case_alt_8_selection_res\  : boolean;
  signal \c$bv_17\                     : std_logic_vector(17 downto 0);
  signal \c$bv_18\                     : std_logic_vector(17 downto 0);
  signal result_9_selection_res        : boolean;
  signal \c$r'_9_projection\           : filters_types.tup2_0;
  signal \c$case_alt_9_selection_res\  : boolean;
  signal \c$bv_19\                     : std_logic_vector(17 downto 0);
  signal \c$bv_20\                     : std_logic_vector(17 downto 0);
  signal result_10_selection_res       : boolean;
  signal \c$r'_10_projection\          : filters_types.tup2_0;
  signal \c$case_alt_10_selection_res\ : boolean;
  signal \c$bv_21\                     : std_logic_vector(17 downto 0);
  signal \c$bv_22\                     : std_logic_vector(17 downto 0);
  signal result_11_selection_res       : boolean;
  signal \c$r'_11_projection\          : filters_types.tup2_0;
  signal \c$case_alt_11_selection_res\ : boolean;
  signal \c$bv_23\                     : std_logic_vector(17 downto 0);
  signal \c$bv_24\                     : std_logic_vector(17 downto 0);
  signal result_12_selection_res       : boolean;
  signal \c$r'_12_projection\          : filters_types.tup2_0;
  signal \c$case_alt_12_selection_res\ : boolean;
  signal \c$bv_25\                     : std_logic_vector(17 downto 0);
  signal \c$bv_26\                     : std_logic_vector(17 downto 0);
  signal result_13_selection_res       : boolean;
  signal \c$r'_13_projection\          : filters_types.tup2_0;
  signal \c$case_alt_13_selection_res\ : boolean;
  signal \c$bv_27\                     : std_logic_vector(17 downto 0);
  signal \c$bv_28\                     : std_logic_vector(17 downto 0);
  signal result_14_selection_res       : boolean;
  signal \c$r'_14_projection\          : filters_types.tup2_0;
  signal \c$case_alt_14_selection_res\ : boolean;
  signal \c$bv_29\                     : std_logic_vector(17 downto 0);
  signal \c$bv_30\                     : std_logic_vector(17 downto 0);
  signal result_15_selection_res       : boolean;
  signal \c$r'_15_projection\          : filters_types.tup2_0;
  signal \c$case_alt_15_selection_res\ : boolean;
  signal \c$bv_31\                     : std_logic_vector(17 downto 0);
  signal \c$bv_32\                     : std_logic_vector(17 downto 0);
  signal result_16_selection_res       : boolean;
  signal \c$r'_16_projection\          : filters_types.tup2_0;
  signal \c$case_alt_16_selection_res\ : boolean;
  signal \c$bv_33\                     : std_logic_vector(17 downto 0);
  signal \c$bv_34\                     : std_logic_vector(17 downto 0);
  signal result_17_selection_res       : boolean;
  signal \c$r'_17_projection\          : filters_types.tup2_0;
  signal \c$case_alt_17_selection_res\ : boolean;
  signal \c$bv_35\                     : std_logic_vector(17 downto 0);
  signal \c$bv_36\                     : std_logic_vector(17 downto 0);
  signal result_18_selection_res       : boolean;
  signal \c$r'_18_projection\          : filters_types.tup2_0;
  signal \c$case_alt_18_selection_res\ : boolean;
  signal \c$bv_37\                     : std_logic_vector(17 downto 0);
  signal \c$bv_38\                     : std_logic_vector(17 downto 0);
  signal result_19_selection_res       : boolean;
  signal \c$r'_19_projection\          : filters_types.tup2_0;
  signal \c$case_alt_19_selection_res\ : boolean;
  signal \c$bv_39\                     : std_logic_vector(17 downto 0);
  signal \c$bv_40\                     : std_logic_vector(17 downto 0);
  signal result_20_selection_res       : boolean;
  signal \c$r'_20_projection\          : filters_types.tup2_0;
  signal \c$case_alt_20_selection_res\ : boolean;
  signal \c$bv_41\                     : std_logic_vector(17 downto 0);
  signal \c$bv_42\                     : std_logic_vector(17 downto 0);
  signal result_21_selection_res       : boolean;
  signal \c$r'_21_projection\          : filters_types.tup2_0;
  signal \c$case_alt_21_selection_res\ : boolean;
  signal \c$bv_43\                     : std_logic_vector(17 downto 0);
  signal \c$bv_44\                     : std_logic_vector(17 downto 0);
  signal result_22_selection_res       : boolean;
  signal \c$r'_22_projection\          : filters_types.tup2_0;
  signal \c$case_alt_22_selection_res\ : boolean;
  signal \c$bv_45\                     : std_logic_vector(17 downto 0);
  signal \c$bv_46\                     : std_logic_vector(17 downto 0);
  signal result_23_selection_res       : boolean;
  signal \c$r'_23_projection\          : filters_types.tup2_0;
  signal \c$case_alt_23_selection_res\ : boolean;
  signal \c$bv_47\                     : std_logic_vector(17 downto 0);
  signal \c$bv_48\                     : std_logic_vector(17 downto 0);
  signal result_24_selection_res       : boolean;
  signal \c$r'_24_projection\          : filters_types.tup2_0;
  signal \c$case_alt_24_selection_res\ : boolean;
  signal \c$bv_49\                     : std_logic_vector(17 downto 0);
  signal \c$bv_50\                     : std_logic_vector(17 downto 0);
  signal result_25_selection_res       : boolean;
  signal \c$r'_25_projection\          : filters_types.tup2_0;
  signal \c$case_alt_25_selection_res\ : boolean;
  signal \c$bv_51\                     : std_logic_vector(17 downto 0);
  signal \c$bv_52\                     : std_logic_vector(17 downto 0);
  signal result_26_selection_res       : boolean;
  signal \c$r'_26_projection\          : filters_types.tup2_0;
  signal \c$case_alt_26_selection_res\ : boolean;
  signal \c$bv_53\                     : std_logic_vector(17 downto 0);
  signal \c$bv_54\                     : std_logic_vector(17 downto 0);
  signal result_27_selection_res       : boolean;
  signal \c$r'_27_projection\          : filters_types.tup2_0;
  signal \c$case_alt_27_selection_res\ : boolean;
  signal \c$bv_55\                     : std_logic_vector(17 downto 0);
  signal \c$bv_56\                     : std_logic_vector(17 downto 0);
  signal result_28_selection_res       : boolean;
  signal \c$r'_28_projection\          : filters_types.tup2_0;
  signal \c$case_alt_28_selection_res\ : boolean;
  signal \c$bv_57\                     : std_logic_vector(17 downto 0);
  signal \c$bv_58\                     : std_logic_vector(17 downto 0);
  signal result_29_selection_res       : boolean;
  signal \c$r'_29_projection\          : filters_types.tup2_0;
  signal \c$case_alt_29_selection_res\ : boolean;
  signal \c$bv_59\                     : std_logic_vector(17 downto 0);
  signal \c$bv_60\                     : std_logic_vector(17 downto 0);
  signal result_30_selection_res       : boolean;
  signal \c$r'_30_projection\          : filters_types.tup2_0;
  signal \c$case_alt_30_selection_res\ : boolean;
  signal \c$bv_61\                     : std_logic_vector(17 downto 0);
  signal \c$bv_62\                     : std_logic_vector(17 downto 0);
  signal result_31_selection_res       : boolean;
  signal \c$r'_31_projection\          : filters_types.tup2_0;
  signal \c$case_alt_31_selection_res\ : boolean;
  signal \c$bv_63\                     : std_logic_vector(17 downto 0);
  signal \c$bv_64\                     : std_logic_vector(17 downto 0);
  signal result_32_selection_res       : boolean;
  signal \c$r'_32_projection\          : filters_types.tup2_0;
  signal \c$case_alt_32_selection_res\ : boolean;
  signal \c$bv_65\                     : std_logic_vector(17 downto 0);
  signal \c$bv_66\                     : std_logic_vector(17 downto 0);
  signal result_33_selection_res       : boolean;
  signal \c$r'_33_projection\          : filters_types.tup2_0;
  signal \c$case_alt_33_selection_res\ : boolean;
  signal \c$bv_67\                     : std_logic_vector(17 downto 0);
  signal \c$bv_68\                     : std_logic_vector(17 downto 0);
  signal result_34_selection_res       : boolean;
  signal \c$r'_34_projection\          : filters_types.tup2_0;
  signal \c$case_alt_34_selection_res\ : boolean;
  signal \c$bv_69\                     : std_logic_vector(17 downto 0);
  signal \c$bv_70\                     : std_logic_vector(17 downto 0);
  signal result_35_selection_res       : boolean;
  signal \c$r'_35_projection\          : filters_types.tup2_0;
  signal \c$case_alt_35_selection_res\ : boolean;
  signal \c$bv_71\                     : std_logic_vector(17 downto 0);
  signal \c$bv_72\                     : std_logic_vector(17 downto 0);
  signal result_36_selection_res       : boolean;
  signal \c$r'_36_projection\          : filters_types.tup2_0;
  signal \c$case_alt_36_selection_res\ : boolean;
  signal \c$bv_73\                     : std_logic_vector(17 downto 0);
  signal \c$bv_74\                     : std_logic_vector(17 downto 0);
  signal result_37_selection_res       : boolean;
  signal \c$r'_37_projection\          : filters_types.tup2_0;
  signal \c$case_alt_37_selection_res\ : boolean;
  signal \c$bv_75\                     : std_logic_vector(17 downto 0);
  signal \c$bv_76\                     : std_logic_vector(17 downto 0);
  signal result_38_selection_res       : boolean;
  signal \c$r'_38_projection\          : filters_types.tup2_0;
  signal \c$case_alt_38_selection_res\ : boolean;
  signal \c$bv_77\                     : std_logic_vector(17 downto 0);
  signal \c$bv_78\                     : std_logic_vector(17 downto 0);
  signal result_39_selection_res       : boolean;
  signal \c$r'_39_projection\          : filters_types.tup2_0;
  signal \c$case_alt_39_selection_res\ : boolean;
  signal \c$bv_79\                     : std_logic_vector(17 downto 0);
  signal \c$bv_80\                     : std_logic_vector(17 downto 0);
  signal result_40_selection_res       : boolean;
  signal \c$r'_40_projection\          : filters_types.tup2_0;
  signal \c$case_alt_40_selection_res\ : boolean;
  signal \c$bv_81\                     : std_logic_vector(17 downto 0);
  signal \c$bv_82\                     : std_logic_vector(17 downto 0);
  signal result_41_selection_res       : boolean;
  signal \c$r'_41_projection\          : filters_types.tup2_0;
  signal \c$case_alt_41_selection_res\ : boolean;
  signal \c$bv_83\                     : std_logic_vector(17 downto 0);
  signal \c$bv_84\                     : std_logic_vector(17 downto 0);
  signal result_42_selection_res       : boolean;
  signal \c$r'_42_projection\          : filters_types.tup2_0;
  signal \c$case_alt_42_selection_res\ : boolean;
  signal \c$bv_85\                     : std_logic_vector(17 downto 0);
  signal \c$bv_86\                     : std_logic_vector(17 downto 0);
  signal result_43_selection_res       : boolean;
  signal \c$r'_43_projection\          : filters_types.tup2_0;
  signal \c$case_alt_43_selection_res\ : boolean;
  signal \c$bv_87\                     : std_logic_vector(17 downto 0);
  signal \c$bv_88\                     : std_logic_vector(17 downto 0);
  signal result_44_selection_res       : boolean;
  signal \c$r'_44_projection\          : filters_types.tup2_0;
  signal \c$case_alt_44_selection_res\ : boolean;
  signal \c$bv_89\                     : std_logic_vector(17 downto 0);
  signal \c$bv_90\                     : std_logic_vector(17 downto 0);
  signal result_45_selection_res       : boolean;
  signal \c$r'_45_projection\          : filters_types.tup2_0;
  signal \c$case_alt_45_selection_res\ : boolean;
  signal \c$bv_91\                     : std_logic_vector(17 downto 0);
  signal \c$bv_92\                     : std_logic_vector(17 downto 0);
  signal result_46_selection_res       : boolean;
  signal \c$r'_46_projection\          : filters_types.tup2_0;
  signal \c$case_alt_46_selection_res\ : boolean;
  signal \c$bv_93\                     : std_logic_vector(17 downto 0);
  signal \c$bv_94\                     : std_logic_vector(17 downto 0);
  signal result_47_selection_res       : boolean;
  signal \c$r'_47_projection\          : filters_types.tup2_0;
  signal \c$case_alt_47_selection_res\ : boolean;
  signal \c$bv_95\                     : std_logic_vector(17 downto 0);
  signal \c$bv_96\                     : std_logic_vector(17 downto 0);
  signal result_48_selection_res       : boolean;
  signal \c$restF0_case_scrut_res\     : filters_types.array_of_signed_18(0 to 49);
  signal \c$vec\                       : filters_types.array_of_signed_18(0 to 99);
  signal \c$vec_0\                     : filters_types.array_of_signed_18(0 to 100);
  signal result_49_dc_arg              : filters_types.tup2_1;

begin
  result <= result_49.tup2_sel1_signed;

  r <= resize(result_2,19) + resize(result_1,19);

  \c$app_arg\ <= std_logic_vector(r);

  \c$r'_projection\ <= (\c$app_arg\(\c$app_arg\'high downto 18),\c$app_arg\(18-1 downto 0));

  \r'\ <= \c$r'_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv\ <= (std_logic_vector(result_2));

  \c$bv_0\ <= (std_logic_vector(result_1));

  \c$case_alt_selection_res\ <= (( \c$bv\(\c$bv\'high) ) and ( \c$bv_0\(\c$bv_0\'high) )) = '0';

  \c$case_alt\ <= to_signed(131071,18) when \c$case_alt_selection_res\ else
                  to_signed(-131072,18);

  result_0_selection_res <= (( \c$app_arg\(\c$app_arg\'high) ) xor ( \r'\(\r'\'high) )) = '0';

  result_0 <= signed(\r'\) when result_0_selection_res else
              \c$case_alt\;

  r_0 <= resize(result_4,19) + resize(result_3,19);

  \c$app_arg_0\ <= std_logic_vector(r_0);

  \c$r'_0_projection\ <= (\c$app_arg_0\(\c$app_arg_0\'high downto 18),\c$app_arg_0\(18-1 downto 0));

  \c$r'_0\ <= \c$r'_0_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_1\ <= (std_logic_vector(result_4));

  \c$bv_2\ <= (std_logic_vector(result_3));

  \c$case_alt_0_selection_res\ <= (( \c$bv_1\(\c$bv_1\'high) ) and ( \c$bv_2\(\c$bv_2\'high) )) = '0';

  \c$case_alt_0\ <= to_signed(131071,18) when \c$case_alt_0_selection_res\ else
                    to_signed(-131072,18);

  result_1_selection_res <= (( \c$app_arg_0\(\c$app_arg_0\'high) ) xor ( \c$r'_0\(\c$r'_0\'high) )) = '0';

  result_1 <= signed(\c$r'_0\) when result_1_selection_res else
              \c$case_alt_0\;

  r_1 <= resize(result_6,19) + resize(result_5,19);

  \c$app_arg_1\ <= std_logic_vector(r_1);

  \c$r'_1_projection\ <= (\c$app_arg_1\(\c$app_arg_1\'high downto 18),\c$app_arg_1\(18-1 downto 0));

  \c$r'_1\ <= \c$r'_1_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_3\ <= (std_logic_vector(result_6));

  \c$bv_4\ <= (std_logic_vector(result_5));

  \c$case_alt_1_selection_res\ <= (( \c$bv_3\(\c$bv_3\'high) ) and ( \c$bv_4\(\c$bv_4\'high) )) = '0';

  \c$case_alt_1\ <= to_signed(131071,18) when \c$case_alt_1_selection_res\ else
                    to_signed(-131072,18);

  result_2_selection_res <= (( \c$app_arg_1\(\c$app_arg_1\'high) ) xor ( \c$r'_1\(\c$r'_1\'high) )) = '0';

  result_2 <= signed(\c$r'_1\) when result_2_selection_res else
              \c$case_alt_1\;

  r_2 <= resize(result_8,19) + resize(result_7,19);

  \c$app_arg_2\ <= std_logic_vector(r_2);

  \c$r'_2_projection\ <= (\c$app_arg_2\(\c$app_arg_2\'high downto 18),\c$app_arg_2\(18-1 downto 0));

  \c$r'_2\ <= \c$r'_2_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_5\ <= (std_logic_vector(result_8));

  \c$bv_6\ <= (std_logic_vector(result_7));

  \c$case_alt_2_selection_res\ <= (( \c$bv_5\(\c$bv_5\'high) ) and ( \c$bv_6\(\c$bv_6\'high) )) = '0';

  \c$case_alt_2\ <= to_signed(131071,18) when \c$case_alt_2_selection_res\ else
                    to_signed(-131072,18);

  result_3_selection_res <= (( \c$app_arg_2\(\c$app_arg_2\'high) ) xor ( \c$r'_2\(\c$r'_2\'high) )) = '0';

  result_3 <= signed(\c$r'_2\) when result_3_selection_res else
              \c$case_alt_2\;

  r_3 <= resize(result_10,19) + resize(result_9,19);

  \c$app_arg_3\ <= std_logic_vector(r_3);

  \c$r'_3_projection\ <= (\c$app_arg_3\(\c$app_arg_3\'high downto 18),\c$app_arg_3\(18-1 downto 0));

  \c$r'_3\ <= \c$r'_3_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_7\ <= (std_logic_vector(result_10));

  \c$bv_8\ <= (std_logic_vector(result_9));

  \c$case_alt_3_selection_res\ <= (( \c$bv_7\(\c$bv_7\'high) ) and ( \c$bv_8\(\c$bv_8\'high) )) = '0';

  \c$case_alt_3\ <= to_signed(131071,18) when \c$case_alt_3_selection_res\ else
                    to_signed(-131072,18);

  result_4_selection_res <= (( \c$app_arg_3\(\c$app_arg_3\'high) ) xor ( \c$r'_3\(\c$r'_3\'high) )) = '0';

  result_4 <= signed(\c$r'_3\) when result_4_selection_res else
              \c$case_alt_3\;

  r_4 <= resize(result_12,19) + resize(result_11,19);

  \c$app_arg_4\ <= std_logic_vector(r_4);

  \c$r'_4_projection\ <= (\c$app_arg_4\(\c$app_arg_4\'high downto 18),\c$app_arg_4\(18-1 downto 0));

  \c$r'_4\ <= \c$r'_4_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_9\ <= (std_logic_vector(result_12));

  \c$bv_10\ <= (std_logic_vector(result_11));

  \c$case_alt_4_selection_res\ <= (( \c$bv_9\(\c$bv_9\'high) ) and ( \c$bv_10\(\c$bv_10\'high) )) = '0';

  \c$case_alt_4\ <= to_signed(131071,18) when \c$case_alt_4_selection_res\ else
                    to_signed(-131072,18);

  result_5_selection_res <= (( \c$app_arg_4\(\c$app_arg_4\'high) ) xor ( \c$r'_4\(\c$r'_4\'high) )) = '0';

  result_5 <= signed(\c$r'_4\) when result_5_selection_res else
              \c$case_alt_4\;

  r_5 <= resize(result_14,19) + resize(result_13,19);

  \c$app_arg_5\ <= std_logic_vector(r_5);

  \c$r'_5_projection\ <= (\c$app_arg_5\(\c$app_arg_5\'high downto 18),\c$app_arg_5\(18-1 downto 0));

  \c$r'_5\ <= \c$r'_5_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_11\ <= (std_logic_vector(result_14));

  \c$bv_12\ <= (std_logic_vector(result_13));

  \c$case_alt_5_selection_res\ <= (( \c$bv_11\(\c$bv_11\'high) ) and ( \c$bv_12\(\c$bv_12\'high) )) = '0';

  \c$case_alt_5\ <= to_signed(131071,18) when \c$case_alt_5_selection_res\ else
                    to_signed(-131072,18);

  result_6_selection_res <= (( \c$app_arg_5\(\c$app_arg_5\'high) ) xor ( \c$r'_5\(\c$r'_5\'high) )) = '0';

  result_6 <= signed(\c$r'_5\) when result_6_selection_res else
              \c$case_alt_5\;

  r_6 <= resize(result_16,19) + resize(result_15,19);

  \c$app_arg_6\ <= std_logic_vector(r_6);

  \c$r'_6_projection\ <= (\c$app_arg_6\(\c$app_arg_6\'high downto 18),\c$app_arg_6\(18-1 downto 0));

  \c$r'_6\ <= \c$r'_6_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_13\ <= (std_logic_vector(result_16));

  \c$bv_14\ <= (std_logic_vector(result_15));

  \c$case_alt_6_selection_res\ <= (( \c$bv_13\(\c$bv_13\'high) ) and ( \c$bv_14\(\c$bv_14\'high) )) = '0';

  \c$case_alt_6\ <= to_signed(131071,18) when \c$case_alt_6_selection_res\ else
                    to_signed(-131072,18);

  result_7_selection_res <= (( \c$app_arg_6\(\c$app_arg_6\'high) ) xor ( \c$r'_6\(\c$r'_6\'high) )) = '0';

  result_7 <= signed(\c$r'_6\) when result_7_selection_res else
              \c$case_alt_6\;

  r_7 <= resize(result_18,19) + resize(result_17,19);

  \c$app_arg_7\ <= std_logic_vector(r_7);

  \c$r'_7_projection\ <= (\c$app_arg_7\(\c$app_arg_7\'high downto 18),\c$app_arg_7\(18-1 downto 0));

  \c$r'_7\ <= \c$r'_7_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_15\ <= (std_logic_vector(result_18));

  \c$bv_16\ <= (std_logic_vector(result_17));

  \c$case_alt_7_selection_res\ <= (( \c$bv_15\(\c$bv_15\'high) ) and ( \c$bv_16\(\c$bv_16\'high) )) = '0';

  \c$case_alt_7\ <= to_signed(131071,18) when \c$case_alt_7_selection_res\ else
                    to_signed(-131072,18);

  result_8_selection_res <= (( \c$app_arg_7\(\c$app_arg_7\'high) ) xor ( \c$r'_7\(\c$r'_7\'high) )) = '0';

  result_8 <= signed(\c$r'_7\) when result_8_selection_res else
              \c$case_alt_7\;

  r_8 <= resize(result_20,19) + resize(result_19,19);

  \c$app_arg_8\ <= std_logic_vector(r_8);

  \c$r'_8_projection\ <= (\c$app_arg_8\(\c$app_arg_8\'high downto 18),\c$app_arg_8\(18-1 downto 0));

  \c$r'_8\ <= \c$r'_8_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_17\ <= (std_logic_vector(result_20));

  \c$bv_18\ <= (std_logic_vector(result_19));

  \c$case_alt_8_selection_res\ <= (( \c$bv_17\(\c$bv_17\'high) ) and ( \c$bv_18\(\c$bv_18\'high) )) = '0';

  \c$case_alt_8\ <= to_signed(131071,18) when \c$case_alt_8_selection_res\ else
                    to_signed(-131072,18);

  result_9_selection_res <= (( \c$app_arg_8\(\c$app_arg_8\'high) ) xor ( \c$r'_8\(\c$r'_8\'high) )) = '0';

  result_9 <= signed(\c$r'_8\) when result_9_selection_res else
              \c$case_alt_8\;

  r_9 <= resize(result_22,19) + resize(result_21,19);

  \c$app_arg_9\ <= std_logic_vector(r_9);

  \c$r'_9_projection\ <= (\c$app_arg_9\(\c$app_arg_9\'high downto 18),\c$app_arg_9\(18-1 downto 0));

  \c$r'_9\ <= \c$r'_9_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_19\ <= (std_logic_vector(result_22));

  \c$bv_20\ <= (std_logic_vector(result_21));

  \c$case_alt_9_selection_res\ <= (( \c$bv_19\(\c$bv_19\'high) ) and ( \c$bv_20\(\c$bv_20\'high) )) = '0';

  \c$case_alt_9\ <= to_signed(131071,18) when \c$case_alt_9_selection_res\ else
                    to_signed(-131072,18);

  result_10_selection_res <= (( \c$app_arg_9\(\c$app_arg_9\'high) ) xor ( \c$r'_9\(\c$r'_9\'high) )) = '0';

  result_10 <= signed(\c$r'_9\) when result_10_selection_res else
               \c$case_alt_9\;

  r_10 <= resize(result_24,19) + resize(result_23,19);

  \c$app_arg_10\ <= std_logic_vector(r_10);

  \c$r'_10_projection\ <= (\c$app_arg_10\(\c$app_arg_10\'high downto 18),\c$app_arg_10\(18-1 downto 0));

  \c$r'_10\ <= \c$r'_10_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_21\ <= (std_logic_vector(result_24));

  \c$bv_22\ <= (std_logic_vector(result_23));

  \c$case_alt_10_selection_res\ <= (( \c$bv_21\(\c$bv_21\'high) ) and ( \c$bv_22\(\c$bv_22\'high) )) = '0';

  \c$case_alt_10\ <= to_signed(131071,18) when \c$case_alt_10_selection_res\ else
                     to_signed(-131072,18);

  result_11_selection_res <= (( \c$app_arg_10\(\c$app_arg_10\'high) ) xor ( \c$r'_10\(\c$r'_10\'high) )) = '0';

  result_11 <= signed(\c$r'_10\) when result_11_selection_res else
               \c$case_alt_10\;

  r_11 <= resize(result_26,19) + resize(result_25,19);

  \c$app_arg_11\ <= std_logic_vector(r_11);

  \c$r'_11_projection\ <= (\c$app_arg_11\(\c$app_arg_11\'high downto 18),\c$app_arg_11\(18-1 downto 0));

  \c$r'_11\ <= \c$r'_11_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_23\ <= (std_logic_vector(result_26));

  \c$bv_24\ <= (std_logic_vector(result_25));

  \c$case_alt_11_selection_res\ <= (( \c$bv_23\(\c$bv_23\'high) ) and ( \c$bv_24\(\c$bv_24\'high) )) = '0';

  \c$case_alt_11\ <= to_signed(131071,18) when \c$case_alt_11_selection_res\ else
                     to_signed(-131072,18);

  result_12_selection_res <= (( \c$app_arg_11\(\c$app_arg_11\'high) ) xor ( \c$r'_11\(\c$r'_11\'high) )) = '0';

  result_12 <= signed(\c$r'_11\) when result_12_selection_res else
               \c$case_alt_11\;

  r_12 <= resize(result_28,19) + resize(result_27,19);

  \c$app_arg_12\ <= std_logic_vector(r_12);

  \c$r'_12_projection\ <= (\c$app_arg_12\(\c$app_arg_12\'high downto 18),\c$app_arg_12\(18-1 downto 0));

  \c$r'_12\ <= \c$r'_12_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_25\ <= (std_logic_vector(result_28));

  \c$bv_26\ <= (std_logic_vector(result_27));

  \c$case_alt_12_selection_res\ <= (( \c$bv_25\(\c$bv_25\'high) ) and ( \c$bv_26\(\c$bv_26\'high) )) = '0';

  \c$case_alt_12\ <= to_signed(131071,18) when \c$case_alt_12_selection_res\ else
                     to_signed(-131072,18);

  result_13_selection_res <= (( \c$app_arg_12\(\c$app_arg_12\'high) ) xor ( \c$r'_12\(\c$r'_12\'high) )) = '0';

  result_13 <= signed(\c$r'_12\) when result_13_selection_res else
               \c$case_alt_12\;

  r_13 <= resize(result_30,19) + resize(result_29,19);

  \c$app_arg_13\ <= std_logic_vector(r_13);

  \c$r'_13_projection\ <= (\c$app_arg_13\(\c$app_arg_13\'high downto 18),\c$app_arg_13\(18-1 downto 0));

  \c$r'_13\ <= \c$r'_13_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_27\ <= (std_logic_vector(result_30));

  \c$bv_28\ <= (std_logic_vector(result_29));

  \c$case_alt_13_selection_res\ <= (( \c$bv_27\(\c$bv_27\'high) ) and ( \c$bv_28\(\c$bv_28\'high) )) = '0';

  \c$case_alt_13\ <= to_signed(131071,18) when \c$case_alt_13_selection_res\ else
                     to_signed(-131072,18);

  result_14_selection_res <= (( \c$app_arg_13\(\c$app_arg_13\'high) ) xor ( \c$r'_13\(\c$r'_13\'high) )) = '0';

  result_14 <= signed(\c$r'_13\) when result_14_selection_res else
               \c$case_alt_13\;

  r_14 <= resize(result_31,19) + resize(result_32,19);

  \c$app_arg_14\ <= std_logic_vector(r_14);

  \c$r'_14_projection\ <= (\c$app_arg_14\(\c$app_arg_14\'high downto 18),\c$app_arg_14\(18-1 downto 0));

  \c$r'_14\ <= \c$r'_14_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_29\ <= (std_logic_vector(result_31));

  \c$bv_30\ <= (std_logic_vector(result_32));

  \c$case_alt_14_selection_res\ <= (( \c$bv_29\(\c$bv_29\'high) ) and ( \c$bv_30\(\c$bv_30\'high) )) = '0';

  \c$case_alt_14\ <= to_signed(131071,18) when \c$case_alt_14_selection_res\ else
                     to_signed(-131072,18);

  result_15_selection_res <= (( \c$app_arg_14\(\c$app_arg_14\'high) ) xor ( \c$r'_14\(\c$r'_14\'high) )) = '0';

  result_15 <= signed(\c$r'_14\) when result_15_selection_res else
               \c$case_alt_14\;

  r_15 <= resize(\c$restF0_case_scrut\(43),19) + resize(result_33,19);

  \c$app_arg_15\ <= std_logic_vector(r_15);

  \c$r'_15_projection\ <= (\c$app_arg_15\(\c$app_arg_15\'high downto 18),\c$app_arg_15\(18-1 downto 0));

  \c$r'_15\ <= \c$r'_15_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_31\ <= (std_logic_vector(\c$restF0_case_scrut\(43)));

  \c$bv_32\ <= (std_logic_vector(result_33));

  \c$case_alt_15_selection_res\ <= (( \c$bv_31\(\c$bv_31\'high) ) and ( \c$bv_32\(\c$bv_32\'high) )) = '0';

  \c$case_alt_15\ <= to_signed(131071,18) when \c$case_alt_15_selection_res\ else
                     to_signed(-131072,18);

  result_16_selection_res <= (( \c$app_arg_15\(\c$app_arg_15\'high) ) xor ( \c$r'_15\(\c$r'_15\'high) )) = '0';

  result_16 <= signed(\c$r'_15\) when result_16_selection_res else
               \c$case_alt_15\;

  r_16 <= resize(\c$restF0_case_scrut\(40),19) + resize(result_34,19);

  \c$app_arg_16\ <= std_logic_vector(r_16);

  \c$r'_16_projection\ <= (\c$app_arg_16\(\c$app_arg_16\'high downto 18),\c$app_arg_16\(18-1 downto 0));

  \c$r'_16\ <= \c$r'_16_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_33\ <= (std_logic_vector(\c$restF0_case_scrut\(40)));

  \c$bv_34\ <= (std_logic_vector(result_34));

  \c$case_alt_16_selection_res\ <= (( \c$bv_33\(\c$bv_33\'high) ) and ( \c$bv_34\(\c$bv_34\'high) )) = '0';

  \c$case_alt_16\ <= to_signed(131071,18) when \c$case_alt_16_selection_res\ else
                     to_signed(-131072,18);

  result_17_selection_res <= (( \c$app_arg_16\(\c$app_arg_16\'high) ) xor ( \c$r'_16\(\c$r'_16\'high) )) = '0';

  result_17 <= signed(\c$r'_16\) when result_17_selection_res else
               \c$case_alt_16\;

  r_17 <= resize(\c$restF0_case_scrut\(37),19) + resize(result_35,19);

  \c$app_arg_17\ <= std_logic_vector(r_17);

  \c$r'_17_projection\ <= (\c$app_arg_17\(\c$app_arg_17\'high downto 18),\c$app_arg_17\(18-1 downto 0));

  \c$r'_17\ <= \c$r'_17_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_35\ <= (std_logic_vector(\c$restF0_case_scrut\(37)));

  \c$bv_36\ <= (std_logic_vector(result_35));

  \c$case_alt_17_selection_res\ <= (( \c$bv_35\(\c$bv_35\'high) ) and ( \c$bv_36\(\c$bv_36\'high) )) = '0';

  \c$case_alt_17\ <= to_signed(131071,18) when \c$case_alt_17_selection_res\ else
                     to_signed(-131072,18);

  result_18_selection_res <= (( \c$app_arg_17\(\c$app_arg_17\'high) ) xor ( \c$r'_17\(\c$r'_17\'high) )) = '0';

  result_18 <= signed(\c$r'_17\) when result_18_selection_res else
               \c$case_alt_17\;

  r_18 <= resize(\c$restF0_case_scrut\(34),19) + resize(result_36,19);

  \c$app_arg_18\ <= std_logic_vector(r_18);

  \c$r'_18_projection\ <= (\c$app_arg_18\(\c$app_arg_18\'high downto 18),\c$app_arg_18\(18-1 downto 0));

  \c$r'_18\ <= \c$r'_18_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_37\ <= (std_logic_vector(\c$restF0_case_scrut\(34)));

  \c$bv_38\ <= (std_logic_vector(result_36));

  \c$case_alt_18_selection_res\ <= (( \c$bv_37\(\c$bv_37\'high) ) and ( \c$bv_38\(\c$bv_38\'high) )) = '0';

  \c$case_alt_18\ <= to_signed(131071,18) when \c$case_alt_18_selection_res\ else
                     to_signed(-131072,18);

  result_19_selection_res <= (( \c$app_arg_18\(\c$app_arg_18\'high) ) xor ( \c$r'_18\(\c$r'_18\'high) )) = '0';

  result_19 <= signed(\c$r'_18\) when result_19_selection_res else
               \c$case_alt_18\;

  r_19 <= resize(\c$restF0_case_scrut\(31),19) + resize(result_37,19);

  \c$app_arg_19\ <= std_logic_vector(r_19);

  \c$r'_19_projection\ <= (\c$app_arg_19\(\c$app_arg_19\'high downto 18),\c$app_arg_19\(18-1 downto 0));

  \c$r'_19\ <= \c$r'_19_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_39\ <= (std_logic_vector(\c$restF0_case_scrut\(31)));

  \c$bv_40\ <= (std_logic_vector(result_37));

  \c$case_alt_19_selection_res\ <= (( \c$bv_39\(\c$bv_39\'high) ) and ( \c$bv_40\(\c$bv_40\'high) )) = '0';

  \c$case_alt_19\ <= to_signed(131071,18) when \c$case_alt_19_selection_res\ else
                     to_signed(-131072,18);

  result_20_selection_res <= (( \c$app_arg_19\(\c$app_arg_19\'high) ) xor ( \c$r'_19\(\c$r'_19\'high) )) = '0';

  result_20 <= signed(\c$r'_19\) when result_20_selection_res else
               \c$case_alt_19\;

  r_20 <= resize(\c$restF0_case_scrut\(28),19) + resize(result_38,19);

  \c$app_arg_20\ <= std_logic_vector(r_20);

  \c$r'_20_projection\ <= (\c$app_arg_20\(\c$app_arg_20\'high downto 18),\c$app_arg_20\(18-1 downto 0));

  \c$r'_20\ <= \c$r'_20_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_41\ <= (std_logic_vector(\c$restF0_case_scrut\(28)));

  \c$bv_42\ <= (std_logic_vector(result_38));

  \c$case_alt_20_selection_res\ <= (( \c$bv_41\(\c$bv_41\'high) ) and ( \c$bv_42\(\c$bv_42\'high) )) = '0';

  \c$case_alt_20\ <= to_signed(131071,18) when \c$case_alt_20_selection_res\ else
                     to_signed(-131072,18);

  result_21_selection_res <= (( \c$app_arg_20\(\c$app_arg_20\'high) ) xor ( \c$r'_20\(\c$r'_20\'high) )) = '0';

  result_21 <= signed(\c$r'_20\) when result_21_selection_res else
               \c$case_alt_20\;

  r_21 <= resize(\c$restF0_case_scrut\(25),19) + resize(result_39,19);

  \c$app_arg_21\ <= std_logic_vector(r_21);

  \c$r'_21_projection\ <= (\c$app_arg_21\(\c$app_arg_21\'high downto 18),\c$app_arg_21\(18-1 downto 0));

  \c$r'_21\ <= \c$r'_21_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_43\ <= (std_logic_vector(\c$restF0_case_scrut\(25)));

  \c$bv_44\ <= (std_logic_vector(result_39));

  \c$case_alt_21_selection_res\ <= (( \c$bv_43\(\c$bv_43\'high) ) and ( \c$bv_44\(\c$bv_44\'high) )) = '0';

  \c$case_alt_21\ <= to_signed(131071,18) when \c$case_alt_21_selection_res\ else
                     to_signed(-131072,18);

  result_22_selection_res <= (( \c$app_arg_21\(\c$app_arg_21\'high) ) xor ( \c$r'_21\(\c$r'_21\'high) )) = '0';

  result_22 <= signed(\c$r'_21\) when result_22_selection_res else
               \c$case_alt_21\;

  r_22 <= resize(result_40,19) + resize(result_41,19);

  \c$app_arg_22\ <= std_logic_vector(r_22);

  \c$r'_22_projection\ <= (\c$app_arg_22\(\c$app_arg_22\'high downto 18),\c$app_arg_22\(18-1 downto 0));

  \c$r'_22\ <= \c$r'_22_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_45\ <= (std_logic_vector(result_40));

  \c$bv_46\ <= (std_logic_vector(result_41));

  \c$case_alt_22_selection_res\ <= (( \c$bv_45\(\c$bv_45\'high) ) and ( \c$bv_46\(\c$bv_46\'high) )) = '0';

  \c$case_alt_22\ <= to_signed(131071,18) when \c$case_alt_22_selection_res\ else
                     to_signed(-131072,18);

  result_23_selection_res <= (( \c$app_arg_22\(\c$app_arg_22\'high) ) xor ( \c$r'_22\(\c$r'_22\'high) )) = '0';

  result_23 <= signed(\c$r'_22\) when result_23_selection_res else
               \c$case_alt_22\;

  r_23 <= resize(\c$restF0_case_scrut\(18),19) + resize(result_42,19);

  \c$app_arg_23\ <= std_logic_vector(r_23);

  \c$r'_23_projection\ <= (\c$app_arg_23\(\c$app_arg_23\'high downto 18),\c$app_arg_23\(18-1 downto 0));

  \c$r'_23\ <= \c$r'_23_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_47\ <= (std_logic_vector(\c$restF0_case_scrut\(18)));

  \c$bv_48\ <= (std_logic_vector(result_42));

  \c$case_alt_23_selection_res\ <= (( \c$bv_47\(\c$bv_47\'high) ) and ( \c$bv_48\(\c$bv_48\'high) )) = '0';

  \c$case_alt_23\ <= to_signed(131071,18) when \c$case_alt_23_selection_res\ else
                     to_signed(-131072,18);

  result_24_selection_res <= (( \c$app_arg_23\(\c$app_arg_23\'high) ) xor ( \c$r'_23\(\c$r'_23\'high) )) = '0';

  result_24 <= signed(\c$r'_23\) when result_24_selection_res else
               \c$case_alt_23\;

  r_24 <= resize(\c$restF0_case_scrut\(15),19) + resize(result_43,19);

  \c$app_arg_24\ <= std_logic_vector(r_24);

  \c$r'_24_projection\ <= (\c$app_arg_24\(\c$app_arg_24\'high downto 18),\c$app_arg_24\(18-1 downto 0));

  \c$r'_24\ <= \c$r'_24_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_49\ <= (std_logic_vector(\c$restF0_case_scrut\(15)));

  \c$bv_50\ <= (std_logic_vector(result_43));

  \c$case_alt_24_selection_res\ <= (( \c$bv_49\(\c$bv_49\'high) ) and ( \c$bv_50\(\c$bv_50\'high) )) = '0';

  \c$case_alt_24\ <= to_signed(131071,18) when \c$case_alt_24_selection_res\ else
                     to_signed(-131072,18);

  result_25_selection_res <= (( \c$app_arg_24\(\c$app_arg_24\'high) ) xor ( \c$r'_24\(\c$r'_24\'high) )) = '0';

  result_25 <= signed(\c$r'_24\) when result_25_selection_res else
               \c$case_alt_24\;

  r_25 <= resize(\c$restF0_case_scrut\(12),19) + resize(result_44,19);

  \c$app_arg_25\ <= std_logic_vector(r_25);

  \c$r'_25_projection\ <= (\c$app_arg_25\(\c$app_arg_25\'high downto 18),\c$app_arg_25\(18-1 downto 0));

  \c$r'_25\ <= \c$r'_25_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_51\ <= (std_logic_vector(\c$restF0_case_scrut\(12)));

  \c$bv_52\ <= (std_logic_vector(result_44));

  \c$case_alt_25_selection_res\ <= (( \c$bv_51\(\c$bv_51\'high) ) and ( \c$bv_52\(\c$bv_52\'high) )) = '0';

  \c$case_alt_25\ <= to_signed(131071,18) when \c$case_alt_25_selection_res\ else
                     to_signed(-131072,18);

  result_26_selection_res <= (( \c$app_arg_25\(\c$app_arg_25\'high) ) xor ( \c$r'_25\(\c$r'_25\'high) )) = '0';

  result_26 <= signed(\c$r'_25\) when result_26_selection_res else
               \c$case_alt_25\;

  r_26 <= resize(\c$restF0_case_scrut\(9),19) + resize(result_45,19);

  \c$app_arg_26\ <= std_logic_vector(r_26);

  \c$r'_26_projection\ <= (\c$app_arg_26\(\c$app_arg_26\'high downto 18),\c$app_arg_26\(18-1 downto 0));

  \c$r'_26\ <= \c$r'_26_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_53\ <= (std_logic_vector(\c$restF0_case_scrut\(9)));

  \c$bv_54\ <= (std_logic_vector(result_45));

  \c$case_alt_26_selection_res\ <= (( \c$bv_53\(\c$bv_53\'high) ) and ( \c$bv_54\(\c$bv_54\'high) )) = '0';

  \c$case_alt_26\ <= to_signed(131071,18) when \c$case_alt_26_selection_res\ else
                     to_signed(-131072,18);

  result_27_selection_res <= (( \c$app_arg_26\(\c$app_arg_26\'high) ) xor ( \c$r'_26\(\c$r'_26\'high) )) = '0';

  result_27 <= signed(\c$r'_26\) when result_27_selection_res else
               \c$case_alt_26\;

  r_27 <= resize(\c$restF0_case_scrut\(6),19) + resize(result_46,19);

  \c$app_arg_27\ <= std_logic_vector(r_27);

  \c$r'_27_projection\ <= (\c$app_arg_27\(\c$app_arg_27\'high downto 18),\c$app_arg_27\(18-1 downto 0));

  \c$r'_27\ <= \c$r'_27_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_55\ <= (std_logic_vector(\c$restF0_case_scrut\(6)));

  \c$bv_56\ <= (std_logic_vector(result_46));

  \c$case_alt_27_selection_res\ <= (( \c$bv_55\(\c$bv_55\'high) ) and ( \c$bv_56\(\c$bv_56\'high) )) = '0';

  \c$case_alt_27\ <= to_signed(131071,18) when \c$case_alt_27_selection_res\ else
                     to_signed(-131072,18);

  result_28_selection_res <= (( \c$app_arg_27\(\c$app_arg_27\'high) ) xor ( \c$r'_27\(\c$r'_27\'high) )) = '0';

  result_28 <= signed(\c$r'_27\) when result_28_selection_res else
               \c$case_alt_27\;

  r_28 <= resize(\c$restF0_case_scrut\(3),19) + resize(result_47,19);

  \c$app_arg_28\ <= std_logic_vector(r_28);

  \c$r'_28_projection\ <= (\c$app_arg_28\(\c$app_arg_28\'high downto 18),\c$app_arg_28\(18-1 downto 0));

  \c$r'_28\ <= \c$r'_28_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_57\ <= (std_logic_vector(\c$restF0_case_scrut\(3)));

  \c$bv_58\ <= (std_logic_vector(result_47));

  \c$case_alt_28_selection_res\ <= (( \c$bv_57\(\c$bv_57\'high) ) and ( \c$bv_58\(\c$bv_58\'high) )) = '0';

  \c$case_alt_28\ <= to_signed(131071,18) when \c$case_alt_28_selection_res\ else
                     to_signed(-131072,18);

  result_29_selection_res <= (( \c$app_arg_28\(\c$app_arg_28\'high) ) xor ( \c$r'_28\(\c$r'_28\'high) )) = '0';

  result_29 <= signed(\c$r'_28\) when result_29_selection_res else
               \c$case_alt_28\;

  r_29 <= resize(\c$restF0_case_scrut\(0),19) + resize(result_48,19);

  \c$app_arg_29\ <= std_logic_vector(r_29);

  \c$r'_29_projection\ <= (\c$app_arg_29\(\c$app_arg_29\'high downto 18),\c$app_arg_29\(18-1 downto 0));

  \c$r'_29\ <= \c$r'_29_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_59\ <= (std_logic_vector(\c$restF0_case_scrut\(0)));

  \c$bv_60\ <= (std_logic_vector(result_48));

  \c$case_alt_29_selection_res\ <= (( \c$bv_59\(\c$bv_59\'high) ) and ( \c$bv_60\(\c$bv_60\'high) )) = '0';

  \c$case_alt_29\ <= to_signed(131071,18) when \c$case_alt_29_selection_res\ else
                     to_signed(-131072,18);

  result_30_selection_res <= (( \c$app_arg_29\(\c$app_arg_29\'high) ) xor ( \c$r'_29\(\c$r'_29\'high) )) = '0';

  result_30 <= signed(\c$r'_29\) when result_30_selection_res else
               \c$case_alt_29\;

  r_30 <= resize(\c$restF0_case_scrut\(46),19) + resize(\c$restF0_case_scrut\(47),19);

  \c$app_arg_30\ <= std_logic_vector(r_30);

  \c$r'_30_projection\ <= (\c$app_arg_30\(\c$app_arg_30\'high downto 18),\c$app_arg_30\(18-1 downto 0));

  \c$r'_30\ <= \c$r'_30_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_61\ <= (std_logic_vector(\c$restF0_case_scrut\(46)));

  \c$bv_62\ <= (std_logic_vector(\c$restF0_case_scrut\(47)));

  \c$case_alt_30_selection_res\ <= (( \c$bv_61\(\c$bv_61\'high) ) and ( \c$bv_62\(\c$bv_62\'high) )) = '0';

  \c$case_alt_30\ <= to_signed(131071,18) when \c$case_alt_30_selection_res\ else
                     to_signed(-131072,18);

  result_31_selection_res <= (( \c$app_arg_30\(\c$app_arg_30\'high) ) xor ( \c$r'_30\(\c$r'_30\'high) )) = '0';

  result_31 <= signed(\c$r'_30\) when result_31_selection_res else
               \c$case_alt_30\;

  r_31 <= resize(\c$restF0_case_scrut\(48),19) + resize(\c$restF0_case_scrut\(49),19);

  \c$app_arg_31\ <= std_logic_vector(r_31);

  \c$r'_31_projection\ <= (\c$app_arg_31\(\c$app_arg_31\'high downto 18),\c$app_arg_31\(18-1 downto 0));

  \c$r'_31\ <= \c$r'_31_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_63\ <= (std_logic_vector(\c$restF0_case_scrut\(48)));

  \c$bv_64\ <= (std_logic_vector(\c$restF0_case_scrut\(49)));

  \c$case_alt_31_selection_res\ <= (( \c$bv_63\(\c$bv_63\'high) ) and ( \c$bv_64\(\c$bv_64\'high) )) = '0';

  \c$case_alt_31\ <= to_signed(131071,18) when \c$case_alt_31_selection_res\ else
                     to_signed(-131072,18);

  result_32_selection_res <= (( \c$app_arg_31\(\c$app_arg_31\'high) ) xor ( \c$r'_31\(\c$r'_31\'high) )) = '0';

  result_32 <= signed(\c$r'_31\) when result_32_selection_res else
               \c$case_alt_31\;

  r_32 <= resize(\c$restF0_case_scrut\(44),19) + resize(\c$restF0_case_scrut\(45),19);

  \c$app_arg_32\ <= std_logic_vector(r_32);

  \c$r'_32_projection\ <= (\c$app_arg_32\(\c$app_arg_32\'high downto 18),\c$app_arg_32\(18-1 downto 0));

  \c$r'_32\ <= \c$r'_32_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_65\ <= (std_logic_vector(\c$restF0_case_scrut\(44)));

  \c$bv_66\ <= (std_logic_vector(\c$restF0_case_scrut\(45)));

  \c$case_alt_32_selection_res\ <= (( \c$bv_65\(\c$bv_65\'high) ) and ( \c$bv_66\(\c$bv_66\'high) )) = '0';

  \c$case_alt_32\ <= to_signed(131071,18) when \c$case_alt_32_selection_res\ else
                     to_signed(-131072,18);

  result_33_selection_res <= (( \c$app_arg_32\(\c$app_arg_32\'high) ) xor ( \c$r'_32\(\c$r'_32\'high) )) = '0';

  result_33 <= signed(\c$r'_32\) when result_33_selection_res else
               \c$case_alt_32\;

  r_33 <= resize(\c$restF0_case_scrut\(41),19) + resize(\c$restF0_case_scrut\(42),19);

  \c$app_arg_33\ <= std_logic_vector(r_33);

  \c$r'_33_projection\ <= (\c$app_arg_33\(\c$app_arg_33\'high downto 18),\c$app_arg_33\(18-1 downto 0));

  \c$r'_33\ <= \c$r'_33_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_67\ <= (std_logic_vector(\c$restF0_case_scrut\(41)));

  \c$bv_68\ <= (std_logic_vector(\c$restF0_case_scrut\(42)));

  \c$case_alt_33_selection_res\ <= (( \c$bv_67\(\c$bv_67\'high) ) and ( \c$bv_68\(\c$bv_68\'high) )) = '0';

  \c$case_alt_33\ <= to_signed(131071,18) when \c$case_alt_33_selection_res\ else
                     to_signed(-131072,18);

  result_34_selection_res <= (( \c$app_arg_33\(\c$app_arg_33\'high) ) xor ( \c$r'_33\(\c$r'_33\'high) )) = '0';

  result_34 <= signed(\c$r'_33\) when result_34_selection_res else
               \c$case_alt_33\;

  r_34 <= resize(\c$restF0_case_scrut\(38),19) + resize(\c$restF0_case_scrut\(39),19);

  \c$app_arg_34\ <= std_logic_vector(r_34);

  \c$r'_34_projection\ <= (\c$app_arg_34\(\c$app_arg_34\'high downto 18),\c$app_arg_34\(18-1 downto 0));

  \c$r'_34\ <= \c$r'_34_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_69\ <= (std_logic_vector(\c$restF0_case_scrut\(38)));

  \c$bv_70\ <= (std_logic_vector(\c$restF0_case_scrut\(39)));

  \c$case_alt_34_selection_res\ <= (( \c$bv_69\(\c$bv_69\'high) ) and ( \c$bv_70\(\c$bv_70\'high) )) = '0';

  \c$case_alt_34\ <= to_signed(131071,18) when \c$case_alt_34_selection_res\ else
                     to_signed(-131072,18);

  result_35_selection_res <= (( \c$app_arg_34\(\c$app_arg_34\'high) ) xor ( \c$r'_34\(\c$r'_34\'high) )) = '0';

  result_35 <= signed(\c$r'_34\) when result_35_selection_res else
               \c$case_alt_34\;

  r_35 <= resize(\c$restF0_case_scrut\(35),19) + resize(\c$restF0_case_scrut\(36),19);

  \c$app_arg_35\ <= std_logic_vector(r_35);

  \c$r'_35_projection\ <= (\c$app_arg_35\(\c$app_arg_35\'high downto 18),\c$app_arg_35\(18-1 downto 0));

  \c$r'_35\ <= \c$r'_35_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_71\ <= (std_logic_vector(\c$restF0_case_scrut\(35)));

  \c$bv_72\ <= (std_logic_vector(\c$restF0_case_scrut\(36)));

  \c$case_alt_35_selection_res\ <= (( \c$bv_71\(\c$bv_71\'high) ) and ( \c$bv_72\(\c$bv_72\'high) )) = '0';

  \c$case_alt_35\ <= to_signed(131071,18) when \c$case_alt_35_selection_res\ else
                     to_signed(-131072,18);

  result_36_selection_res <= (( \c$app_arg_35\(\c$app_arg_35\'high) ) xor ( \c$r'_35\(\c$r'_35\'high) )) = '0';

  result_36 <= signed(\c$r'_35\) when result_36_selection_res else
               \c$case_alt_35\;

  r_36 <= resize(\c$restF0_case_scrut\(32),19) + resize(\c$restF0_case_scrut\(33),19);

  \c$app_arg_36\ <= std_logic_vector(r_36);

  \c$r'_36_projection\ <= (\c$app_arg_36\(\c$app_arg_36\'high downto 18),\c$app_arg_36\(18-1 downto 0));

  \c$r'_36\ <= \c$r'_36_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_73\ <= (std_logic_vector(\c$restF0_case_scrut\(32)));

  \c$bv_74\ <= (std_logic_vector(\c$restF0_case_scrut\(33)));

  \c$case_alt_36_selection_res\ <= (( \c$bv_73\(\c$bv_73\'high) ) and ( \c$bv_74\(\c$bv_74\'high) )) = '0';

  \c$case_alt_36\ <= to_signed(131071,18) when \c$case_alt_36_selection_res\ else
                     to_signed(-131072,18);

  result_37_selection_res <= (( \c$app_arg_36\(\c$app_arg_36\'high) ) xor ( \c$r'_36\(\c$r'_36\'high) )) = '0';

  result_37 <= signed(\c$r'_36\) when result_37_selection_res else
               \c$case_alt_36\;

  r_37 <= resize(\c$restF0_case_scrut\(29),19) + resize(\c$restF0_case_scrut\(30),19);

  \c$app_arg_37\ <= std_logic_vector(r_37);

  \c$r'_37_projection\ <= (\c$app_arg_37\(\c$app_arg_37\'high downto 18),\c$app_arg_37\(18-1 downto 0));

  \c$r'_37\ <= \c$r'_37_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_75\ <= (std_logic_vector(\c$restF0_case_scrut\(29)));

  \c$bv_76\ <= (std_logic_vector(\c$restF0_case_scrut\(30)));

  \c$case_alt_37_selection_res\ <= (( \c$bv_75\(\c$bv_75\'high) ) and ( \c$bv_76\(\c$bv_76\'high) )) = '0';

  \c$case_alt_37\ <= to_signed(131071,18) when \c$case_alt_37_selection_res\ else
                     to_signed(-131072,18);

  result_38_selection_res <= (( \c$app_arg_37\(\c$app_arg_37\'high) ) xor ( \c$r'_37\(\c$r'_37\'high) )) = '0';

  result_38 <= signed(\c$r'_37\) when result_38_selection_res else
               \c$case_alt_37\;

  r_38 <= resize(\c$restF0_case_scrut\(26),19) + resize(\c$restF0_case_scrut\(27),19);

  \c$app_arg_38\ <= std_logic_vector(r_38);

  \c$r'_38_projection\ <= (\c$app_arg_38\(\c$app_arg_38\'high downto 18),\c$app_arg_38\(18-1 downto 0));

  \c$r'_38\ <= \c$r'_38_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_77\ <= (std_logic_vector(\c$restF0_case_scrut\(26)));

  \c$bv_78\ <= (std_logic_vector(\c$restF0_case_scrut\(27)));

  \c$case_alt_38_selection_res\ <= (( \c$bv_77\(\c$bv_77\'high) ) and ( \c$bv_78\(\c$bv_78\'high) )) = '0';

  \c$case_alt_38\ <= to_signed(131071,18) when \c$case_alt_38_selection_res\ else
                     to_signed(-131072,18);

  result_39_selection_res <= (( \c$app_arg_38\(\c$app_arg_38\'high) ) xor ( \c$r'_38\(\c$r'_38\'high) )) = '0';

  result_39 <= signed(\c$r'_38\) when result_39_selection_res else
               \c$case_alt_38\;

  r_39 <= resize(\c$restF0_case_scrut\(21),19) + resize(\c$restF0_case_scrut\(22),19);

  \c$app_arg_39\ <= std_logic_vector(r_39);

  \c$r'_39_projection\ <= (\c$app_arg_39\(\c$app_arg_39\'high downto 18),\c$app_arg_39\(18-1 downto 0));

  \c$r'_39\ <= \c$r'_39_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_79\ <= (std_logic_vector(\c$restF0_case_scrut\(21)));

  \c$bv_80\ <= (std_logic_vector(\c$restF0_case_scrut\(22)));

  \c$case_alt_39_selection_res\ <= (( \c$bv_79\(\c$bv_79\'high) ) and ( \c$bv_80\(\c$bv_80\'high) )) = '0';

  \c$case_alt_39\ <= to_signed(131071,18) when \c$case_alt_39_selection_res\ else
                     to_signed(-131072,18);

  result_40_selection_res <= (( \c$app_arg_39\(\c$app_arg_39\'high) ) xor ( \c$r'_39\(\c$r'_39\'high) )) = '0';

  result_40 <= signed(\c$r'_39\) when result_40_selection_res else
               \c$case_alt_39\;

  r_40 <= resize(\c$restF0_case_scrut\(23),19) + resize(\c$restF0_case_scrut\(24),19);

  \c$app_arg_40\ <= std_logic_vector(r_40);

  \c$r'_40_projection\ <= (\c$app_arg_40\(\c$app_arg_40\'high downto 18),\c$app_arg_40\(18-1 downto 0));

  \c$r'_40\ <= \c$r'_40_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_81\ <= (std_logic_vector(\c$restF0_case_scrut\(23)));

  \c$bv_82\ <= (std_logic_vector(\c$restF0_case_scrut\(24)));

  \c$case_alt_40_selection_res\ <= (( \c$bv_81\(\c$bv_81\'high) ) and ( \c$bv_82\(\c$bv_82\'high) )) = '0';

  \c$case_alt_40\ <= to_signed(131071,18) when \c$case_alt_40_selection_res\ else
                     to_signed(-131072,18);

  result_41_selection_res <= (( \c$app_arg_40\(\c$app_arg_40\'high) ) xor ( \c$r'_40\(\c$r'_40\'high) )) = '0';

  result_41 <= signed(\c$r'_40\) when result_41_selection_res else
               \c$case_alt_40\;

  r_41 <= resize(\c$restF0_case_scrut\(19),19) + resize(\c$restF0_case_scrut\(20),19);

  \c$app_arg_41\ <= std_logic_vector(r_41);

  \c$r'_41_projection\ <= (\c$app_arg_41\(\c$app_arg_41\'high downto 18),\c$app_arg_41\(18-1 downto 0));

  \c$r'_41\ <= \c$r'_41_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_83\ <= (std_logic_vector(\c$restF0_case_scrut\(19)));

  \c$bv_84\ <= (std_logic_vector(\c$restF0_case_scrut\(20)));

  \c$case_alt_41_selection_res\ <= (( \c$bv_83\(\c$bv_83\'high) ) and ( \c$bv_84\(\c$bv_84\'high) )) = '0';

  \c$case_alt_41\ <= to_signed(131071,18) when \c$case_alt_41_selection_res\ else
                     to_signed(-131072,18);

  result_42_selection_res <= (( \c$app_arg_41\(\c$app_arg_41\'high) ) xor ( \c$r'_41\(\c$r'_41\'high) )) = '0';

  result_42 <= signed(\c$r'_41\) when result_42_selection_res else
               \c$case_alt_41\;

  r_42 <= resize(\c$restF0_case_scrut\(16),19) + resize(\c$restF0_case_scrut\(17),19);

  \c$app_arg_42\ <= std_logic_vector(r_42);

  \c$r'_42_projection\ <= (\c$app_arg_42\(\c$app_arg_42\'high downto 18),\c$app_arg_42\(18-1 downto 0));

  \c$r'_42\ <= \c$r'_42_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_85\ <= (std_logic_vector(\c$restF0_case_scrut\(16)));

  \c$bv_86\ <= (std_logic_vector(\c$restF0_case_scrut\(17)));

  \c$case_alt_42_selection_res\ <= (( \c$bv_85\(\c$bv_85\'high) ) and ( \c$bv_86\(\c$bv_86\'high) )) = '0';

  \c$case_alt_42\ <= to_signed(131071,18) when \c$case_alt_42_selection_res\ else
                     to_signed(-131072,18);

  result_43_selection_res <= (( \c$app_arg_42\(\c$app_arg_42\'high) ) xor ( \c$r'_42\(\c$r'_42\'high) )) = '0';

  result_43 <= signed(\c$r'_42\) when result_43_selection_res else
               \c$case_alt_42\;

  r_43 <= resize(\c$restF0_case_scrut\(13),19) + resize(\c$restF0_case_scrut\(14),19);

  \c$app_arg_43\ <= std_logic_vector(r_43);

  \c$r'_43_projection\ <= (\c$app_arg_43\(\c$app_arg_43\'high downto 18),\c$app_arg_43\(18-1 downto 0));

  \c$r'_43\ <= \c$r'_43_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_87\ <= (std_logic_vector(\c$restF0_case_scrut\(13)));

  \c$bv_88\ <= (std_logic_vector(\c$restF0_case_scrut\(14)));

  \c$case_alt_43_selection_res\ <= (( \c$bv_87\(\c$bv_87\'high) ) and ( \c$bv_88\(\c$bv_88\'high) )) = '0';

  \c$case_alt_43\ <= to_signed(131071,18) when \c$case_alt_43_selection_res\ else
                     to_signed(-131072,18);

  result_44_selection_res <= (( \c$app_arg_43\(\c$app_arg_43\'high) ) xor ( \c$r'_43\(\c$r'_43\'high) )) = '0';

  result_44 <= signed(\c$r'_43\) when result_44_selection_res else
               \c$case_alt_43\;

  r_44 <= resize(\c$restF0_case_scrut\(10),19) + resize(\c$restF0_case_scrut\(11),19);

  \c$app_arg_44\ <= std_logic_vector(r_44);

  \c$r'_44_projection\ <= (\c$app_arg_44\(\c$app_arg_44\'high downto 18),\c$app_arg_44\(18-1 downto 0));

  \c$r'_44\ <= \c$r'_44_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_89\ <= (std_logic_vector(\c$restF0_case_scrut\(10)));

  \c$bv_90\ <= (std_logic_vector(\c$restF0_case_scrut\(11)));

  \c$case_alt_44_selection_res\ <= (( \c$bv_89\(\c$bv_89\'high) ) and ( \c$bv_90\(\c$bv_90\'high) )) = '0';

  \c$case_alt_44\ <= to_signed(131071,18) when \c$case_alt_44_selection_res\ else
                     to_signed(-131072,18);

  result_45_selection_res <= (( \c$app_arg_44\(\c$app_arg_44\'high) ) xor ( \c$r'_44\(\c$r'_44\'high) )) = '0';

  result_45 <= signed(\c$r'_44\) when result_45_selection_res else
               \c$case_alt_44\;

  r_45 <= resize(\c$restF0_case_scrut\(7),19) + resize(\c$restF0_case_scrut\(8),19);

  \c$app_arg_45\ <= std_logic_vector(r_45);

  \c$r'_45_projection\ <= (\c$app_arg_45\(\c$app_arg_45\'high downto 18),\c$app_arg_45\(18-1 downto 0));

  \c$r'_45\ <= \c$r'_45_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_91\ <= (std_logic_vector(\c$restF0_case_scrut\(7)));

  \c$bv_92\ <= (std_logic_vector(\c$restF0_case_scrut\(8)));

  \c$case_alt_45_selection_res\ <= (( \c$bv_91\(\c$bv_91\'high) ) and ( \c$bv_92\(\c$bv_92\'high) )) = '0';

  \c$case_alt_45\ <= to_signed(131071,18) when \c$case_alt_45_selection_res\ else
                     to_signed(-131072,18);

  result_46_selection_res <= (( \c$app_arg_45\(\c$app_arg_45\'high) ) xor ( \c$r'_45\(\c$r'_45\'high) )) = '0';

  result_46 <= signed(\c$r'_45\) when result_46_selection_res else
               \c$case_alt_45\;

  r_46 <= resize(\c$restF0_case_scrut\(4),19) + resize(\c$restF0_case_scrut\(5),19);

  \c$app_arg_46\ <= std_logic_vector(r_46);

  \c$r'_46_projection\ <= (\c$app_arg_46\(\c$app_arg_46\'high downto 18),\c$app_arg_46\(18-1 downto 0));

  \c$r'_46\ <= \c$r'_46_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_93\ <= (std_logic_vector(\c$restF0_case_scrut\(4)));

  \c$bv_94\ <= (std_logic_vector(\c$restF0_case_scrut\(5)));

  \c$case_alt_46_selection_res\ <= (( \c$bv_93\(\c$bv_93\'high) ) and ( \c$bv_94\(\c$bv_94\'high) )) = '0';

  \c$case_alt_46\ <= to_signed(131071,18) when \c$case_alt_46_selection_res\ else
                     to_signed(-131072,18);

  result_47_selection_res <= (( \c$app_arg_46\(\c$app_arg_46\'high) ) xor ( \c$r'_46\(\c$r'_46\'high) )) = '0';

  result_47 <= signed(\c$r'_46\) when result_47_selection_res else
               \c$case_alt_46\;

  r_47 <= resize(\c$restF0_case_scrut\(1),19) + resize(\c$restF0_case_scrut\(2),19);

  \c$app_arg_47\ <= std_logic_vector(r_47);

  \c$r'_47_projection\ <= (\c$app_arg_47\(\c$app_arg_47\'high downto 18),\c$app_arg_47\(18-1 downto 0));

  \c$r'_47\ <= \c$r'_47_projection\.tup2_0_sel1_std_logic_vector_1;

  \c$bv_95\ <= (std_logic_vector(\c$restF0_case_scrut\(1)));

  \c$bv_96\ <= (std_logic_vector(\c$restF0_case_scrut\(2)));

  \c$case_alt_47_selection_res\ <= (( \c$bv_95\(\c$bv_95\'high) ) and ( \c$bv_96\(\c$bv_96\'high) )) = '0';

  \c$case_alt_47\ <= to_signed(131071,18) when \c$case_alt_47_selection_res\ else
                     to_signed(-131072,18);

  result_48_selection_res <= (( \c$app_arg_47\(\c$app_arg_47\'high) ) xor ( \c$r'_47\(\c$r'_47\'high) )) = '0';

  result_48 <= signed(\c$r'_47\) when result_48_selection_res else
               \c$case_alt_47\;

  \c$vec\ <= filters_types.array_of_signed_18'( to_signed(52,18)
                                              , to_signed(52,18)
                                              , to_signed(50,18)
                                              , to_signed(47,18)
                                              , to_signed(43,18)
                                              , to_signed(38,18)
                                              , to_signed(31,18)
                                              , to_signed(23,18)
                                              , to_signed(14,18)
                                              , to_signed(5,18)
                                              , to_signed(-5,18)
                                              , to_signed(-15,18)
                                              , to_signed(-26,18)
                                              , to_signed(-37,18)
                                              , to_signed(-47,18)
                                              , to_signed(-57,18)
                                              , to_signed(-66,18)
                                              , to_signed(-74,18)
                                              , to_signed(-80,18)
                                              , to_signed(-85,18)
                                              , to_signed(-88,18)
                                              , to_signed(-88,18)
                                              , to_signed(-87,18)
                                              , to_signed(-83,18)
                                              , to_signed(-77,18)
                                              , to_signed(-69,18)
                                              , to_signed(-57,18)
                                              , to_signed(-44,18)
                                              , to_signed(-28,18)
                                              , to_signed(-9,18)
                                              , to_signed(10,18)
                                              , to_signed(32,18)
                                              , to_signed(57,18)
                                              , to_signed(82,18)
                                              , to_signed(109,18)
                                              , to_signed(136,18)
                                              , to_signed(164,18)
                                              , to_signed(192,18)
                                              , to_signed(220,18)
                                              , to_signed(247,18)
                                              , to_signed(273,18)
                                              , to_signed(298,18)
                                              , to_signed(321,18)
                                              , to_signed(342,18)
                                              , to_signed(360,18)
                                              , to_signed(376,18)
                                              , to_signed(389,18)
                                              , to_signed(399,18)
                                              , to_signed(405,18)
                                              , to_signed(409,18)
                                              , to_signed(409,18)
                                              , to_signed(405,18)
                                              , to_signed(399,18)
                                              , to_signed(389,18)
                                              , to_signed(376,18)
                                              , to_signed(360,18)
                                              , to_signed(342,18)
                                              , to_signed(321,18)
                                              , to_signed(298,18)
                                              , to_signed(273,18)
                                              , to_signed(247,18)
                                              , to_signed(220,18)
                                              , to_signed(192,18)
                                              , to_signed(164,18)
                                              , to_signed(136,18)
                                              , to_signed(109,18)
                                              , to_signed(82,18)
                                              , to_signed(57,18)
                                              , to_signed(32,18)
                                              , to_signed(10,18)
                                              , to_signed(-9,18)
                                              , to_signed(-28,18)
                                              , to_signed(-44,18)
                                              , to_signed(-57,18)
                                              , to_signed(-69,18)
                                              , to_signed(-77,18)
                                              , to_signed(-83,18)
                                              , to_signed(-87,18)
                                              , to_signed(-88,18)
                                              , to_signed(-88,18)
                                              , to_signed(-85,18)
                                              , to_signed(-80,18)
                                              , to_signed(-74,18)
                                              , to_signed(-66,18)
                                              , to_signed(-57,18)
                                              , to_signed(-47,18)
                                              , to_signed(-37,18)
                                              , to_signed(-26,18)
                                              , to_signed(-15,18)
                                              , to_signed(-5,18)
                                              , to_signed(5,18)
                                              , to_signed(14,18)
                                              , to_signed(23,18)
                                              , to_signed(31,18)
                                              , to_signed(38,18)
                                              , to_signed(43,18)
                                              , to_signed(47,18)
                                              , to_signed(50,18)
                                              , to_signed(52,18)
                                              , to_signed(52,18) );

  -- select begin
  select_r : for i in \c$restF0_case_scrut_res\'range generate
    \c$restF0_case_scrut_res\(i) <= \c$vec\(0+(1*i));
  end generate;
  -- select end

  -- zipWith begin
  zipwith : for i_0 in \c$restF0_case_scrut\'range generate
  begin
    fun : block
      signal \c$case_alt_48\               : signed(17 downto 0);
      signal \c$app_arg_48\                : std_logic;
      signal \rL\                          : std_logic_vector(4 downto 0);
      signal \rR\                          : std_logic_vector(30 downto 0);
      signal \c$app_arg_49\                : std_logic;
      signal ds3                           : filters_types.tup2_2;
      signal x_0                           : std_logic_vector(5 downto 0);
      signal \c$c$restF0_case_scrut(i_0)_selection_res\    : boolean;
      signal \c$case_alt_48_selection_res\ : boolean;
      signal \c$bv_97\                     : std_logic_vector(35 downto 0);
    begin
      \c$c$restF0_case_scrut(i_0)_selection_res\ <= ((not \c$app_arg_49\) or \c$app_arg_48\) = '1';

      \c$restF0_case_scrut\(i_0) <= signed((std_logic_vector(resize(unsigned((std_logic_vector(shift_right(unsigned(\rR\),to_integer(to_signed(13,64)))))),18)))) when \c$c$restF0_case_scrut(i_0)_selection_res\ else
                 \c$case_alt_48\;

      \c$case_alt_48_selection_res\ <= ( \rL\(\rL\'high) ) = '0';

      \c$case_alt_48\ <= to_signed(131071,18) when \c$case_alt_48_selection_res\ else
                         to_signed(-131072,18);

      -- reduceAnd begin,

      reduceand : block
        function and_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '1';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := and_reduce (argi (argi'left downto half));
              lower  := and_reduce (argi (half - 1 downto argi'right));
              result := upper and lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_48\ <= and_reduce(x_0);
      end block;
      -- reduceAnd end

      \rL\ <= ds3.tup2_2_sel0_std_logic_vector_0;

      \rR\ <= ds3.tup2_2_sel1_std_logic_vector_1;

      -- reduceOr begin 
      reduceor : block
        function or_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '0';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := or_reduce (argi (argi'left downto half));
              lower  := or_reduce (argi (half - 1 downto argi'right));
              result := upper or lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_49\ <= or_reduce(x_0);
      end block;
      -- reduceOr end

      \c$bv_97\ <= (std_logic_vector((\c$restF0_case_scrut_res\(i_0) * \c$restF0_app_arg\(i_0))));

      ds3 <= (\c$bv_97\(\c$bv_97\'high downto 31),\c$bv_97\(31-1 downto 0));

      x_0 <= std_logic_vector'(std_logic_vector'((std_logic_vector'(0 => ( \rR\(\rR\'high) )))) & std_logic_vector'(\rL\));


    end block;
  end generate;
  -- zipWith end

  -- select begin
  select_r_0 : for i_1 in \c$elF0_app_arg\'range generate
    \c$elF0_app_arg\(i_1) <= \c$tup_app_arg\(0+(1*i_1));
  end generate;
  -- select end

  -- zipWith begin
  zipwith_0 : for i_2 in \c$restF0_app_arg\'range generate
  begin
    fun_0 : block
      signal r_48                          : signed(18 downto 0);
      signal \c$app_arg_50\                : std_logic_vector(18 downto 0);
      signal \c$r'_48\                     : std_logic_vector(17 downto 0);
      signal \c$case_alt_49\               : signed(17 downto 0);
      signal \c$r'_48_projection\          : filters_types.tup2_0;
      signal \c$case_alt_49_selection_res\ : boolean;
      signal \c$bv_98\                     : std_logic_vector(17 downto 0);
      signal \c$bv_99\                     : std_logic_vector(17 downto 0);
      signal \c$c$restF0_app_arg(i_2)_selection_res_0\  : boolean;
    begin
      r_48 <= resize(\c$elF0_app_arg\(i_2),19) + resize(\c$restF0_app_arg_0\(i_2),19);

      \c$app_arg_50\ <= std_logic_vector(r_48);

      \c$r'_48_projection\ <= (\c$app_arg_50\(\c$app_arg_50\'high downto 18),\c$app_arg_50\(18-1 downto 0));

      \c$r'_48\ <= \c$r'_48_projection\.tup2_0_sel1_std_logic_vector_1;

      \c$bv_98\ <= (std_logic_vector(\c$elF0_app_arg\(i_2)));

      \c$bv_99\ <= (std_logic_vector(\c$restF0_app_arg_0\(i_2)));

      \c$case_alt_49_selection_res\ <= (( \c$bv_98\(\c$bv_98\'high) ) and ( \c$bv_99\(\c$bv_99\'high) )) = '0';

      \c$case_alt_49\ <= to_signed(131071,18) when \c$case_alt_49_selection_res\ else
                         to_signed(-131072,18);

      \c$c$restF0_app_arg(i_2)_selection_res_0\ <= (( \c$app_arg_50\(\c$app_arg_50\'high) ) xor ( \c$r'_48\(\c$r'_48\'high) )) = '0';

      \c$restF0_app_arg\(i_2) <= signed(\c$r'_48\) when \c$c$restF0_app_arg(i_2)_selection_res_0\ else
                 \c$case_alt_49\;


    end block;
  end generate;
  -- zipWith end

  -- select begin
  select_r_1 : for i_3 in \c$elF0_app_arg_0\'range generate
    \c$elF0_app_arg_0\(i_3) <= \c$tup_app_arg\(50+(1*i_3));
  end generate;
  -- select end

  -- reverse begin
  reverse_loop : for i_4 in 0 to (50 - 1) generate
    \c$restF0_app_arg_0\(\c$elF0_app_arg_0\'high - i_4) <= \c$elF0_app_arg_0\(i_4);
  end generate;
  -- reverse end

  \c$vec_0\ <= (filters_types.array_of_signed_18'(filters_types.array_of_signed_18'(filters_types.array_of_signed_18'(0 => x)) & filters_types.array_of_signed_18'(\c$tup_app_arg\)));

  result_49_dc_arg <= (\c$vec_0\(0 to 100-1),\c$vec_0\(100 to \c$vec_0\'high));

  result_49 <= ( tup2_sel0_array_of_signed_18 => result_49_dc_arg.tup2_1_sel0_array_of_signed_18_0
               , tup2_sel1_signed => result_0 );

  -- register begin
  topentity_register : block
    signal ctup_app_arg_reg : filters_types.array_of_signed_18(0 to 99) := filters_types.array_of_signed_18'( to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18) );
  begin
    \c$tup_app_arg\ <= ctup_app_arg_reg; 
    ctup_app_arg_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ctup_app_arg_reg <= filters_types.array_of_signed_18'( to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ctup_app_arg_reg <= result_49.tup2_sel0_array_of_signed_18
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end


end;

