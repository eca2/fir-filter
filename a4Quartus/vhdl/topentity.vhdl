-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(-- clock
       clk    : in filters_types.clk_system;
       -- reset
       rst    : in filters_types.rst_system;
       x      : in signed(17 downto 0);
       result : out signed(17 downto 0));
end;

architecture structural of topentity is
  -- Filters.hs:87:1-8
  signal ws1             : filters_types.array_of_signed_18(0 to 100);
  -- Filters.hs:87:1-8
  signal ws2             : filters_types.array_of_signed_18(0 to 99);
  -- Filters.hs:87:1-8
  signal s               : filters_types.array_of_signed_18(0 to 99);
  -- Filters.hs:87:1-8
  signal \c$ws2_app_arg\ : filters_types.array_of_signed_18(0 to 99);
  signal \c$vec2\        : filters_types.array_of_signed_18(0 to 99);
  signal \c$vec\         : filters_types.array_of_signed_18(0 to 100);
  signal s_0             : filters_types.tup2;
  signal \c$vec1\        : filters_types.array_of_signed_18(0 to 99);

begin
  result <=  ws1(ws1'high) ;

  ws1 <= filters_types.array_of_signed_18'(signed'(to_signed(0,18)) & ws2);

  \c$vec2\ <= (ws1(0 to ws1'high - 1));

  -- zipWith begin
  zipwith : for i in ws2'range generate
  begin
    fun : block
      signal r                          : signed(18 downto 0);
      signal \c$app_arg\                : std_logic_vector(18 downto 0);
      signal \r'\                       : std_logic_vector(17 downto 0);
      signal \c$case_alt\               : signed(17 downto 0);
      signal \c$r'_projection\          : filters_types.tup2_0;
      signal \c$case_alt_selection_res\ : boolean;
      signal \c$bv\                     : std_logic_vector(17 downto 0);
      signal \c$bv_0\                   : std_logic_vector(17 downto 0);
      signal \c$ws2(i)_selection_res\ : boolean;
    begin
      r <= resize(\c$vec2\(i),19) + resize(\c$ws2_app_arg\(i),19);

      \c$app_arg\ <= std_logic_vector(r);

      \c$r'_projection\ <= (\c$app_arg\(\c$app_arg\'high downto 18),\c$app_arg\(18-1 downto 0));

      \r'\ <= \c$r'_projection\.tup2_0_sel1_std_logic_vector_1;

      \c$bv\ <= (std_logic_vector(\c$vec2\(i)));

      \c$bv_0\ <= (std_logic_vector(\c$ws2_app_arg\(i)));

      \c$case_alt_selection_res\ <= (( \c$bv\(\c$bv\'high) ) and ( \c$bv_0\(\c$bv_0\'high) )) = '0';

      \c$case_alt\ <= to_signed(131071,18) when \c$case_alt_selection_res\ else
                      to_signed(-131072,18);

      \c$ws2(i)_selection_res\ <= (( \c$app_arg\(\c$app_arg\'high) ) xor ( \r'\(\r'\'high) )) = '0';

      ws2(i) <= signed(\r'\) when \c$ws2(i)_selection_res\ else
                 \c$case_alt\;


    end block;
  end generate;
  -- zipWith end

  \c$vec\ <= (filters_types.array_of_signed_18'(filters_types.array_of_signed_18'(filters_types.array_of_signed_18'(0 => x)) & filters_types.array_of_signed_18'(s)));

  s_0 <= (\c$vec\(0 to 100-1),\c$vec\(100 to \c$vec\'high));

  -- register begin
  topentity_register : block
    signal s_reg : filters_types.array_of_signed_18(0 to 99) := filters_types.array_of_signed_18'( to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18) );
  begin
    s <= s_reg; 
    s_r : process(clk,rst)
    begin
      if rst =  '1'  then
        s_reg <= filters_types.array_of_signed_18'( to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18), to_signed(0,18) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        s_reg <= s_0.tup2_sel0_array_of_signed_18_0
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end

  \c$vec1\ <= filters_types.array_of_signed_18'( to_signed(52,18)
                                               , to_signed(52,18)
                                               , to_signed(50,18)
                                               , to_signed(47,18)
                                               , to_signed(43,18)
                                               , to_signed(38,18)
                                               , to_signed(31,18)
                                               , to_signed(23,18)
                                               , to_signed(14,18)
                                               , to_signed(5,18)
                                               , to_signed(-5,18)
                                               , to_signed(-15,18)
                                               , to_signed(-26,18)
                                               , to_signed(-37,18)
                                               , to_signed(-47,18)
                                               , to_signed(-57,18)
                                               , to_signed(-66,18)
                                               , to_signed(-74,18)
                                               , to_signed(-80,18)
                                               , to_signed(-85,18)
                                               , to_signed(-88,18)
                                               , to_signed(-88,18)
                                               , to_signed(-87,18)
                                               , to_signed(-83,18)
                                               , to_signed(-77,18)
                                               , to_signed(-69,18)
                                               , to_signed(-57,18)
                                               , to_signed(-44,18)
                                               , to_signed(-28,18)
                                               , to_signed(-9,18)
                                               , to_signed(10,18)
                                               , to_signed(32,18)
                                               , to_signed(57,18)
                                               , to_signed(82,18)
                                               , to_signed(109,18)
                                               , to_signed(136,18)
                                               , to_signed(164,18)
                                               , to_signed(192,18)
                                               , to_signed(220,18)
                                               , to_signed(247,18)
                                               , to_signed(273,18)
                                               , to_signed(298,18)
                                               , to_signed(321,18)
                                               , to_signed(342,18)
                                               , to_signed(360,18)
                                               , to_signed(376,18)
                                               , to_signed(389,18)
                                               , to_signed(399,18)
                                               , to_signed(405,18)
                                               , to_signed(409,18)
                                               , to_signed(409,18)
                                               , to_signed(405,18)
                                               , to_signed(399,18)
                                               , to_signed(389,18)
                                               , to_signed(376,18)
                                               , to_signed(360,18)
                                               , to_signed(342,18)
                                               , to_signed(321,18)
                                               , to_signed(298,18)
                                               , to_signed(273,18)
                                               , to_signed(247,18)
                                               , to_signed(220,18)
                                               , to_signed(192,18)
                                               , to_signed(164,18)
                                               , to_signed(136,18)
                                               , to_signed(109,18)
                                               , to_signed(82,18)
                                               , to_signed(57,18)
                                               , to_signed(32,18)
                                               , to_signed(10,18)
                                               , to_signed(-9,18)
                                               , to_signed(-28,18)
                                               , to_signed(-44,18)
                                               , to_signed(-57,18)
                                               , to_signed(-69,18)
                                               , to_signed(-77,18)
                                               , to_signed(-83,18)
                                               , to_signed(-87,18)
                                               , to_signed(-88,18)
                                               , to_signed(-88,18)
                                               , to_signed(-85,18)
                                               , to_signed(-80,18)
                                               , to_signed(-74,18)
                                               , to_signed(-66,18)
                                               , to_signed(-57,18)
                                               , to_signed(-47,18)
                                               , to_signed(-37,18)
                                               , to_signed(-26,18)
                                               , to_signed(-15,18)
                                               , to_signed(-5,18)
                                               , to_signed(5,18)
                                               , to_signed(14,18)
                                               , to_signed(23,18)
                                               , to_signed(31,18)
                                               , to_signed(38,18)
                                               , to_signed(43,18)
                                               , to_signed(47,18)
                                               , to_signed(50,18)
                                               , to_signed(52,18)
                                               , to_signed(52,18) );

  -- zipWith begin
  zipwith_0 : for i_0 in \c$ws2_app_arg\'range generate
  begin
    fun_0 : block
      signal \c$case_alt_0\               : signed(17 downto 0);
      signal \c$app_arg_0\                : std_logic;
      signal \rL\                         : std_logic_vector(4 downto 0);
      signal \rR\                         : std_logic_vector(30 downto 0);
      signal \c$app_arg_1\                : std_logic;
      signal ds3                          : filters_types.tup2_1;
      signal x_0                          : std_logic_vector(5 downto 0);
      signal \c$c$ws2_app_arg(i_0)_selection_res_0\ : boolean;
      signal \c$case_alt_0_selection_res\ : boolean;
      signal \c$bv_1\                     : std_logic_vector(35 downto 0);
    begin
      \c$c$ws2_app_arg(i_0)_selection_res_0\ <= ((not \c$app_arg_1\) or \c$app_arg_0\) = '1';

      \c$ws2_app_arg\(i_0) <= signed((std_logic_vector(resize(unsigned((std_logic_vector(shift_right(unsigned(\rR\),to_integer(to_signed(13,64)))))),18)))) when \c$c$ws2_app_arg(i_0)_selection_res_0\ else
                 \c$case_alt_0\;

      \c$case_alt_0_selection_res\ <= ( \rL\(\rL\'high) ) = '0';

      \c$case_alt_0\ <= to_signed(131071,18) when \c$case_alt_0_selection_res\ else
                        to_signed(-131072,18);

      -- reduceAnd begin,

      reduceand : block
        function and_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '1';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := and_reduce (argi (argi'left downto half));
              lower  := and_reduce (argi (half - 1 downto argi'right));
              result := upper and lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_0\ <= and_reduce(x_0);
      end block;
      -- reduceAnd end

      \rL\ <= ds3.tup2_1_sel0_std_logic_vector_0;

      \rR\ <= ds3.tup2_1_sel1_std_logic_vector_1;

      -- reduceOr begin 
      reduceor : block
        function or_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '0';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := or_reduce (argi (argi'left downto half));
              lower  := or_reduce (argi (half - 1 downto argi'right));
              result := upper or lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_1\ <= or_reduce(x_0);
      end block;
      -- reduceOr end

      \c$bv_1\ <= (std_logic_vector((\c$vec1\(i_0) * s(i_0))));

      ds3 <= (\c$bv_1\(\c$bv_1\'high downto 31),\c$bv_1\(31-1 downto 0));

      x_0 <= std_logic_vector'(std_logic_vector'((std_logic_vector'(0 => ( \rR\(\rR\'high) )))) & std_logic_vector'(\rL\));


    end block;
  end generate;
  -- zipWith end


end;

