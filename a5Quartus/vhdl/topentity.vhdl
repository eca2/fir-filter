-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(-- clock
       clk    : in filters_types.clk_system;
       -- reset
       rst    : in filters_types.rst_system;
       x      : in signed(7 downto 0);
       result : out signed(7 downto 0));
end;

architecture structural of topentity is
  signal \c$restF0_case_scrut\     : filters_types.array_of_signed_8(0 to 2);
  signal \c$elF0_app_arg\          : filters_types.array_of_signed_8(0 to 2);
  signal \c$restF0_app_arg\        : filters_types.array_of_signed_8(0 to 2);
  signal \c$elF0_app_arg_0\        : filters_types.array_of_signed_8(0 to 2);
  signal \c$restF0_app_arg_0\      : filters_types.array_of_signed_8(0 to 2);
  signal result_0                  : filters_types.tup2;
  -- Filters.hs:214:1-9
  signal \c$tup_app_arg\           : filters_types.array_of_signed_8(0 to 5);
  signal \c$restF0_case_scrut_res\ : filters_types.array_of_signed_8(0 to 2);
  signal \c$vec\                   : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec_0\                 : filters_types.array_of_signed_8(0 to 6);
  signal result_0_dc_arg           : filters_types.tup2_0;

begin
  result <= result_0.tup2_sel1_signed;

  \c$vec\ <= filters_types.array_of_signed_8'( to_signed(2,8)
                                             , to_signed(4,8)
                                             , to_signed(3,8)
                                             , to_signed(2,8)
                                             , to_signed(7,8)
                                             , to_signed(6,8) );

  -- select begin
  select_r : for i in \c$restF0_case_scrut_res\'range generate
    \c$restF0_case_scrut_res\(i) <= \c$vec\(0+(1*i));
  end generate;
  -- select end

  -- zipWith begin
  zipwith : for i_0 in \c$restF0_case_scrut\'range generate
  begin
    \c$restF0_case_scrut\(i_0) <= resize(\c$restF0_case_scrut_res\(i_0) * \c$restF0_app_arg\(i_0), 8);


  end generate;
  -- zipWith end

  -- select begin
  select_r_0 : for i_1 in \c$elF0_app_arg\'range generate
    \c$elF0_app_arg\(i_1) <= \c$tup_app_arg\(0+(1*i_1));
  end generate;
  -- select end

  -- zipWith begin
  zipwith_0 : for i_2 in \c$restF0_app_arg\'range generate
  begin
    \c$restF0_app_arg\(i_2) <= \c$elF0_app_arg\(i_2) + \c$restF0_app_arg_0\(i_2);


  end generate;
  -- zipWith end

  -- select begin
  select_r_1 : for i_3 in \c$elF0_app_arg_0\'range generate
    \c$elF0_app_arg_0\(i_3) <= \c$tup_app_arg\(3+(1*i_3));
  end generate;
  -- select end

  -- reverse begin
  reverse_loop : for i_4 in 0 to (3 - 1) generate
    \c$restF0_app_arg_0\(\c$elF0_app_arg_0\'high - i_4) <= \c$elF0_app_arg_0\(i_4);
  end generate;
  -- reverse end

  \c$vec_0\ <= (filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(0 => x)) & filters_types.array_of_signed_8'(\c$tup_app_arg\)));

  result_0_dc_arg <= (\c$vec_0\(0 to 6-1),\c$vec_0\(6 to \c$vec_0\'high));

  result_0 <= ( tup2_sel0_array_of_signed_8 => result_0_dc_arg.tup2_0_sel0_array_of_signed_8_0
              , tup2_sel1_signed => \c$restF0_case_scrut\(0) + (\c$restF0_case_scrut\(1) + \c$restF0_case_scrut\(2)) );

  -- register begin
  topentity_register : block
    signal ctup_app_arg_reg : filters_types.array_of_signed_8(0 to 5) := filters_types.array_of_signed_8'( to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8) );
  begin
    \c$tup_app_arg\ <= ctup_app_arg_reg; 
    ctup_app_arg_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ctup_app_arg_reg <= filters_types.array_of_signed_8'( to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ctup_app_arg_reg <= result_0.tup2_sel0_array_of_signed_8
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end


end;

