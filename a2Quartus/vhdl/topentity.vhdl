-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(\c$arg_0\  : in signed(17 downto 0);
       \c$arg_1\  : in signed(17 downto 0);
       \c$arg_2\  : in signed(17 downto 0);
       \c$arg_3\  : in signed(17 downto 0);
       \c$arg_4\  : in signed(17 downto 0);
       \c$arg_5\  : in signed(17 downto 0);
       \c$arg_6\  : in signed(17 downto 0);
       \c$arg_7\  : in signed(17 downto 0);
       \c$arg_8\  : in signed(17 downto 0);
       \c$arg_9\  : in signed(17 downto 0);
       \c$arg_10\ : in signed(17 downto 0);
       \c$arg_11\ : in signed(17 downto 0);
       \c$arg_12\ : in signed(17 downto 0);
       \c$arg_13\ : in signed(17 downto 0);
       \c$arg_14\ : in signed(17 downto 0);
       \c$arg_15\ : in signed(17 downto 0);
       \c$arg_16\ : in signed(17 downto 0);
       \c$arg_17\ : in signed(17 downto 0);
       \c$arg_18\ : in signed(17 downto 0);
       \c$arg_19\ : in signed(17 downto 0);
       \c$arg_20\ : in signed(17 downto 0);
       \c$arg_21\ : in signed(17 downto 0);
       \c$arg_22\ : in signed(17 downto 0);
       \c$arg_23\ : in signed(17 downto 0);
       \c$arg_24\ : in signed(17 downto 0);
       \c$arg_25\ : in signed(17 downto 0);
       \c$arg_26\ : in signed(17 downto 0);
       \c$arg_27\ : in signed(17 downto 0);
       \c$arg_28\ : in signed(17 downto 0);
       \c$arg_29\ : in signed(17 downto 0);
       \c$arg_30\ : in signed(17 downto 0);
       \c$arg_31\ : in signed(17 downto 0);
       \c$arg_32\ : in signed(17 downto 0);
       \c$arg_33\ : in signed(17 downto 0);
       \c$arg_34\ : in signed(17 downto 0);
       \c$arg_35\ : in signed(17 downto 0);
       \c$arg_36\ : in signed(17 downto 0);
       \c$arg_37\ : in signed(17 downto 0);
       \c$arg_38\ : in signed(17 downto 0);
       \c$arg_39\ : in signed(17 downto 0);
       \c$arg_40\ : in signed(17 downto 0);
       \c$arg_41\ : in signed(17 downto 0);
       \c$arg_42\ : in signed(17 downto 0);
       \c$arg_43\ : in signed(17 downto 0);
       \c$arg_44\ : in signed(17 downto 0);
       \c$arg_45\ : in signed(17 downto 0);
       \c$arg_46\ : in signed(17 downto 0);
       \c$arg_47\ : in signed(17 downto 0);
       \c$arg_48\ : in signed(17 downto 0);
       \c$arg_49\ : in signed(17 downto 0);
       \c$arg_50\ : in signed(17 downto 0);
       \c$arg_51\ : in signed(17 downto 0);
       \c$arg_52\ : in signed(17 downto 0);
       \c$arg_53\ : in signed(17 downto 0);
       \c$arg_54\ : in signed(17 downto 0);
       \c$arg_55\ : in signed(17 downto 0);
       \c$arg_56\ : in signed(17 downto 0);
       \c$arg_57\ : in signed(17 downto 0);
       \c$arg_58\ : in signed(17 downto 0);
       \c$arg_59\ : in signed(17 downto 0);
       \c$arg_60\ : in signed(17 downto 0);
       \c$arg_61\ : in signed(17 downto 0);
       \c$arg_62\ : in signed(17 downto 0);
       \c$arg_63\ : in signed(17 downto 0);
       \c$arg_64\ : in signed(17 downto 0);
       \c$arg_65\ : in signed(17 downto 0);
       \c$arg_66\ : in signed(17 downto 0);
       \c$arg_67\ : in signed(17 downto 0);
       \c$arg_68\ : in signed(17 downto 0);
       \c$arg_69\ : in signed(17 downto 0);
       \c$arg_70\ : in signed(17 downto 0);
       \c$arg_71\ : in signed(17 downto 0);
       \c$arg_72\ : in signed(17 downto 0);
       \c$arg_73\ : in signed(17 downto 0);
       \c$arg_74\ : in signed(17 downto 0);
       \c$arg_75\ : in signed(17 downto 0);
       \c$arg_76\ : in signed(17 downto 0);
       \c$arg_77\ : in signed(17 downto 0);
       \c$arg_78\ : in signed(17 downto 0);
       \c$arg_79\ : in signed(17 downto 0);
       \c$arg_80\ : in signed(17 downto 0);
       \c$arg_81\ : in signed(17 downto 0);
       \c$arg_82\ : in signed(17 downto 0);
       \c$arg_83\ : in signed(17 downto 0);
       \c$arg_84\ : in signed(17 downto 0);
       \c$arg_85\ : in signed(17 downto 0);
       \c$arg_86\ : in signed(17 downto 0);
       \c$arg_87\ : in signed(17 downto 0);
       \c$arg_88\ : in signed(17 downto 0);
       \c$arg_89\ : in signed(17 downto 0);
       \c$arg_90\ : in signed(17 downto 0);
       \c$arg_91\ : in signed(17 downto 0);
       \c$arg_92\ : in signed(17 downto 0);
       \c$arg_93\ : in signed(17 downto 0);
       \c$arg_94\ : in signed(17 downto 0);
       \c$arg_95\ : in signed(17 downto 0);
       \c$arg_96\ : in signed(17 downto 0);
       \c$arg_97\ : in signed(17 downto 0);
       \c$arg_98\ : in signed(17 downto 0);
       \c$arg_99\ : in signed(17 downto 0);
       result     : out signed(17 downto 0));
end;

architecture structural of topentity is
  -- Filters.hs:57:1-8
  signal ws1             : filters_types.array_of_signed_18(0 to 100);
  -- Filters.hs:57:1-8
  signal ws2             : filters_types.array_of_signed_18(0 to 99);
  -- Filters.hs:57:1-8
  signal \c$ws2_app_arg\ : filters_types.array_of_signed_18(0 to 99);
  signal \c$arg\         : filters_types.array_of_signed_18(0 to 99);
  signal \c$vec2\        : filters_types.array_of_signed_18(0 to 99);
  signal \c$vec2_0\      : filters_types.array_of_signed_18(0 to 99);

begin
  \c$arg\ <= filters_types.array_of_signed_18'( \c$arg_0\
                                              , \c$arg_1\
                                              , \c$arg_2\
                                              , \c$arg_3\
                                              , \c$arg_4\
                                              , \c$arg_5\
                                              , \c$arg_6\
                                              , \c$arg_7\
                                              , \c$arg_8\
                                              , \c$arg_9\
                                              , \c$arg_10\
                                              , \c$arg_11\
                                              , \c$arg_12\
                                              , \c$arg_13\
                                              , \c$arg_14\
                                              , \c$arg_15\
                                              , \c$arg_16\
                                              , \c$arg_17\
                                              , \c$arg_18\
                                              , \c$arg_19\
                                              , \c$arg_20\
                                              , \c$arg_21\
                                              , \c$arg_22\
                                              , \c$arg_23\
                                              , \c$arg_24\
                                              , \c$arg_25\
                                              , \c$arg_26\
                                              , \c$arg_27\
                                              , \c$arg_28\
                                              , \c$arg_29\
                                              , \c$arg_30\
                                              , \c$arg_31\
                                              , \c$arg_32\
                                              , \c$arg_33\
                                              , \c$arg_34\
                                              , \c$arg_35\
                                              , \c$arg_36\
                                              , \c$arg_37\
                                              , \c$arg_38\
                                              , \c$arg_39\
                                              , \c$arg_40\
                                              , \c$arg_41\
                                              , \c$arg_42\
                                              , \c$arg_43\
                                              , \c$arg_44\
                                              , \c$arg_45\
                                              , \c$arg_46\
                                              , \c$arg_47\
                                              , \c$arg_48\
                                              , \c$arg_49\
                                              , \c$arg_50\
                                              , \c$arg_51\
                                              , \c$arg_52\
                                              , \c$arg_53\
                                              , \c$arg_54\
                                              , \c$arg_55\
                                              , \c$arg_56\
                                              , \c$arg_57\
                                              , \c$arg_58\
                                              , \c$arg_59\
                                              , \c$arg_60\
                                              , \c$arg_61\
                                              , \c$arg_62\
                                              , \c$arg_63\
                                              , \c$arg_64\
                                              , \c$arg_65\
                                              , \c$arg_66\
                                              , \c$arg_67\
                                              , \c$arg_68\
                                              , \c$arg_69\
                                              , \c$arg_70\
                                              , \c$arg_71\
                                              , \c$arg_72\
                                              , \c$arg_73\
                                              , \c$arg_74\
                                              , \c$arg_75\
                                              , \c$arg_76\
                                              , \c$arg_77\
                                              , \c$arg_78\
                                              , \c$arg_79\
                                              , \c$arg_80\
                                              , \c$arg_81\
                                              , \c$arg_82\
                                              , \c$arg_83\
                                              , \c$arg_84\
                                              , \c$arg_85\
                                              , \c$arg_86\
                                              , \c$arg_87\
                                              , \c$arg_88\
                                              , \c$arg_89\
                                              , \c$arg_90\
                                              , \c$arg_91\
                                              , \c$arg_92\
                                              , \c$arg_93\
                                              , \c$arg_94\
                                              , \c$arg_95\
                                              , \c$arg_96\
                                              , \c$arg_97\
                                              , \c$arg_98\
                                              , \c$arg_99\ );

  result <=  ws1(ws1'high) ;

  ws1 <= filters_types.array_of_signed_18'(signed'(to_signed(0,18)) & ws2);

  \c$vec2\ <= (ws1(0 to ws1'high - 1));

  -- zipWith begin
  zipwith : for i in ws2'range generate
  begin
    fun : block
      signal r                          : signed(18 downto 0);
      signal \c$app_arg\                : std_logic_vector(18 downto 0);
      signal \r'\                       : std_logic_vector(17 downto 0);
      signal \c$case_alt\               : signed(17 downto 0);
      signal \c$r'_projection\          : filters_types.tup2;
      signal \c$case_alt_selection_res\ : boolean;
      signal \c$bv\                     : std_logic_vector(17 downto 0);
      signal \c$bv_0\                   : std_logic_vector(17 downto 0);
      signal \c$ws2(i)_selection_res\ : boolean;
    begin
      r <= resize(\c$vec2\(i),19) + resize(\c$ws2_app_arg\(i),19);

      \c$app_arg\ <= std_logic_vector(r);

      \c$r'_projection\ <= (\c$app_arg\(\c$app_arg\'high downto 18),\c$app_arg\(18-1 downto 0));

      \r'\ <= \c$r'_projection\.tup2_sel1_std_logic_vector_1;

      \c$bv\ <= (std_logic_vector(\c$vec2\(i)));

      \c$bv_0\ <= (std_logic_vector(\c$ws2_app_arg\(i)));

      \c$case_alt_selection_res\ <= (( \c$bv\(\c$bv\'high) ) and ( \c$bv_0\(\c$bv_0\'high) )) = '0';

      \c$case_alt\ <= to_signed(131071,18) when \c$case_alt_selection_res\ else
                      to_signed(-131072,18);

      \c$ws2(i)_selection_res\ <= (( \c$app_arg\(\c$app_arg\'high) ) xor ( \r'\(\r'\'high) )) = '0';

      ws2(i) <= signed(\r'\) when \c$ws2(i)_selection_res\ else
                 \c$case_alt\;


    end block;
  end generate;
  -- zipWith end

  \c$vec2_0\ <= filters_types.array_of_signed_18'( to_signed(52,18)
                                                 , to_signed(52,18)
                                                 , to_signed(50,18)
                                                 , to_signed(47,18)
                                                 , to_signed(43,18)
                                                 , to_signed(38,18)
                                                 , to_signed(31,18)
                                                 , to_signed(23,18)
                                                 , to_signed(14,18)
                                                 , to_signed(5,18)
                                                 , to_signed(-5,18)
                                                 , to_signed(-15,18)
                                                 , to_signed(-26,18)
                                                 , to_signed(-37,18)
                                                 , to_signed(-47,18)
                                                 , to_signed(-57,18)
                                                 , to_signed(-66,18)
                                                 , to_signed(-74,18)
                                                 , to_signed(-80,18)
                                                 , to_signed(-85,18)
                                                 , to_signed(-88,18)
                                                 , to_signed(-88,18)
                                                 , to_signed(-87,18)
                                                 , to_signed(-83,18)
                                                 , to_signed(-77,18)
                                                 , to_signed(-69,18)
                                                 , to_signed(-57,18)
                                                 , to_signed(-44,18)
                                                 , to_signed(-28,18)
                                                 , to_signed(-9,18)
                                                 , to_signed(10,18)
                                                 , to_signed(32,18)
                                                 , to_signed(57,18)
                                                 , to_signed(82,18)
                                                 , to_signed(109,18)
                                                 , to_signed(136,18)
                                                 , to_signed(164,18)
                                                 , to_signed(192,18)
                                                 , to_signed(220,18)
                                                 , to_signed(247,18)
                                                 , to_signed(273,18)
                                                 , to_signed(298,18)
                                                 , to_signed(321,18)
                                                 , to_signed(342,18)
                                                 , to_signed(360,18)
                                                 , to_signed(376,18)
                                                 , to_signed(389,18)
                                                 , to_signed(399,18)
                                                 , to_signed(405,18)
                                                 , to_signed(409,18)
                                                 , to_signed(409,18)
                                                 , to_signed(405,18)
                                                 , to_signed(399,18)
                                                 , to_signed(389,18)
                                                 , to_signed(376,18)
                                                 , to_signed(360,18)
                                                 , to_signed(342,18)
                                                 , to_signed(321,18)
                                                 , to_signed(298,18)
                                                 , to_signed(273,18)
                                                 , to_signed(247,18)
                                                 , to_signed(220,18)
                                                 , to_signed(192,18)
                                                 , to_signed(164,18)
                                                 , to_signed(136,18)
                                                 , to_signed(109,18)
                                                 , to_signed(82,18)
                                                 , to_signed(57,18)
                                                 , to_signed(32,18)
                                                 , to_signed(10,18)
                                                 , to_signed(-9,18)
                                                 , to_signed(-28,18)
                                                 , to_signed(-44,18)
                                                 , to_signed(-57,18)
                                                 , to_signed(-69,18)
                                                 , to_signed(-77,18)
                                                 , to_signed(-83,18)
                                                 , to_signed(-87,18)
                                                 , to_signed(-88,18)
                                                 , to_signed(-88,18)
                                                 , to_signed(-85,18)
                                                 , to_signed(-80,18)
                                                 , to_signed(-74,18)
                                                 , to_signed(-66,18)
                                                 , to_signed(-57,18)
                                                 , to_signed(-47,18)
                                                 , to_signed(-37,18)
                                                 , to_signed(-26,18)
                                                 , to_signed(-15,18)
                                                 , to_signed(-5,18)
                                                 , to_signed(5,18)
                                                 , to_signed(14,18)
                                                 , to_signed(23,18)
                                                 , to_signed(31,18)
                                                 , to_signed(38,18)
                                                 , to_signed(43,18)
                                                 , to_signed(47,18)
                                                 , to_signed(50,18)
                                                 , to_signed(52,18)
                                                 , to_signed(52,18) );

  -- zipWith begin
  zipwith_0 : for i_0 in \c$ws2_app_arg\'range generate
  begin
    fun_0 : block
      signal \c$case_alt_0\               : signed(17 downto 0);
      signal \c$app_arg_0\                : std_logic;
      signal \rL\                         : std_logic_vector(4 downto 0);
      signal \rR\                         : std_logic_vector(30 downto 0);
      signal \c$app_arg_1\                : std_logic;
      signal ds3                          : filters_types.tup2_0;
      signal x                            : std_logic_vector(5 downto 0);
      signal \c$c$ws2_app_arg(i_0)_selection_res_0\ : boolean;
      signal \c$case_alt_0_selection_res\ : boolean;
      signal \c$bv_1\                     : std_logic_vector(35 downto 0);
    begin
      \c$c$ws2_app_arg(i_0)_selection_res_0\ <= ((not \c$app_arg_1\) or \c$app_arg_0\) = '1';

      \c$ws2_app_arg\(i_0) <= signed((std_logic_vector(resize(unsigned((std_logic_vector(shift_right(unsigned(\rR\),to_integer(to_signed(13,64)))))),18)))) when \c$c$ws2_app_arg(i_0)_selection_res_0\ else
                 \c$case_alt_0\;

      \c$case_alt_0_selection_res\ <= ( \rL\(\rL\'high) ) = '0';

      \c$case_alt_0\ <= to_signed(131071,18) when \c$case_alt_0_selection_res\ else
                        to_signed(-131072,18);

      -- reduceAnd begin,

      reduceand : block
        function and_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '1';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := and_reduce (argi (argi'left downto half));
              lower  := and_reduce (argi (half - 1 downto argi'right));
              result := upper and lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_0\ <= and_reduce(x);
      end block;
      -- reduceAnd end

      \rL\ <= ds3.tup2_0_sel0_std_logic_vector_0;

      \rR\ <= ds3.tup2_0_sel1_std_logic_vector_1;

      -- reduceOr begin 
      reduceor : block
        function or_reduce (arg : std_logic_vector) return std_logic is
          variable upper, lower : std_logic;
          variable half         : integer;
          variable argi         : std_logic_vector (arg'length - 1 downto 0);
          variable result       : std_logic;
        begin
          if (arg'length < 1) then
            result := '0';
          else
            argi := arg;
            if (argi'length = 1) then
              result := argi(argi'left);
            else
              half   := (argi'length + 1) / 2; -- lsb-biased tree
              upper  := or_reduce (argi (argi'left downto half));
              lower  := or_reduce (argi (half - 1 downto argi'right));
              result := upper or lower;
            end if;
          end if;
          return result;
        end;
      begin
        \c$app_arg_1\ <= or_reduce(x);
      end block;
      -- reduceOr end

      \c$bv_1\ <= (std_logic_vector((\c$arg\(i_0) * \c$vec2_0\(i_0))));

      ds3 <= (\c$bv_1\(\c$bv_1\'high downto 31),\c$bv_1\(31-1 downto 0));

      x <= std_logic_vector'(std_logic_vector'((std_logic_vector'(0 => ( \rR\(\rR\'high) )))) & std_logic_vector'(\rL\));


    end block;
  end generate;
  -- zipWith end


end;

