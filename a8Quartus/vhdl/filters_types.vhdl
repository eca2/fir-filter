library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package filters_types is



  type tup2_1 is record
    tup2_1_sel0_std_logic_vector_0 : std_logic_vector(0 downto 0);
    tup2_1_sel1_std_logic_vector_1 : std_logic_vector(17 downto 0);
  end record;
  type tup2_0 is record
    tup2_0_sel0_std_logic_vector_0 : std_logic_vector(4 downto 0);
    tup2_0_sel1_std_logic_vector_1 : std_logic_vector(30 downto 0);
  end record;
  subtype rst_system is std_logic;

  subtype clk_system is std_logic;
  type array_of_signed_18 is array (integer range <>) of signed(17 downto 0);
  type tup2 is record
    tup2_sel0_array_of_signed_18 : filters_types.array_of_signed_18(0 to 2);
    tup2_sel1_signed : signed(17 downto 0);
  end record;
  type tup2_2 is record
    tup2_2_sel0_array_of_signed_18_0 : filters_types.array_of_signed_18(0 to 2);
    tup2_2_sel1_array_of_signed_18_1 : filters_types.array_of_signed_18(0 to 0);
  end record;
  function toSLV (slv : in std_logic_vector) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return std_logic_vector;
  function toSLV (b : in boolean) return std_logic_vector;
  function fromSLV (sl : in std_logic_vector) return boolean;
  function tagToEnum (s : in signed) return boolean;
  function dataToTag (b : in boolean) return signed;
  function toSLV (sl : in std_logic) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return std_logic;
  function toSLV (p : filters_types.tup2_1) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return filters_types.tup2_1;
  function toSLV (p : filters_types.tup2_0) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return filters_types.tup2_0;
  function toSLV (s : in signed) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return signed;
  function toSLV (value :  filters_types.array_of_signed_18) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return filters_types.array_of_signed_18;
  function toSLV (p : filters_types.tup2) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return filters_types.tup2;
  function toSLV (p : filters_types.tup2_2) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return filters_types.tup2_2;
end;

package body filters_types is
  function toSLV (slv : in std_logic_vector) return std_logic_vector is
  begin
    return slv;
  end;
  function fromSLV (slv : in std_logic_vector) return std_logic_vector is
  begin
    return slv;
  end;
  function toSLV (b : in boolean) return std_logic_vector is
  begin
    if b then
      return "1";
    else
      return "0";
    end if;
  end;
  function fromSLV (sl : in std_logic_vector) return boolean is
  begin
    if sl = "1" then
      return true;
    else
      return false;
    end if;
  end;
  function tagToEnum (s : in signed) return boolean is
  begin
    if s = to_signed(0,64) then
      return false;
    else
      return true;
    end if;
  end;
  function dataToTag (b : in boolean) return signed is
  begin
    if b then
      return to_signed(1,64);
    else
      return to_signed(0,64);
    end if;
  end;
  function toSLV (sl : in std_logic) return std_logic_vector is
  begin
    return std_logic_vector'(0 => sl);
  end;
  function fromSLV (slv : in std_logic_vector) return std_logic is
    alias islv : std_logic_vector (0 to slv'length - 1) is slv;
  begin
    return islv(0);
  end;
  function toSLV (p : filters_types.tup2_1) return std_logic_vector is
  begin
    return (toSLV(p.tup2_1_sel0_std_logic_vector_0) & toSLV(p.tup2_1_sel1_std_logic_vector_1));
  end;
  function fromSLV (slv : in std_logic_vector) return filters_types.tup2_1 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 0)),fromSLV(islv(1 to 18)));
  end;
  function toSLV (p : filters_types.tup2_0) return std_logic_vector is
  begin
    return (toSLV(p.tup2_0_sel0_std_logic_vector_0) & toSLV(p.tup2_0_sel1_std_logic_vector_1));
  end;
  function fromSLV (slv : in std_logic_vector) return filters_types.tup2_0 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 4)),fromSLV(islv(5 to 35)));
  end;
  function toSLV (s : in signed) return std_logic_vector is
  begin
    return std_logic_vector(s);
  end;
  function fromSLV (slv : in std_logic_vector) return signed is
  begin
    return signed(slv);
  end;
  function toSLV (value :  filters_types.array_of_signed_18) return std_logic_vector is
    alias ivalue    : filters_types.array_of_signed_18(1 to value'length) is value;
    variable result : std_logic_vector(1 to value'length * 18);
  begin
    for i in ivalue'range loop
      result(((i - 1) * 18) + 1 to i*18) := toSLV(ivalue(i));
    end loop;
    return result;
  end;
  function fromSLV (slv : in std_logic_vector) return filters_types.array_of_signed_18 is
    alias islv      : std_logic_vector(0 to slv'length - 1) is slv;
    variable result : filters_types.array_of_signed_18(0 to slv'length / 18 - 1);
  begin
    for i in result'range loop
      result(i) := fromSLV(islv(i * 18 to (i+1) * 18 - 1));
    end loop;
    return result;
  end;
  function toSLV (p : filters_types.tup2) return std_logic_vector is
  begin
    return (toSLV(p.tup2_sel0_array_of_signed_18) & toSLV(p.tup2_sel1_signed));
  end;
  function fromSLV (slv : in std_logic_vector) return filters_types.tup2 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 53)),fromSLV(islv(54 to 71)));
  end;
  function toSLV (p : filters_types.tup2_2) return std_logic_vector is
  begin
    return (toSLV(p.tup2_2_sel0_array_of_signed_18_0) & toSLV(p.tup2_2_sel1_array_of_signed_18_1));
  end;
  function fromSLV (slv : in std_logic_vector) return filters_types.tup2_2 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 53)),fromSLV(islv(54 to 71)));
  end;
end;

