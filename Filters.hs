module Filters where

import Clash.Prelude
import FilterCoefAndInput

type Clk = Clock System
type Rst = Reset System
type Sig = Signal System

-- Student information:
--  Student 1
--    lastname:
--    student number:
--  Student 2
--    lastname:
--    student number:

-- NOTE: topentity functions are at the bottom
-- comment and uncomment the functions for each assignment
-- it should not be necessary, but you may change the 
-- definition of the topEntity functions.


type Value = Signed 8
type Vector = Vec 6 Value

fir1_6_coef :: Vector
fir1_6_coef = (2:>4:>3:>2:>7:>6:>Nil)

fir1_6_coef_simmetric :: Vec 3 Value
fir1_6_coef_simmetric = select d0 d1 d3 fir1_6_coef

-----------------------------------------------------------
-- Assignment 1
-- FIR1 N = 6 
-----------------------------------------------------------

fir1_6 :: Vector -> Value
fir1_6 xs = last_step
 where
  step1 = zipWith (*) xs fir1_6_coef
  last_step = foldl (+) 0 step1



fir1_6_debug :: IO ()
fir1_6_debug  = do
  let xs = (1 :> 2 :> 3 :> 4 :> 5 :> 6:> Nil)
  putStrLn $ show $ fir1_6 xs

-----------------------------------------------------------
-- Assignment 2
-- FIR1 N = 100 
-----------------------------------------------------------
type Value1 = SFixed 5 13
type Vector1 = Vec 100 Value1

fir1_100 :: Vector1 -> Value1
fir1_100 xs = last_step
 where
  step1 = zipWith (*) xs filterCoef
  last_step = foldl (+) 0 step1  



-----------------------------------------------------------
-- Assignment 3
-- FIR2 N = 6
-----------------------------------------------------------

init_coef :: Vector
init_coef = (0:>0:>0:>0:>0:>0:>Nil)

fir2_6 s x = (s', z)
  where
    s' = x +>> s
    step1 = zipWith (*) fir1_6_coef s
    z  = foldl (+) 0 step1


-----------------------------------------------------------
-- Assignment 4
-- FIR2 N = 100
-----------------------------------------------------------
init_coef100 = repeat 0 :: Vec 100 (SFixed 5 13)

fir2_100 s x = (s', z)
  where
    s' = x +>> s
    step1 = zipWith (*) filterCoef s
    z  = foldl (+) 0 step1
-----------------------------------------------------------
-- Assignment 5
-- FIR3 N = 6
-----------------------------------------------------------
fir3_6 :: Vec 6 Value -> Value -> (Vec 6 Value, Value)
fir3_6 state x = (state', out)
    where
      state' = x +>> state
      x_vec1 =  select d0 d1 d3 state
      x_vec2 = reverse(select d3 d1 d3 state)
      x_vec = zipWith (+) x_vec1 x_vec2
      out = sum( zipWith (*) (select d0 d1 d3 fir1_6_coef) x_vec )

fir3_6Sim:: HiddenClockResetEnable dom  => Signal dom Value -> Signal dom (Value)
fir3_6Sim = mealy fir3_6 (replicate d6 0)
-- use as simulate @System fir3_6Sim [1,1,1,1,1,1]

-----------------------------------------------------------
-- Assignment 6
-- FIR3 N = 100
-----------------------------------------------------------

-- For assignment 6 we have considered that only 50 dsp were 
-- available.


fir3_100 :: Vec 100 Value1 -> Value1 -> (Vec 100 Value1, Value1)
fir3_100 state x = (state', out)
    where
      state' = x +>> state
      x_vec1 =  select d0 d1 d50 state
      x_vec2 = reverse(select d50 d1 d50 state)
      x_vec = zipWith (+) x_vec1 x_vec2
      out = sum( zipWith (*) (select d0 d1 d50 filterCoef) x_vec )
 
fir3_100Sim:: HiddenClockResetEnable dom  => Signal dom Value1 -> Signal dom ( Value1 )
fir3_100Sim = mealy fir3_100 (replicate d100 0)

-- use as simulate @System fir3_100Sim inputSignal


-----------------------------------------------------------
-- Assignment 7
-- FIR3' N = 6
-----------------------------------------------------------
fir3t_6 :: Vec 6 Value -> Value -> (Vec 6 Value, Value) 
fir3t_6 state x = (state', out)
   where
    out = last state
    first_update = zipWith (+)  (0 +>> (select d0 d1 d3 state) ) (zipWith (*) (replicate d3 x) fir1_6_coef_simmetric)  
    second_update = zipWith (+) (select d2 d1 d3 state)  ( zipWith (*) (replicate d3 x) (reverse fir1_6_coef_simmetric) )  
    state' = first_update ++ second_update

fir3t_6Sim:: HiddenClockResetEnable dom  => Signal dom Value -> Signal dom (Value)
fir3t_6Sim = mealy fir3t_6 (replicate d6 0)
-- use as simulate @System fir3t_6Sim [1,1,1,1,1,1]

-----------------------------------------------------------
-- Assignment 8
-- IIR
-----------------------------------------------------------
iirInput = toList ( (1:>Nil) ++  (replicate d500 0)++(replicate d500 0)++(replicate d500 0)++(replicate d500 0) )

iir:: Vec 3 Value1 -> Value1 ->  (Vec 3 Value1 , Value1)
iir state x = (state' , z)
  where
     z = (last iir_bs) * x + (last state)
     x_vec = repeat x :: Vec 3 (SFixed 5 13)
     z_vec = repeat z :: Vec 3 (SFixed 5 13)
     step1 = zipWith (*) x_vec (init iir_bs)
     step2 = zipWith (*) z_vec iir_as
     step3 = zipWith (+) step1 step2
     state'= zipWith (+) (0 +>> state) step3 

iirSim :: HiddenClockResetEnable dom  => Signal dom Value1 -> Signal dom Value1
iirSim  = mealy iir (0:>0:>0:>Nil)

--Use as : simulate @System iirSim iirInput

-----------------------------------------------------------
-- Assignment 9
-- IIR
-----------------------------------------------------------
iir2:: Vec 3 Value1 -> Value1 ->  (Vec 3 Value1 , Value1)
iir2 state z = (state' , x)
  where
    step1 = zipWith (*) iir_as state
    step2 = foldl (+) 0 step1
    step3 = z + step2
    step4 = state ++ (step3 :> Nil)
    step5 = zipWith (*) iir_bs step4
    x = foldr (+) 0 step5
    state' = state <<+ step3


iir2Sim :: HiddenClockResetEnable dom  => Signal dom Value1 -> Signal dom Value1
iir2Sim  = mealy iir2 (0:>0:>0:>Nil)

-- Use as : simulate @System iir2Sim iirInput

-----------------------------------------------------------
-- topEntity's
-----------------------------------------------------------
-- Assignment 1
--topEntity = fir1_6

-- Assignment 2
--topEntity = fir1_100

-- Assignment 3
--topEntity :: Clk -> Rst  -> Sig Value -> Sig Value
--topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy fir2_6 init_coef ) x

fir2_6Sim :: HiddenClockResetEnable dom  => Signal dom Value -> Signal dom Value
fir2_6Sim = mealy fir2_6 (0:>0:>0:>0:>0:>0:>Nil)

-- Assignment 4
--topEntity :: Clk -> Rst  -> Sig Value1 -> Sig Value1
--topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy fir2_100 init_coef100 ) x

fir2_100Sim :: HiddenClockResetEnable dom  => Signal dom Value1 -> Signal dom Value1
fir2_100Sim = mealy fir2_100 init_coef100

-- Use as : simulate @System fir2_100Sim inputSignal
-- Reference http://hackage.haskell.org/package/clash-prelude-1.0.1/docs/Clash-Prelude.html

-- Assignment 5
--topEntity :: Clk -> Rst -> Sig Value -> Sig Value
--topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy fir3_6 (replicate d6 0) ) x

-- Assignment 6

--topEntity :: Clk -> Rst -> Sig Value1 -> Sig Value1
--topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy fir3_100 (replicate d100 0) ) x

-- Assignment 7
--topEntity :: Clk -> Rst -> Sig Value -> Sig Value
--topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy fir3t_6 (replicate d6 0) ) x

-- Assignment 8

--topEntity :: Clk -> Rst -> Sig (SFixed 5 13) -> Sig (SFixed 5 13)
--topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy iir (0:>0:>0:>Nil)) x


-- Assignment 9
topEntity :: Clk -> Rst -> Sig (SFixed 5 13) -> Sig (SFixed 5 13)
topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy iir2 (0:>0:>0:>Nil)) x