-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(-- clock
       clk    : in filters_types.clk_system;
       -- reset
       rst    : in filters_types.rst_system;
       x      : in signed(7 downto 0);
       result : out signed(7 downto 0));
end;

architecture structural of topentity is
  -- Filters.hs:74:1-6
  signal ws              : filters_types.array_of_signed_8(0 to 6);
  -- Filters.hs:74:1-6
  signal ws1             : filters_types.array_of_signed_8(0 to 5);
  -- Filters.hs:74:1-6
  signal s               : filters_types.array_of_signed_8(0 to 5);
  -- Filters.hs:74:1-6
  signal \c$ws1_app_arg\ : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec2\        : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec\         : filters_types.array_of_signed_8(0 to 6);
  signal s_0             : filters_types.tup2;
  signal \c$vec1\        : filters_types.array_of_signed_8(0 to 5);

begin
  result <=  ws(ws'high) ;

  ws <= filters_types.array_of_signed_8'(signed'(to_signed(0,8)) & ws1);

  \c$vec2\ <= (ws(0 to ws'high - 1));

  -- zipWith begin
  zipwith : for i in ws1'range generate
  begin
    ws1(i) <= \c$vec2\(i) + \c$ws1_app_arg\(i);


  end generate;
  -- zipWith end

  \c$vec\ <= (filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(0 => x)) & filters_types.array_of_signed_8'(s)));

  s_0 <= (\c$vec\(0 to 6-1),\c$vec\(6 to \c$vec\'high));

  -- register begin
  topentity_register : block
    signal s_reg : filters_types.array_of_signed_8(0 to 5) := filters_types.array_of_signed_8'( to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8) );
  begin
    s <= s_reg; 
    s_r : process(clk,rst)
    begin
      if rst =  '1'  then
        s_reg <= filters_types.array_of_signed_8'( to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8), to_signed(0,8) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        s_reg <= s_0.tup2_sel0_array_of_signed_8_0
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end

  \c$vec1\ <= filters_types.array_of_signed_8'( to_signed(2,8)
                                              , to_signed(4,8)
                                              , to_signed(3,8)
                                              , to_signed(2,8)
                                              , to_signed(7,8)
                                              , to_signed(6,8) );

  -- zipWith begin
  zipwith_0 : for i_0 in \c$ws1_app_arg\'range generate
  begin
    \c$ws1_app_arg\(i_0) <= resize(\c$vec1\(i_0) * s(i_0), 8);


  end generate;
  -- zipWith end


end;

