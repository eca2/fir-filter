-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(-- clock
       clk        : in filters_types.clk_system;
       -- reset
       rst        : in filters_types.rst_system;
       \c$arg_0\  : in signed(7 downto 0);
       \c$arg_1\  : in signed(7 downto 0);
       \c$arg_2\  : in signed(7 downto 0);
       \c$arg_3\  : in signed(7 downto 0);
       \c$arg_4\  : in signed(7 downto 0);
       \c$arg_5\  : in signed(7 downto 0);
       \c$arg_6\  : in signed(7 downto 0);
       result_0_0 : out signed(7 downto 0);
       result_0_1 : out signed(7 downto 0);
       result_0_2 : out signed(7 downto 0);
       result_0_3 : out signed(7 downto 0);
       result_0_4 : out signed(7 downto 0);
       result_0_5 : out signed(7 downto 0);
       result_1   : out signed(7 downto 0));
end;

architecture structural of topentity is
  -- Filters.hs:66:1-6
  signal ws1             : filters_types.array_of_signed_8(0 to 6);
  -- Filters.hs:66:1-6
  signal ws2             : filters_types.array_of_signed_8(0 to 5);
  -- Filters.hs:66:1-6
  signal \c$ws2_app_arg\ : filters_types.array_of_signed_8(0 to 5);
  signal \c$arg\         : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec2\        : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec1\        : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec\         : filters_types.array_of_signed_8(0 to 6);
  signal result_dc_arg   : filters_types.tup2;
  signal result          : filters_types.tup2_0;
  signal result_0        : filters_types.array_of_signed_8(0 to 5);

begin
  \c$arg\ <= filters_types.array_of_signed_8'( \c$arg_0\
                                             , \c$arg_1\
                                             , \c$arg_2\
                                             , \c$arg_3\
                                             , \c$arg_4\
                                             , \c$arg_5\ );

  ws1 <= filters_types.array_of_signed_8'(signed'(to_signed(0,8)) & ws2);

  \c$vec2\ <= (ws1(0 to ws1'high - 1));

  -- zipWith begin
  zipwith : for i in ws2'range generate
  begin
    ws2(i) <= \c$vec2\(i) + \c$ws2_app_arg\(i);


  end generate;
  -- zipWith end

  \c$vec1\ <= filters_types.array_of_signed_8'( to_signed(2,8)
                                              , to_signed(4,8)
                                              , to_signed(3,8)
                                              , to_signed(2,8)
                                              , to_signed(7,8)
                                              , to_signed(6,8) );

  -- zipWith begin
  zipwith_0 : for i_0 in \c$ws2_app_arg\'range generate
  begin
    \c$ws2_app_arg\(i_0) <= resize(\c$vec1\(i_0) * \c$arg\(i_0), 8);


  end generate;
  -- zipWith end

  \c$vec\ <= (filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(filters_types.array_of_signed_8'(0 => \c$arg_6\)) & filters_types.array_of_signed_8'(\c$arg\)));

  result_dc_arg <= (\c$vec\(0 to 6-1),\c$vec\(6 to \c$vec\'high));

  result <= ( tup2_0_sel0_array_of_signed_8 => result_dc_arg.tup2_sel0_array_of_signed_8_0
            , tup2_0_sel1_signed =>  ws1(ws1'high)  );

  result_0 <= result.tup2_0_sel0_array_of_signed_8;

  result_1 <= result.tup2_0_sel1_signed;

  result_0_0 <= result_0(0);

  result_0_1 <= result_0(1);

  result_0_2 <= result_0(2);

  result_0_3 <= result_0(3);

  result_0_4 <= result_0(4);

  result_0_5 <= result_0(5);


end;

