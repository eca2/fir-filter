-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 19.1.0 Build 670 09/22/2019 SJ Lite Edition"

-- DATE "01/03/2020 13:41:51"

-- 
-- Device: Altera 5CEBA2F17A7 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	topentity IS
    PORT (
	\c$arg_0\ : IN std_logic_vector(7 DOWNTO 0);
	\c$arg_1\ : IN std_logic_vector(7 DOWNTO 0);
	\c$arg_2\ : IN std_logic_vector(7 DOWNTO 0);
	\c$arg_3\ : IN std_logic_vector(7 DOWNTO 0);
	\c$arg_4\ : IN std_logic_vector(7 DOWNTO 0);
	\c$arg_5\ : IN std_logic_vector(7 DOWNTO 0);
	result : OUT IEEE.NUMERIC_STD.signed(7 DOWNTO 0)
	);
END topentity;

-- Design Ports Information
-- result[0]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- result[1]	=>  Location: PIN_K4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- result[2]	=>  Location: PIN_T7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- result[3]	=>  Location: PIN_T4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- result[4]	=>  Location: PIN_R12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- result[5]	=>  Location: PIN_M10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- result[6]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- result[7]	=>  Location: PIN_M13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_5[0]	=>  Location: PIN_N3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_5[1]	=>  Location: PIN_T8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_5[2]	=>  Location: PIN_R15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_5[3]	=>  Location: PIN_T2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_5[4]	=>  Location: PIN_P1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_5[5]	=>  Location: PIN_R1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_5[6]	=>  Location: PIN_P4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_5[7]	=>  Location: PIN_P2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_4[0]	=>  Location: PIN_T15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_4[1]	=>  Location: PIN_P9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_4[2]	=>  Location: PIN_R9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_4[3]	=>  Location: PIN_M8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_4[4]	=>  Location: PIN_R2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_4[5]	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_4[6]	=>  Location: PIN_P8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_4[7]	=>  Location: PIN_T3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_3[0]	=>  Location: PIN_L7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_3[1]	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_3[2]	=>  Location: PIN_J2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_3[3]	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_3[4]	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_3[5]	=>  Location: PIN_G2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_3[6]	=>  Location: PIN_G3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_3[7]	=>  Location: PIN_N4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_2[0]	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_2[1]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_2[2]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_2[3]	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_2[4]	=>  Location: PIN_H4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_2[5]	=>  Location: PIN_H5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_2[6]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_2[7]	=>  Location: PIN_K5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_1[0]	=>  Location: PIN_M12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_1[1]	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_1[2]	=>  Location: PIN_R10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_1[3]	=>  Location: PIN_R11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_1[4]	=>  Location: PIN_R7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_1[5]	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_1[6]	=>  Location: PIN_T12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_1[7]	=>  Location: PIN_R6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_0[0]	=>  Location: PIN_N14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_0[1]	=>  Location: PIN_H3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_0[2]	=>  Location: PIN_M7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_0[3]	=>  Location: PIN_F3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_0[4]	=>  Location: PIN_R4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_0[5]	=>  Location: PIN_T5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_0[6]	=>  Location: PIN_N15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c$arg_0[7]	=>  Location: PIN_P7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF topentity IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \ww_c$arg_0\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ww_c$arg_1\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ww_c$arg_2\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ww_c$arg_3\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ww_c$arg_4\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ww_c$arg_5\ : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_result : std_logic_vector(7 DOWNTO 0);
SIGNAL \Mult5~mac_AX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult5~mac_AY_bus\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \Mult5~mac_BX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult5~mac_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \Mult4~mac_AX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult4~mac_AY_bus\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \Mult4~mac_BX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult4~mac_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \Mult3~mac_AX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult3~mac_AY_bus\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \Mult3~mac_BX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult3~mac_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \Mult2~mac_AX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult2~mac_AY_bus\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \Mult2~mac_BX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult2~mac_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \Mult1~mac_AX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult1~mac_AY_bus\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \Mult1~mac_BX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult1~mac_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \Mult0~mac_AX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult0~mac_AY_bus\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \Mult0~mac_BX_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \Mult0~mac_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \Mult5~8\ : std_logic;
SIGNAL \Mult5~9\ : std_logic;
SIGNAL \Mult5~10\ : std_logic;
SIGNAL \Mult5~11\ : std_logic;
SIGNAL \Mult5~12\ : std_logic;
SIGNAL \Mult5~13\ : std_logic;
SIGNAL \Mult5~14\ : std_logic;
SIGNAL \Mult5~16\ : std_logic;
SIGNAL \Mult5~17\ : std_logic;
SIGNAL \Mult5~18\ : std_logic;
SIGNAL \Mult5~19\ : std_logic;
SIGNAL \Mult5~20\ : std_logic;
SIGNAL \Mult5~21\ : std_logic;
SIGNAL \Mult5~22\ : std_logic;
SIGNAL \Mult5~23\ : std_logic;
SIGNAL \Mult5~24\ : std_logic;
SIGNAL \Mult5~25\ : std_logic;
SIGNAL \Mult5~26\ : std_logic;
SIGNAL \Mult5~27\ : std_logic;
SIGNAL \Mult5~28\ : std_logic;
SIGNAL \Mult5~29\ : std_logic;
SIGNAL \Mult5~30\ : std_logic;
SIGNAL \Mult5~31\ : std_logic;
SIGNAL \Mult5~32\ : std_logic;
SIGNAL \Mult5~33\ : std_logic;
SIGNAL \Mult5~34\ : std_logic;
SIGNAL \Mult5~35\ : std_logic;
SIGNAL \Mult5~36\ : std_logic;
SIGNAL \Mult5~37\ : std_logic;
SIGNAL \Mult5~38\ : std_logic;
SIGNAL \Mult5~39\ : std_logic;
SIGNAL \Mult5~40\ : std_logic;
SIGNAL \Mult5~41\ : std_logic;
SIGNAL \Mult5~42\ : std_logic;
SIGNAL \Mult5~43\ : std_logic;
SIGNAL \Mult5~44\ : std_logic;
SIGNAL \Mult5~45\ : std_logic;
SIGNAL \Mult5~46\ : std_logic;
SIGNAL \Mult5~47\ : std_logic;
SIGNAL \Mult5~48\ : std_logic;
SIGNAL \Mult5~49\ : std_logic;
SIGNAL \Mult5~50\ : std_logic;
SIGNAL \Mult5~51\ : std_logic;
SIGNAL \Mult5~52\ : std_logic;
SIGNAL \Mult5~53\ : std_logic;
SIGNAL \Mult5~54\ : std_logic;
SIGNAL \Mult5~55\ : std_logic;
SIGNAL \Mult5~56\ : std_logic;
SIGNAL \Mult5~57\ : std_logic;
SIGNAL \Mult5~58\ : std_logic;
SIGNAL \Mult5~59\ : std_logic;
SIGNAL \Mult5~60\ : std_logic;
SIGNAL \Mult5~61\ : std_logic;
SIGNAL \Mult5~62\ : std_logic;
SIGNAL \Mult5~63\ : std_logic;
SIGNAL \Mult5~64\ : std_logic;
SIGNAL \Mult4~8\ : std_logic;
SIGNAL \Mult4~9\ : std_logic;
SIGNAL \Mult4~10\ : std_logic;
SIGNAL \Mult4~11\ : std_logic;
SIGNAL \Mult4~12\ : std_logic;
SIGNAL \Mult4~13\ : std_logic;
SIGNAL \Mult4~14\ : std_logic;
SIGNAL \Mult4~16\ : std_logic;
SIGNAL \Mult4~17\ : std_logic;
SIGNAL \Mult4~18\ : std_logic;
SIGNAL \Mult4~19\ : std_logic;
SIGNAL \Mult4~20\ : std_logic;
SIGNAL \Mult4~21\ : std_logic;
SIGNAL \Mult4~22\ : std_logic;
SIGNAL \Mult4~23\ : std_logic;
SIGNAL \Mult4~24\ : std_logic;
SIGNAL \Mult4~25\ : std_logic;
SIGNAL \Mult4~26\ : std_logic;
SIGNAL \Mult4~27\ : std_logic;
SIGNAL \Mult4~28\ : std_logic;
SIGNAL \Mult4~29\ : std_logic;
SIGNAL \Mult4~30\ : std_logic;
SIGNAL \Mult4~31\ : std_logic;
SIGNAL \Mult4~32\ : std_logic;
SIGNAL \Mult4~33\ : std_logic;
SIGNAL \Mult4~34\ : std_logic;
SIGNAL \Mult4~35\ : std_logic;
SIGNAL \Mult4~36\ : std_logic;
SIGNAL \Mult4~37\ : std_logic;
SIGNAL \Mult4~38\ : std_logic;
SIGNAL \Mult4~39\ : std_logic;
SIGNAL \Mult4~40\ : std_logic;
SIGNAL \Mult4~41\ : std_logic;
SIGNAL \Mult4~42\ : std_logic;
SIGNAL \Mult4~43\ : std_logic;
SIGNAL \Mult4~44\ : std_logic;
SIGNAL \Mult4~45\ : std_logic;
SIGNAL \Mult4~46\ : std_logic;
SIGNAL \Mult4~47\ : std_logic;
SIGNAL \Mult4~48\ : std_logic;
SIGNAL \Mult4~49\ : std_logic;
SIGNAL \Mult4~50\ : std_logic;
SIGNAL \Mult4~51\ : std_logic;
SIGNAL \Mult4~52\ : std_logic;
SIGNAL \Mult4~53\ : std_logic;
SIGNAL \Mult4~54\ : std_logic;
SIGNAL \Mult4~55\ : std_logic;
SIGNAL \Mult4~56\ : std_logic;
SIGNAL \Mult4~57\ : std_logic;
SIGNAL \Mult4~58\ : std_logic;
SIGNAL \Mult4~59\ : std_logic;
SIGNAL \Mult4~60\ : std_logic;
SIGNAL \Mult4~61\ : std_logic;
SIGNAL \Mult4~62\ : std_logic;
SIGNAL \Mult4~63\ : std_logic;
SIGNAL \Mult4~64\ : std_logic;
SIGNAL \Mult3~8\ : std_logic;
SIGNAL \Mult3~9\ : std_logic;
SIGNAL \Mult3~10\ : std_logic;
SIGNAL \Mult3~11\ : std_logic;
SIGNAL \Mult3~12\ : std_logic;
SIGNAL \Mult3~13\ : std_logic;
SIGNAL \Mult3~14\ : std_logic;
SIGNAL \Mult3~16\ : std_logic;
SIGNAL \Mult3~17\ : std_logic;
SIGNAL \Mult3~18\ : std_logic;
SIGNAL \Mult3~19\ : std_logic;
SIGNAL \Mult3~20\ : std_logic;
SIGNAL \Mult3~21\ : std_logic;
SIGNAL \Mult3~22\ : std_logic;
SIGNAL \Mult3~23\ : std_logic;
SIGNAL \Mult3~24\ : std_logic;
SIGNAL \Mult3~25\ : std_logic;
SIGNAL \Mult3~26\ : std_logic;
SIGNAL \Mult3~27\ : std_logic;
SIGNAL \Mult3~28\ : std_logic;
SIGNAL \Mult3~29\ : std_logic;
SIGNAL \Mult3~30\ : std_logic;
SIGNAL \Mult3~31\ : std_logic;
SIGNAL \Mult3~32\ : std_logic;
SIGNAL \Mult3~33\ : std_logic;
SIGNAL \Mult3~34\ : std_logic;
SIGNAL \Mult3~35\ : std_logic;
SIGNAL \Mult3~36\ : std_logic;
SIGNAL \Mult3~37\ : std_logic;
SIGNAL \Mult3~38\ : std_logic;
SIGNAL \Mult3~39\ : std_logic;
SIGNAL \Mult3~40\ : std_logic;
SIGNAL \Mult3~41\ : std_logic;
SIGNAL \Mult3~42\ : std_logic;
SIGNAL \Mult3~43\ : std_logic;
SIGNAL \Mult3~44\ : std_logic;
SIGNAL \Mult3~45\ : std_logic;
SIGNAL \Mult3~46\ : std_logic;
SIGNAL \Mult3~47\ : std_logic;
SIGNAL \Mult3~48\ : std_logic;
SIGNAL \Mult3~49\ : std_logic;
SIGNAL \Mult3~50\ : std_logic;
SIGNAL \Mult3~51\ : std_logic;
SIGNAL \Mult3~52\ : std_logic;
SIGNAL \Mult3~53\ : std_logic;
SIGNAL \Mult3~54\ : std_logic;
SIGNAL \Mult3~55\ : std_logic;
SIGNAL \Mult3~56\ : std_logic;
SIGNAL \Mult3~57\ : std_logic;
SIGNAL \Mult3~58\ : std_logic;
SIGNAL \Mult3~59\ : std_logic;
SIGNAL \Mult3~60\ : std_logic;
SIGNAL \Mult3~61\ : std_logic;
SIGNAL \Mult3~62\ : std_logic;
SIGNAL \Mult3~63\ : std_logic;
SIGNAL \Mult3~64\ : std_logic;
SIGNAL \Mult2~8\ : std_logic;
SIGNAL \Mult2~9\ : std_logic;
SIGNAL \Mult2~10\ : std_logic;
SIGNAL \Mult2~11\ : std_logic;
SIGNAL \Mult2~12\ : std_logic;
SIGNAL \Mult2~13\ : std_logic;
SIGNAL \Mult2~14\ : std_logic;
SIGNAL \Mult2~16\ : std_logic;
SIGNAL \Mult2~17\ : std_logic;
SIGNAL \Mult2~18\ : std_logic;
SIGNAL \Mult2~19\ : std_logic;
SIGNAL \Mult2~20\ : std_logic;
SIGNAL \Mult2~21\ : std_logic;
SIGNAL \Mult2~22\ : std_logic;
SIGNAL \Mult2~23\ : std_logic;
SIGNAL \Mult2~24\ : std_logic;
SIGNAL \Mult2~25\ : std_logic;
SIGNAL \Mult2~26\ : std_logic;
SIGNAL \Mult2~27\ : std_logic;
SIGNAL \Mult2~28\ : std_logic;
SIGNAL \Mult2~29\ : std_logic;
SIGNAL \Mult2~30\ : std_logic;
SIGNAL \Mult2~31\ : std_logic;
SIGNAL \Mult2~32\ : std_logic;
SIGNAL \Mult2~33\ : std_logic;
SIGNAL \Mult2~34\ : std_logic;
SIGNAL \Mult2~35\ : std_logic;
SIGNAL \Mult2~36\ : std_logic;
SIGNAL \Mult2~37\ : std_logic;
SIGNAL \Mult2~38\ : std_logic;
SIGNAL \Mult2~39\ : std_logic;
SIGNAL \Mult2~40\ : std_logic;
SIGNAL \Mult2~41\ : std_logic;
SIGNAL \Mult2~42\ : std_logic;
SIGNAL \Mult2~43\ : std_logic;
SIGNAL \Mult2~44\ : std_logic;
SIGNAL \Mult2~45\ : std_logic;
SIGNAL \Mult2~46\ : std_logic;
SIGNAL \Mult2~47\ : std_logic;
SIGNAL \Mult2~48\ : std_logic;
SIGNAL \Mult2~49\ : std_logic;
SIGNAL \Mult2~50\ : std_logic;
SIGNAL \Mult2~51\ : std_logic;
SIGNAL \Mult2~52\ : std_logic;
SIGNAL \Mult2~53\ : std_logic;
SIGNAL \Mult2~54\ : std_logic;
SIGNAL \Mult2~55\ : std_logic;
SIGNAL \Mult2~56\ : std_logic;
SIGNAL \Mult2~57\ : std_logic;
SIGNAL \Mult2~58\ : std_logic;
SIGNAL \Mult2~59\ : std_logic;
SIGNAL \Mult2~60\ : std_logic;
SIGNAL \Mult2~61\ : std_logic;
SIGNAL \Mult2~62\ : std_logic;
SIGNAL \Mult2~63\ : std_logic;
SIGNAL \Mult2~64\ : std_logic;
SIGNAL \Mult1~8\ : std_logic;
SIGNAL \Mult1~9\ : std_logic;
SIGNAL \Mult1~10\ : std_logic;
SIGNAL \Mult1~11\ : std_logic;
SIGNAL \Mult1~12\ : std_logic;
SIGNAL \Mult1~13\ : std_logic;
SIGNAL \Mult1~14\ : std_logic;
SIGNAL \Mult1~16\ : std_logic;
SIGNAL \Mult1~17\ : std_logic;
SIGNAL \Mult1~18\ : std_logic;
SIGNAL \Mult1~19\ : std_logic;
SIGNAL \Mult1~20\ : std_logic;
SIGNAL \Mult1~21\ : std_logic;
SIGNAL \Mult1~22\ : std_logic;
SIGNAL \Mult1~23\ : std_logic;
SIGNAL \Mult1~24\ : std_logic;
SIGNAL \Mult1~25\ : std_logic;
SIGNAL \Mult1~26\ : std_logic;
SIGNAL \Mult1~27\ : std_logic;
SIGNAL \Mult1~28\ : std_logic;
SIGNAL \Mult1~29\ : std_logic;
SIGNAL \Mult1~30\ : std_logic;
SIGNAL \Mult1~31\ : std_logic;
SIGNAL \Mult1~32\ : std_logic;
SIGNAL \Mult1~33\ : std_logic;
SIGNAL \Mult1~34\ : std_logic;
SIGNAL \Mult1~35\ : std_logic;
SIGNAL \Mult1~36\ : std_logic;
SIGNAL \Mult1~37\ : std_logic;
SIGNAL \Mult1~38\ : std_logic;
SIGNAL \Mult1~39\ : std_logic;
SIGNAL \Mult1~40\ : std_logic;
SIGNAL \Mult1~41\ : std_logic;
SIGNAL \Mult1~42\ : std_logic;
SIGNAL \Mult1~43\ : std_logic;
SIGNAL \Mult1~44\ : std_logic;
SIGNAL \Mult1~45\ : std_logic;
SIGNAL \Mult1~46\ : std_logic;
SIGNAL \Mult1~47\ : std_logic;
SIGNAL \Mult1~48\ : std_logic;
SIGNAL \Mult1~49\ : std_logic;
SIGNAL \Mult1~50\ : std_logic;
SIGNAL \Mult1~51\ : std_logic;
SIGNAL \Mult1~52\ : std_logic;
SIGNAL \Mult1~53\ : std_logic;
SIGNAL \Mult1~54\ : std_logic;
SIGNAL \Mult1~55\ : std_logic;
SIGNAL \Mult1~56\ : std_logic;
SIGNAL \Mult1~57\ : std_logic;
SIGNAL \Mult1~58\ : std_logic;
SIGNAL \Mult1~59\ : std_logic;
SIGNAL \Mult1~60\ : std_logic;
SIGNAL \Mult1~61\ : std_logic;
SIGNAL \Mult1~62\ : std_logic;
SIGNAL \Mult1~63\ : std_logic;
SIGNAL \Mult1~64\ : std_logic;
SIGNAL \Mult0~8\ : std_logic;
SIGNAL \Mult0~9\ : std_logic;
SIGNAL \Mult0~10\ : std_logic;
SIGNAL \Mult0~11\ : std_logic;
SIGNAL \Mult0~12\ : std_logic;
SIGNAL \Mult0~13\ : std_logic;
SIGNAL \Mult0~14\ : std_logic;
SIGNAL \Mult0~16\ : std_logic;
SIGNAL \Mult0~17\ : std_logic;
SIGNAL \Mult0~18\ : std_logic;
SIGNAL \Mult0~19\ : std_logic;
SIGNAL \Mult0~20\ : std_logic;
SIGNAL \Mult0~21\ : std_logic;
SIGNAL \Mult0~22\ : std_logic;
SIGNAL \Mult0~23\ : std_logic;
SIGNAL \Mult0~24\ : std_logic;
SIGNAL \Mult0~25\ : std_logic;
SIGNAL \Mult0~26\ : std_logic;
SIGNAL \Mult0~27\ : std_logic;
SIGNAL \Mult0~28\ : std_logic;
SIGNAL \Mult0~29\ : std_logic;
SIGNAL \Mult0~30\ : std_logic;
SIGNAL \Mult0~31\ : std_logic;
SIGNAL \Mult0~32\ : std_logic;
SIGNAL \Mult0~33\ : std_logic;
SIGNAL \Mult0~34\ : std_logic;
SIGNAL \Mult0~35\ : std_logic;
SIGNAL \Mult0~36\ : std_logic;
SIGNAL \Mult0~37\ : std_logic;
SIGNAL \Mult0~38\ : std_logic;
SIGNAL \Mult0~39\ : std_logic;
SIGNAL \Mult0~40\ : std_logic;
SIGNAL \Mult0~41\ : std_logic;
SIGNAL \Mult0~42\ : std_logic;
SIGNAL \Mult0~43\ : std_logic;
SIGNAL \Mult0~44\ : std_logic;
SIGNAL \Mult0~45\ : std_logic;
SIGNAL \Mult0~46\ : std_logic;
SIGNAL \Mult0~47\ : std_logic;
SIGNAL \Mult0~48\ : std_logic;
SIGNAL \Mult0~49\ : std_logic;
SIGNAL \Mult0~50\ : std_logic;
SIGNAL \Mult0~51\ : std_logic;
SIGNAL \Mult0~52\ : std_logic;
SIGNAL \Mult0~53\ : std_logic;
SIGNAL \Mult0~54\ : std_logic;
SIGNAL \Mult0~55\ : std_logic;
SIGNAL \Mult0~56\ : std_logic;
SIGNAL \Mult0~57\ : std_logic;
SIGNAL \Mult0~58\ : std_logic;
SIGNAL \Mult0~59\ : std_logic;
SIGNAL \Mult0~60\ : std_logic;
SIGNAL \Mult0~61\ : std_logic;
SIGNAL \Mult0~62\ : std_logic;
SIGNAL \Mult0~63\ : std_logic;
SIGNAL \Mult0~64\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \c$arg_5[0]~input_o\ : std_logic;
SIGNAL \c$arg_5[1]~input_o\ : std_logic;
SIGNAL \c$arg_5[2]~input_o\ : std_logic;
SIGNAL \c$arg_5[3]~input_o\ : std_logic;
SIGNAL \c$arg_5[4]~input_o\ : std_logic;
SIGNAL \c$arg_5[5]~input_o\ : std_logic;
SIGNAL \c$arg_5[6]~input_o\ : std_logic;
SIGNAL \c$arg_5[7]~input_o\ : std_logic;
SIGNAL \c$ws2_app_arg[5][0]\ : std_logic;
SIGNAL \c$arg_4[0]~input_o\ : std_logic;
SIGNAL \c$arg_4[1]~input_o\ : std_logic;
SIGNAL \c$arg_4[2]~input_o\ : std_logic;
SIGNAL \c$arg_4[3]~input_o\ : std_logic;
SIGNAL \c$arg_4[4]~input_o\ : std_logic;
SIGNAL \c$arg_4[5]~input_o\ : std_logic;
SIGNAL \c$arg_4[6]~input_o\ : std_logic;
SIGNAL \c$arg_4[7]~input_o\ : std_logic;
SIGNAL \c$ws2_app_arg[4][0]\ : std_logic;
SIGNAL \c$arg_3[0]~input_o\ : std_logic;
SIGNAL \c$arg_3[1]~input_o\ : std_logic;
SIGNAL \c$arg_3[2]~input_o\ : std_logic;
SIGNAL \c$arg_3[3]~input_o\ : std_logic;
SIGNAL \c$arg_3[4]~input_o\ : std_logic;
SIGNAL \c$arg_3[5]~input_o\ : std_logic;
SIGNAL \c$arg_3[6]~input_o\ : std_logic;
SIGNAL \c$arg_3[7]~input_o\ : std_logic;
SIGNAL \c$ws2_app_arg[3][0]\ : std_logic;
SIGNAL \Add2~1_sumout\ : std_logic;
SIGNAL \c$arg_2[0]~input_o\ : std_logic;
SIGNAL \c$arg_2[1]~input_o\ : std_logic;
SIGNAL \c$arg_2[2]~input_o\ : std_logic;
SIGNAL \c$arg_2[3]~input_o\ : std_logic;
SIGNAL \c$arg_2[4]~input_o\ : std_logic;
SIGNAL \c$arg_2[5]~input_o\ : std_logic;
SIGNAL \c$arg_2[6]~input_o\ : std_logic;
SIGNAL \c$arg_2[7]~input_o\ : std_logic;
SIGNAL \c$ws2_app_arg[2][0]\ : std_logic;
SIGNAL \c$arg_1[0]~input_o\ : std_logic;
SIGNAL \c$arg_1[1]~input_o\ : std_logic;
SIGNAL \c$arg_1[2]~input_o\ : std_logic;
SIGNAL \c$arg_1[3]~input_o\ : std_logic;
SIGNAL \c$arg_1[4]~input_o\ : std_logic;
SIGNAL \c$arg_1[5]~input_o\ : std_logic;
SIGNAL \c$arg_1[6]~input_o\ : std_logic;
SIGNAL \c$arg_1[7]~input_o\ : std_logic;
SIGNAL \c$ws2_app_arg[1][0]\ : std_logic;
SIGNAL \c$arg_0[0]~input_o\ : std_logic;
SIGNAL \c$arg_0[1]~input_o\ : std_logic;
SIGNAL \c$arg_0[2]~input_o\ : std_logic;
SIGNAL \c$arg_0[3]~input_o\ : std_logic;
SIGNAL \c$arg_0[4]~input_o\ : std_logic;
SIGNAL \c$arg_0[5]~input_o\ : std_logic;
SIGNAL \c$arg_0[6]~input_o\ : std_logic;
SIGNAL \c$arg_0[7]~input_o\ : std_logic;
SIGNAL \c$ws2_app_arg[0][0]\ : std_logic;
SIGNAL \Add1~33_sumout\ : std_logic;
SIGNAL \Add1~1_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[5][1]\ : std_logic;
SIGNAL \c$ws2_app_arg[3][1]\ : std_logic;
SIGNAL \c$ws2_app_arg[4][1]\ : std_logic;
SIGNAL \Add2~2\ : std_logic;
SIGNAL \Add2~3\ : std_logic;
SIGNAL \Add2~5_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[0][1]\ : std_logic;
SIGNAL \c$ws2_app_arg[2][1]\ : std_logic;
SIGNAL \c$ws2_app_arg[1][1]\ : std_logic;
SIGNAL \Add1~34\ : std_logic;
SIGNAL \Add1~35\ : std_logic;
SIGNAL \Add1~37_sumout\ : std_logic;
SIGNAL \Add1~2\ : std_logic;
SIGNAL \Add1~5_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[1][2]\ : std_logic;
SIGNAL \c$ws2_app_arg[2][2]\ : std_logic;
SIGNAL \c$ws2_app_arg[0][2]\ : std_logic;
SIGNAL \Add1~38\ : std_logic;
SIGNAL \Add1~39\ : std_logic;
SIGNAL \Add1~41_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[5][2]\ : std_logic;
SIGNAL \c$ws2_app_arg[4][2]\ : std_logic;
SIGNAL \c$ws2_app_arg[3][2]\ : std_logic;
SIGNAL \Add2~6\ : std_logic;
SIGNAL \Add2~7\ : std_logic;
SIGNAL \Add2~9_sumout\ : std_logic;
SIGNAL \Add1~6\ : std_logic;
SIGNAL \Add1~9_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[1][3]\ : std_logic;
SIGNAL \c$ws2_app_arg[2][3]\ : std_logic;
SIGNAL \c$ws2_app_arg[0][3]\ : std_logic;
SIGNAL \Add1~42\ : std_logic;
SIGNAL \Add1~43\ : std_logic;
SIGNAL \Add1~45_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[4][3]\ : std_logic;
SIGNAL \c$ws2_app_arg[5][3]\ : std_logic;
SIGNAL \c$ws2_app_arg[3][3]\ : std_logic;
SIGNAL \Add2~10\ : std_logic;
SIGNAL \Add2~11\ : std_logic;
SIGNAL \Add2~13_sumout\ : std_logic;
SIGNAL \Add1~10\ : std_logic;
SIGNAL \Add1~13_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[4][4]\ : std_logic;
SIGNAL \c$ws2_app_arg[5][4]\ : std_logic;
SIGNAL \c$ws2_app_arg[3][4]\ : std_logic;
SIGNAL \Add2~14\ : std_logic;
SIGNAL \Add2~15\ : std_logic;
SIGNAL \Add2~17_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[1][4]\ : std_logic;
SIGNAL \c$ws2_app_arg[2][4]\ : std_logic;
SIGNAL \c$ws2_app_arg[0][4]\ : std_logic;
SIGNAL \Add1~46\ : std_logic;
SIGNAL \Add1~47\ : std_logic;
SIGNAL \Add1~49_sumout\ : std_logic;
SIGNAL \Add1~14\ : std_logic;
SIGNAL \Add1~17_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[2][5]\ : std_logic;
SIGNAL \c$ws2_app_arg[1][5]\ : std_logic;
SIGNAL \c$ws2_app_arg[0][5]\ : std_logic;
SIGNAL \Add1~50\ : std_logic;
SIGNAL \Add1~51\ : std_logic;
SIGNAL \Add1~53_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[3][5]\ : std_logic;
SIGNAL \c$ws2_app_arg[5][5]\ : std_logic;
SIGNAL \c$ws2_app_arg[4][5]\ : std_logic;
SIGNAL \Add2~18\ : std_logic;
SIGNAL \Add2~19\ : std_logic;
SIGNAL \Add2~21_sumout\ : std_logic;
SIGNAL \Add1~18\ : std_logic;
SIGNAL \Add1~21_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[1][6]\ : std_logic;
SIGNAL \c$ws2_app_arg[0][6]\ : std_logic;
SIGNAL \c$ws2_app_arg[2][6]\ : std_logic;
SIGNAL \Add1~54\ : std_logic;
SIGNAL \Add1~55\ : std_logic;
SIGNAL \Add1~57_sumout\ : std_logic;
SIGNAL \c$ws2_app_arg[3][6]\ : std_logic;
SIGNAL \c$ws2_app_arg[5][6]\ : std_logic;
SIGNAL \c$ws2_app_arg[4][6]\ : std_logic;
SIGNAL \Add2~22\ : std_logic;
SIGNAL \Add2~23\ : std_logic;
SIGNAL \Add2~25_sumout\ : std_logic;
SIGNAL \Add1~22\ : std_logic;
SIGNAL \Add1~25_sumout\ : std_logic;
SIGNAL \Mult3~15\ : std_logic;
SIGNAL \Mult5~15\ : std_logic;
SIGNAL \Mult4~15\ : std_logic;
SIGNAL \Add2~26\ : std_logic;
SIGNAL \Add2~27\ : std_logic;
SIGNAL \Add2~29_sumout\ : std_logic;
SIGNAL \Mult1~15\ : std_logic;
SIGNAL \Mult0~15\ : std_logic;
SIGNAL \Mult2~15\ : std_logic;
SIGNAL \Add1~58\ : std_logic;
SIGNAL \Add1~59\ : std_logic;
SIGNAL \Add1~61_sumout\ : std_logic;
SIGNAL \Add1~26\ : std_logic;
SIGNAL \Add1~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Mult4~15\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[4][6]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[4][5]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[4][4]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[4][3]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[4][2]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[4][1]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[4][0]\ : std_logic;
SIGNAL \ALT_INV_Mult5~15\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[5][6]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[5][5]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[5][4]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[5][3]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[5][2]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[5][1]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[5][0]\ : std_logic;
SIGNAL \ALT_INV_Add1~61_sumout\ : std_logic;
SIGNAL \ALT_INV_Add2~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~57_sumout\ : std_logic;
SIGNAL \ALT_INV_Add2~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~53_sumout\ : std_logic;
SIGNAL \ALT_INV_Add2~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~49_sumout\ : std_logic;
SIGNAL \ALT_INV_Add2~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~45_sumout\ : std_logic;
SIGNAL \ALT_INV_Add2~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~41_sumout\ : std_logic;
SIGNAL \ALT_INV_Add2~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~37_sumout\ : std_logic;
SIGNAL \ALT_INV_Add2~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~33_sumout\ : std_logic;
SIGNAL \ALT_INV_Add2~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Mult1~15\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[1][6]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[1][5]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[1][4]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[1][3]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[1][2]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[1][1]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[1][0]\ : std_logic;
SIGNAL \ALT_INV_Mult2~15\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[2][6]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[2][5]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[2][4]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[2][3]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[2][2]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[2][1]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[2][0]\ : std_logic;
SIGNAL \ALT_INV_Mult3~15\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[3][6]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[3][5]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[3][4]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[3][3]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[3][2]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[3][1]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[3][0]\ : std_logic;
SIGNAL \ALT_INV_Mult0~15\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[0][6]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[0][5]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[0][4]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[0][3]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[0][2]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[0][1]\ : std_logic;
SIGNAL \ALT_INV_c$ws2_app_arg[0][0]\ : std_logic;

BEGIN

\ww_c$arg_0\ <= \c$arg_0\;
\ww_c$arg_1\ <= \c$arg_1\;
\ww_c$arg_2\ <= \c$arg_2\;
\ww_c$arg_3\ <= \c$arg_3\;
\ww_c$arg_4\ <= \c$arg_4\;
\ww_c$arg_5\ <= \c$arg_5\;
result <= IEEE.NUMERIC_STD.SIGNED(ww_result);
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\Mult5~mac_AX_bus\ <= (\c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\ & \c$arg_5[6]~input_o\ & \c$arg_5[5]~input_o\ & \c$arg_5[4]~input_o\ & \c$arg_5[3]~input_o\ & \c$arg_5[2]~input_o\ & \c$arg_5[1]~input_o\ & \c$arg_5[0]~input_o\);

\Mult5~mac_AY_bus\ <= (gnd & gnd & gnd & gnd & vcc & vcc & gnd);

\Mult5~mac_BX_bus\ <= (\c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\ & \c$arg_5[7]~input_o\);

\c$ws2_app_arg[5][0]\ <= \Mult5~mac_RESULTA_bus\(0);
\c$ws2_app_arg[5][1]\ <= \Mult5~mac_RESULTA_bus\(1);
\c$ws2_app_arg[5][2]\ <= \Mult5~mac_RESULTA_bus\(2);
\c$ws2_app_arg[5][3]\ <= \Mult5~mac_RESULTA_bus\(3);
\c$ws2_app_arg[5][4]\ <= \Mult5~mac_RESULTA_bus\(4);
\c$ws2_app_arg[5][5]\ <= \Mult5~mac_RESULTA_bus\(5);
\c$ws2_app_arg[5][6]\ <= \Mult5~mac_RESULTA_bus\(6);
\Mult5~8\ <= \Mult5~mac_RESULTA_bus\(7);
\Mult5~9\ <= \Mult5~mac_RESULTA_bus\(8);
\Mult5~10\ <= \Mult5~mac_RESULTA_bus\(9);
\Mult5~11\ <= \Mult5~mac_RESULTA_bus\(10);
\Mult5~12\ <= \Mult5~mac_RESULTA_bus\(11);
\Mult5~13\ <= \Mult5~mac_RESULTA_bus\(12);
\Mult5~14\ <= \Mult5~mac_RESULTA_bus\(13);
\Mult5~15\ <= \Mult5~mac_RESULTA_bus\(14);
\Mult5~16\ <= \Mult5~mac_RESULTA_bus\(15);
\Mult5~17\ <= \Mult5~mac_RESULTA_bus\(16);
\Mult5~18\ <= \Mult5~mac_RESULTA_bus\(17);
\Mult5~19\ <= \Mult5~mac_RESULTA_bus\(18);
\Mult5~20\ <= \Mult5~mac_RESULTA_bus\(19);
\Mult5~21\ <= \Mult5~mac_RESULTA_bus\(20);
\Mult5~22\ <= \Mult5~mac_RESULTA_bus\(21);
\Mult5~23\ <= \Mult5~mac_RESULTA_bus\(22);
\Mult5~24\ <= \Mult5~mac_RESULTA_bus\(23);
\Mult5~25\ <= \Mult5~mac_RESULTA_bus\(24);
\Mult5~26\ <= \Mult5~mac_RESULTA_bus\(25);
\Mult5~27\ <= \Mult5~mac_RESULTA_bus\(26);
\Mult5~28\ <= \Mult5~mac_RESULTA_bus\(27);
\Mult5~29\ <= \Mult5~mac_RESULTA_bus\(28);
\Mult5~30\ <= \Mult5~mac_RESULTA_bus\(29);
\Mult5~31\ <= \Mult5~mac_RESULTA_bus\(30);
\Mult5~32\ <= \Mult5~mac_RESULTA_bus\(31);
\Mult5~33\ <= \Mult5~mac_RESULTA_bus\(32);
\Mult5~34\ <= \Mult5~mac_RESULTA_bus\(33);
\Mult5~35\ <= \Mult5~mac_RESULTA_bus\(34);
\Mult5~36\ <= \Mult5~mac_RESULTA_bus\(35);
\Mult5~37\ <= \Mult5~mac_RESULTA_bus\(36);
\Mult5~38\ <= \Mult5~mac_RESULTA_bus\(37);
\Mult5~39\ <= \Mult5~mac_RESULTA_bus\(38);
\Mult5~40\ <= \Mult5~mac_RESULTA_bus\(39);
\Mult5~41\ <= \Mult5~mac_RESULTA_bus\(40);
\Mult5~42\ <= \Mult5~mac_RESULTA_bus\(41);
\Mult5~43\ <= \Mult5~mac_RESULTA_bus\(42);
\Mult5~44\ <= \Mult5~mac_RESULTA_bus\(43);
\Mult5~45\ <= \Mult5~mac_RESULTA_bus\(44);
\Mult5~46\ <= \Mult5~mac_RESULTA_bus\(45);
\Mult5~47\ <= \Mult5~mac_RESULTA_bus\(46);
\Mult5~48\ <= \Mult5~mac_RESULTA_bus\(47);
\Mult5~49\ <= \Mult5~mac_RESULTA_bus\(48);
\Mult5~50\ <= \Mult5~mac_RESULTA_bus\(49);
\Mult5~51\ <= \Mult5~mac_RESULTA_bus\(50);
\Mult5~52\ <= \Mult5~mac_RESULTA_bus\(51);
\Mult5~53\ <= \Mult5~mac_RESULTA_bus\(52);
\Mult5~54\ <= \Mult5~mac_RESULTA_bus\(53);
\Mult5~55\ <= \Mult5~mac_RESULTA_bus\(54);
\Mult5~56\ <= \Mult5~mac_RESULTA_bus\(55);
\Mult5~57\ <= \Mult5~mac_RESULTA_bus\(56);
\Mult5~58\ <= \Mult5~mac_RESULTA_bus\(57);
\Mult5~59\ <= \Mult5~mac_RESULTA_bus\(58);
\Mult5~60\ <= \Mult5~mac_RESULTA_bus\(59);
\Mult5~61\ <= \Mult5~mac_RESULTA_bus\(60);
\Mult5~62\ <= \Mult5~mac_RESULTA_bus\(61);
\Mult5~63\ <= \Mult5~mac_RESULTA_bus\(62);
\Mult5~64\ <= \Mult5~mac_RESULTA_bus\(63);

\Mult4~mac_AX_bus\ <= (\c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\ & \c$arg_4[6]~input_o\ & \c$arg_4[5]~input_o\ & \c$arg_4[4]~input_o\ & \c$arg_4[3]~input_o\ & \c$arg_4[2]~input_o\ & \c$arg_4[1]~input_o\ & \c$arg_4[0]~input_o\);

\Mult4~mac_AY_bus\ <= (gnd & gnd & gnd & gnd & vcc & vcc & vcc);

\Mult4~mac_BX_bus\ <= (\c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\ & \c$arg_4[7]~input_o\);

\c$ws2_app_arg[4][0]\ <= \Mult4~mac_RESULTA_bus\(0);
\c$ws2_app_arg[4][1]\ <= \Mult4~mac_RESULTA_bus\(1);
\c$ws2_app_arg[4][2]\ <= \Mult4~mac_RESULTA_bus\(2);
\c$ws2_app_arg[4][3]\ <= \Mult4~mac_RESULTA_bus\(3);
\c$ws2_app_arg[4][4]\ <= \Mult4~mac_RESULTA_bus\(4);
\c$ws2_app_arg[4][5]\ <= \Mult4~mac_RESULTA_bus\(5);
\c$ws2_app_arg[4][6]\ <= \Mult4~mac_RESULTA_bus\(6);
\Mult4~8\ <= \Mult4~mac_RESULTA_bus\(7);
\Mult4~9\ <= \Mult4~mac_RESULTA_bus\(8);
\Mult4~10\ <= \Mult4~mac_RESULTA_bus\(9);
\Mult4~11\ <= \Mult4~mac_RESULTA_bus\(10);
\Mult4~12\ <= \Mult4~mac_RESULTA_bus\(11);
\Mult4~13\ <= \Mult4~mac_RESULTA_bus\(12);
\Mult4~14\ <= \Mult4~mac_RESULTA_bus\(13);
\Mult4~15\ <= \Mult4~mac_RESULTA_bus\(14);
\Mult4~16\ <= \Mult4~mac_RESULTA_bus\(15);
\Mult4~17\ <= \Mult4~mac_RESULTA_bus\(16);
\Mult4~18\ <= \Mult4~mac_RESULTA_bus\(17);
\Mult4~19\ <= \Mult4~mac_RESULTA_bus\(18);
\Mult4~20\ <= \Mult4~mac_RESULTA_bus\(19);
\Mult4~21\ <= \Mult4~mac_RESULTA_bus\(20);
\Mult4~22\ <= \Mult4~mac_RESULTA_bus\(21);
\Mult4~23\ <= \Mult4~mac_RESULTA_bus\(22);
\Mult4~24\ <= \Mult4~mac_RESULTA_bus\(23);
\Mult4~25\ <= \Mult4~mac_RESULTA_bus\(24);
\Mult4~26\ <= \Mult4~mac_RESULTA_bus\(25);
\Mult4~27\ <= \Mult4~mac_RESULTA_bus\(26);
\Mult4~28\ <= \Mult4~mac_RESULTA_bus\(27);
\Mult4~29\ <= \Mult4~mac_RESULTA_bus\(28);
\Mult4~30\ <= \Mult4~mac_RESULTA_bus\(29);
\Mult4~31\ <= \Mult4~mac_RESULTA_bus\(30);
\Mult4~32\ <= \Mult4~mac_RESULTA_bus\(31);
\Mult4~33\ <= \Mult4~mac_RESULTA_bus\(32);
\Mult4~34\ <= \Mult4~mac_RESULTA_bus\(33);
\Mult4~35\ <= \Mult4~mac_RESULTA_bus\(34);
\Mult4~36\ <= \Mult4~mac_RESULTA_bus\(35);
\Mult4~37\ <= \Mult4~mac_RESULTA_bus\(36);
\Mult4~38\ <= \Mult4~mac_RESULTA_bus\(37);
\Mult4~39\ <= \Mult4~mac_RESULTA_bus\(38);
\Mult4~40\ <= \Mult4~mac_RESULTA_bus\(39);
\Mult4~41\ <= \Mult4~mac_RESULTA_bus\(40);
\Mult4~42\ <= \Mult4~mac_RESULTA_bus\(41);
\Mult4~43\ <= \Mult4~mac_RESULTA_bus\(42);
\Mult4~44\ <= \Mult4~mac_RESULTA_bus\(43);
\Mult4~45\ <= \Mult4~mac_RESULTA_bus\(44);
\Mult4~46\ <= \Mult4~mac_RESULTA_bus\(45);
\Mult4~47\ <= \Mult4~mac_RESULTA_bus\(46);
\Mult4~48\ <= \Mult4~mac_RESULTA_bus\(47);
\Mult4~49\ <= \Mult4~mac_RESULTA_bus\(48);
\Mult4~50\ <= \Mult4~mac_RESULTA_bus\(49);
\Mult4~51\ <= \Mult4~mac_RESULTA_bus\(50);
\Mult4~52\ <= \Mult4~mac_RESULTA_bus\(51);
\Mult4~53\ <= \Mult4~mac_RESULTA_bus\(52);
\Mult4~54\ <= \Mult4~mac_RESULTA_bus\(53);
\Mult4~55\ <= \Mult4~mac_RESULTA_bus\(54);
\Mult4~56\ <= \Mult4~mac_RESULTA_bus\(55);
\Mult4~57\ <= \Mult4~mac_RESULTA_bus\(56);
\Mult4~58\ <= \Mult4~mac_RESULTA_bus\(57);
\Mult4~59\ <= \Mult4~mac_RESULTA_bus\(58);
\Mult4~60\ <= \Mult4~mac_RESULTA_bus\(59);
\Mult4~61\ <= \Mult4~mac_RESULTA_bus\(60);
\Mult4~62\ <= \Mult4~mac_RESULTA_bus\(61);
\Mult4~63\ <= \Mult4~mac_RESULTA_bus\(62);
\Mult4~64\ <= \Mult4~mac_RESULTA_bus\(63);

\Mult3~mac_AX_bus\ <= (\c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\ & \c$arg_3[6]~input_o\ & \c$arg_3[5]~input_o\ & \c$arg_3[4]~input_o\ & \c$arg_3[3]~input_o\ & \c$arg_3[2]~input_o\ & \c$arg_3[1]~input_o\ & \c$arg_3[0]~input_o\);

\Mult3~mac_AY_bus\ <= (gnd & gnd & gnd & gnd & gnd & vcc & gnd);

\Mult3~mac_BX_bus\ <= (\c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\ & \c$arg_3[7]~input_o\);

\c$ws2_app_arg[3][0]\ <= \Mult3~mac_RESULTA_bus\(0);
\c$ws2_app_arg[3][1]\ <= \Mult3~mac_RESULTA_bus\(1);
\c$ws2_app_arg[3][2]\ <= \Mult3~mac_RESULTA_bus\(2);
\c$ws2_app_arg[3][3]\ <= \Mult3~mac_RESULTA_bus\(3);
\c$ws2_app_arg[3][4]\ <= \Mult3~mac_RESULTA_bus\(4);
\c$ws2_app_arg[3][5]\ <= \Mult3~mac_RESULTA_bus\(5);
\c$ws2_app_arg[3][6]\ <= \Mult3~mac_RESULTA_bus\(6);
\Mult3~8\ <= \Mult3~mac_RESULTA_bus\(7);
\Mult3~9\ <= \Mult3~mac_RESULTA_bus\(8);
\Mult3~10\ <= \Mult3~mac_RESULTA_bus\(9);
\Mult3~11\ <= \Mult3~mac_RESULTA_bus\(10);
\Mult3~12\ <= \Mult3~mac_RESULTA_bus\(11);
\Mult3~13\ <= \Mult3~mac_RESULTA_bus\(12);
\Mult3~14\ <= \Mult3~mac_RESULTA_bus\(13);
\Mult3~15\ <= \Mult3~mac_RESULTA_bus\(14);
\Mult3~16\ <= \Mult3~mac_RESULTA_bus\(15);
\Mult3~17\ <= \Mult3~mac_RESULTA_bus\(16);
\Mult3~18\ <= \Mult3~mac_RESULTA_bus\(17);
\Mult3~19\ <= \Mult3~mac_RESULTA_bus\(18);
\Mult3~20\ <= \Mult3~mac_RESULTA_bus\(19);
\Mult3~21\ <= \Mult3~mac_RESULTA_bus\(20);
\Mult3~22\ <= \Mult3~mac_RESULTA_bus\(21);
\Mult3~23\ <= \Mult3~mac_RESULTA_bus\(22);
\Mult3~24\ <= \Mult3~mac_RESULTA_bus\(23);
\Mult3~25\ <= \Mult3~mac_RESULTA_bus\(24);
\Mult3~26\ <= \Mult3~mac_RESULTA_bus\(25);
\Mult3~27\ <= \Mult3~mac_RESULTA_bus\(26);
\Mult3~28\ <= \Mult3~mac_RESULTA_bus\(27);
\Mult3~29\ <= \Mult3~mac_RESULTA_bus\(28);
\Mult3~30\ <= \Mult3~mac_RESULTA_bus\(29);
\Mult3~31\ <= \Mult3~mac_RESULTA_bus\(30);
\Mult3~32\ <= \Mult3~mac_RESULTA_bus\(31);
\Mult3~33\ <= \Mult3~mac_RESULTA_bus\(32);
\Mult3~34\ <= \Mult3~mac_RESULTA_bus\(33);
\Mult3~35\ <= \Mult3~mac_RESULTA_bus\(34);
\Mult3~36\ <= \Mult3~mac_RESULTA_bus\(35);
\Mult3~37\ <= \Mult3~mac_RESULTA_bus\(36);
\Mult3~38\ <= \Mult3~mac_RESULTA_bus\(37);
\Mult3~39\ <= \Mult3~mac_RESULTA_bus\(38);
\Mult3~40\ <= \Mult3~mac_RESULTA_bus\(39);
\Mult3~41\ <= \Mult3~mac_RESULTA_bus\(40);
\Mult3~42\ <= \Mult3~mac_RESULTA_bus\(41);
\Mult3~43\ <= \Mult3~mac_RESULTA_bus\(42);
\Mult3~44\ <= \Mult3~mac_RESULTA_bus\(43);
\Mult3~45\ <= \Mult3~mac_RESULTA_bus\(44);
\Mult3~46\ <= \Mult3~mac_RESULTA_bus\(45);
\Mult3~47\ <= \Mult3~mac_RESULTA_bus\(46);
\Mult3~48\ <= \Mult3~mac_RESULTA_bus\(47);
\Mult3~49\ <= \Mult3~mac_RESULTA_bus\(48);
\Mult3~50\ <= \Mult3~mac_RESULTA_bus\(49);
\Mult3~51\ <= \Mult3~mac_RESULTA_bus\(50);
\Mult3~52\ <= \Mult3~mac_RESULTA_bus\(51);
\Mult3~53\ <= \Mult3~mac_RESULTA_bus\(52);
\Mult3~54\ <= \Mult3~mac_RESULTA_bus\(53);
\Mult3~55\ <= \Mult3~mac_RESULTA_bus\(54);
\Mult3~56\ <= \Mult3~mac_RESULTA_bus\(55);
\Mult3~57\ <= \Mult3~mac_RESULTA_bus\(56);
\Mult3~58\ <= \Mult3~mac_RESULTA_bus\(57);
\Mult3~59\ <= \Mult3~mac_RESULTA_bus\(58);
\Mult3~60\ <= \Mult3~mac_RESULTA_bus\(59);
\Mult3~61\ <= \Mult3~mac_RESULTA_bus\(60);
\Mult3~62\ <= \Mult3~mac_RESULTA_bus\(61);
\Mult3~63\ <= \Mult3~mac_RESULTA_bus\(62);
\Mult3~64\ <= \Mult3~mac_RESULTA_bus\(63);

\Mult2~mac_AX_bus\ <= (\c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\ & \c$arg_2[6]~input_o\ & \c$arg_2[5]~input_o\ & \c$arg_2[4]~input_o\ & \c$arg_2[3]~input_o\ & \c$arg_2[2]~input_o\ & \c$arg_2[1]~input_o\ & \c$arg_2[0]~input_o\);

\Mult2~mac_AY_bus\ <= (gnd & gnd & gnd & gnd & gnd & vcc & vcc);

\Mult2~mac_BX_bus\ <= (\c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\ & \c$arg_2[7]~input_o\);

\c$ws2_app_arg[2][0]\ <= \Mult2~mac_RESULTA_bus\(0);
\c$ws2_app_arg[2][1]\ <= \Mult2~mac_RESULTA_bus\(1);
\c$ws2_app_arg[2][2]\ <= \Mult2~mac_RESULTA_bus\(2);
\c$ws2_app_arg[2][3]\ <= \Mult2~mac_RESULTA_bus\(3);
\c$ws2_app_arg[2][4]\ <= \Mult2~mac_RESULTA_bus\(4);
\c$ws2_app_arg[2][5]\ <= \Mult2~mac_RESULTA_bus\(5);
\c$ws2_app_arg[2][6]\ <= \Mult2~mac_RESULTA_bus\(6);
\Mult2~8\ <= \Mult2~mac_RESULTA_bus\(7);
\Mult2~9\ <= \Mult2~mac_RESULTA_bus\(8);
\Mult2~10\ <= \Mult2~mac_RESULTA_bus\(9);
\Mult2~11\ <= \Mult2~mac_RESULTA_bus\(10);
\Mult2~12\ <= \Mult2~mac_RESULTA_bus\(11);
\Mult2~13\ <= \Mult2~mac_RESULTA_bus\(12);
\Mult2~14\ <= \Mult2~mac_RESULTA_bus\(13);
\Mult2~15\ <= \Mult2~mac_RESULTA_bus\(14);
\Mult2~16\ <= \Mult2~mac_RESULTA_bus\(15);
\Mult2~17\ <= \Mult2~mac_RESULTA_bus\(16);
\Mult2~18\ <= \Mult2~mac_RESULTA_bus\(17);
\Mult2~19\ <= \Mult2~mac_RESULTA_bus\(18);
\Mult2~20\ <= \Mult2~mac_RESULTA_bus\(19);
\Mult2~21\ <= \Mult2~mac_RESULTA_bus\(20);
\Mult2~22\ <= \Mult2~mac_RESULTA_bus\(21);
\Mult2~23\ <= \Mult2~mac_RESULTA_bus\(22);
\Mult2~24\ <= \Mult2~mac_RESULTA_bus\(23);
\Mult2~25\ <= \Mult2~mac_RESULTA_bus\(24);
\Mult2~26\ <= \Mult2~mac_RESULTA_bus\(25);
\Mult2~27\ <= \Mult2~mac_RESULTA_bus\(26);
\Mult2~28\ <= \Mult2~mac_RESULTA_bus\(27);
\Mult2~29\ <= \Mult2~mac_RESULTA_bus\(28);
\Mult2~30\ <= \Mult2~mac_RESULTA_bus\(29);
\Mult2~31\ <= \Mult2~mac_RESULTA_bus\(30);
\Mult2~32\ <= \Mult2~mac_RESULTA_bus\(31);
\Mult2~33\ <= \Mult2~mac_RESULTA_bus\(32);
\Mult2~34\ <= \Mult2~mac_RESULTA_bus\(33);
\Mult2~35\ <= \Mult2~mac_RESULTA_bus\(34);
\Mult2~36\ <= \Mult2~mac_RESULTA_bus\(35);
\Mult2~37\ <= \Mult2~mac_RESULTA_bus\(36);
\Mult2~38\ <= \Mult2~mac_RESULTA_bus\(37);
\Mult2~39\ <= \Mult2~mac_RESULTA_bus\(38);
\Mult2~40\ <= \Mult2~mac_RESULTA_bus\(39);
\Mult2~41\ <= \Mult2~mac_RESULTA_bus\(40);
\Mult2~42\ <= \Mult2~mac_RESULTA_bus\(41);
\Mult2~43\ <= \Mult2~mac_RESULTA_bus\(42);
\Mult2~44\ <= \Mult2~mac_RESULTA_bus\(43);
\Mult2~45\ <= \Mult2~mac_RESULTA_bus\(44);
\Mult2~46\ <= \Mult2~mac_RESULTA_bus\(45);
\Mult2~47\ <= \Mult2~mac_RESULTA_bus\(46);
\Mult2~48\ <= \Mult2~mac_RESULTA_bus\(47);
\Mult2~49\ <= \Mult2~mac_RESULTA_bus\(48);
\Mult2~50\ <= \Mult2~mac_RESULTA_bus\(49);
\Mult2~51\ <= \Mult2~mac_RESULTA_bus\(50);
\Mult2~52\ <= \Mult2~mac_RESULTA_bus\(51);
\Mult2~53\ <= \Mult2~mac_RESULTA_bus\(52);
\Mult2~54\ <= \Mult2~mac_RESULTA_bus\(53);
\Mult2~55\ <= \Mult2~mac_RESULTA_bus\(54);
\Mult2~56\ <= \Mult2~mac_RESULTA_bus\(55);
\Mult2~57\ <= \Mult2~mac_RESULTA_bus\(56);
\Mult2~58\ <= \Mult2~mac_RESULTA_bus\(57);
\Mult2~59\ <= \Mult2~mac_RESULTA_bus\(58);
\Mult2~60\ <= \Mult2~mac_RESULTA_bus\(59);
\Mult2~61\ <= \Mult2~mac_RESULTA_bus\(60);
\Mult2~62\ <= \Mult2~mac_RESULTA_bus\(61);
\Mult2~63\ <= \Mult2~mac_RESULTA_bus\(62);
\Mult2~64\ <= \Mult2~mac_RESULTA_bus\(63);

\Mult1~mac_AX_bus\ <= (\c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\ & \c$arg_1[6]~input_o\ & \c$arg_1[5]~input_o\ & \c$arg_1[4]~input_o\ & \c$arg_1[3]~input_o\ & \c$arg_1[2]~input_o\ & \c$arg_1[1]~input_o\ & \c$arg_1[0]~input_o\);

\Mult1~mac_AY_bus\ <= (gnd & gnd & gnd & gnd & vcc & gnd & gnd);

\Mult1~mac_BX_bus\ <= (\c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\ & \c$arg_1[7]~input_o\);

\c$ws2_app_arg[1][0]\ <= \Mult1~mac_RESULTA_bus\(0);
\c$ws2_app_arg[1][1]\ <= \Mult1~mac_RESULTA_bus\(1);
\c$ws2_app_arg[1][2]\ <= \Mult1~mac_RESULTA_bus\(2);
\c$ws2_app_arg[1][3]\ <= \Mult1~mac_RESULTA_bus\(3);
\c$ws2_app_arg[1][4]\ <= \Mult1~mac_RESULTA_bus\(4);
\c$ws2_app_arg[1][5]\ <= \Mult1~mac_RESULTA_bus\(5);
\c$ws2_app_arg[1][6]\ <= \Mult1~mac_RESULTA_bus\(6);
\Mult1~8\ <= \Mult1~mac_RESULTA_bus\(7);
\Mult1~9\ <= \Mult1~mac_RESULTA_bus\(8);
\Mult1~10\ <= \Mult1~mac_RESULTA_bus\(9);
\Mult1~11\ <= \Mult1~mac_RESULTA_bus\(10);
\Mult1~12\ <= \Mult1~mac_RESULTA_bus\(11);
\Mult1~13\ <= \Mult1~mac_RESULTA_bus\(12);
\Mult1~14\ <= \Mult1~mac_RESULTA_bus\(13);
\Mult1~15\ <= \Mult1~mac_RESULTA_bus\(14);
\Mult1~16\ <= \Mult1~mac_RESULTA_bus\(15);
\Mult1~17\ <= \Mult1~mac_RESULTA_bus\(16);
\Mult1~18\ <= \Mult1~mac_RESULTA_bus\(17);
\Mult1~19\ <= \Mult1~mac_RESULTA_bus\(18);
\Mult1~20\ <= \Mult1~mac_RESULTA_bus\(19);
\Mult1~21\ <= \Mult1~mac_RESULTA_bus\(20);
\Mult1~22\ <= \Mult1~mac_RESULTA_bus\(21);
\Mult1~23\ <= \Mult1~mac_RESULTA_bus\(22);
\Mult1~24\ <= \Mult1~mac_RESULTA_bus\(23);
\Mult1~25\ <= \Mult1~mac_RESULTA_bus\(24);
\Mult1~26\ <= \Mult1~mac_RESULTA_bus\(25);
\Mult1~27\ <= \Mult1~mac_RESULTA_bus\(26);
\Mult1~28\ <= \Mult1~mac_RESULTA_bus\(27);
\Mult1~29\ <= \Mult1~mac_RESULTA_bus\(28);
\Mult1~30\ <= \Mult1~mac_RESULTA_bus\(29);
\Mult1~31\ <= \Mult1~mac_RESULTA_bus\(30);
\Mult1~32\ <= \Mult1~mac_RESULTA_bus\(31);
\Mult1~33\ <= \Mult1~mac_RESULTA_bus\(32);
\Mult1~34\ <= \Mult1~mac_RESULTA_bus\(33);
\Mult1~35\ <= \Mult1~mac_RESULTA_bus\(34);
\Mult1~36\ <= \Mult1~mac_RESULTA_bus\(35);
\Mult1~37\ <= \Mult1~mac_RESULTA_bus\(36);
\Mult1~38\ <= \Mult1~mac_RESULTA_bus\(37);
\Mult1~39\ <= \Mult1~mac_RESULTA_bus\(38);
\Mult1~40\ <= \Mult1~mac_RESULTA_bus\(39);
\Mult1~41\ <= \Mult1~mac_RESULTA_bus\(40);
\Mult1~42\ <= \Mult1~mac_RESULTA_bus\(41);
\Mult1~43\ <= \Mult1~mac_RESULTA_bus\(42);
\Mult1~44\ <= \Mult1~mac_RESULTA_bus\(43);
\Mult1~45\ <= \Mult1~mac_RESULTA_bus\(44);
\Mult1~46\ <= \Mult1~mac_RESULTA_bus\(45);
\Mult1~47\ <= \Mult1~mac_RESULTA_bus\(46);
\Mult1~48\ <= \Mult1~mac_RESULTA_bus\(47);
\Mult1~49\ <= \Mult1~mac_RESULTA_bus\(48);
\Mult1~50\ <= \Mult1~mac_RESULTA_bus\(49);
\Mult1~51\ <= \Mult1~mac_RESULTA_bus\(50);
\Mult1~52\ <= \Mult1~mac_RESULTA_bus\(51);
\Mult1~53\ <= \Mult1~mac_RESULTA_bus\(52);
\Mult1~54\ <= \Mult1~mac_RESULTA_bus\(53);
\Mult1~55\ <= \Mult1~mac_RESULTA_bus\(54);
\Mult1~56\ <= \Mult1~mac_RESULTA_bus\(55);
\Mult1~57\ <= \Mult1~mac_RESULTA_bus\(56);
\Mult1~58\ <= \Mult1~mac_RESULTA_bus\(57);
\Mult1~59\ <= \Mult1~mac_RESULTA_bus\(58);
\Mult1~60\ <= \Mult1~mac_RESULTA_bus\(59);
\Mult1~61\ <= \Mult1~mac_RESULTA_bus\(60);
\Mult1~62\ <= \Mult1~mac_RESULTA_bus\(61);
\Mult1~63\ <= \Mult1~mac_RESULTA_bus\(62);
\Mult1~64\ <= \Mult1~mac_RESULTA_bus\(63);

\Mult0~mac_AX_bus\ <= (\c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\ & \c$arg_0[6]~input_o\ & \c$arg_0[5]~input_o\ & \c$arg_0[4]~input_o\ & \c$arg_0[3]~input_o\ & \c$arg_0[2]~input_o\ & \c$arg_0[1]~input_o\ & \c$arg_0[0]~input_o\);

\Mult0~mac_AY_bus\ <= (gnd & gnd & gnd & gnd & gnd & vcc & gnd);

\Mult0~mac_BX_bus\ <= (\c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\ & \c$arg_0[7]~input_o\);

\c$ws2_app_arg[0][0]\ <= \Mult0~mac_RESULTA_bus\(0);
\c$ws2_app_arg[0][1]\ <= \Mult0~mac_RESULTA_bus\(1);
\c$ws2_app_arg[0][2]\ <= \Mult0~mac_RESULTA_bus\(2);
\c$ws2_app_arg[0][3]\ <= \Mult0~mac_RESULTA_bus\(3);
\c$ws2_app_arg[0][4]\ <= \Mult0~mac_RESULTA_bus\(4);
\c$ws2_app_arg[0][5]\ <= \Mult0~mac_RESULTA_bus\(5);
\c$ws2_app_arg[0][6]\ <= \Mult0~mac_RESULTA_bus\(6);
\Mult0~8\ <= \Mult0~mac_RESULTA_bus\(7);
\Mult0~9\ <= \Mult0~mac_RESULTA_bus\(8);
\Mult0~10\ <= \Mult0~mac_RESULTA_bus\(9);
\Mult0~11\ <= \Mult0~mac_RESULTA_bus\(10);
\Mult0~12\ <= \Mult0~mac_RESULTA_bus\(11);
\Mult0~13\ <= \Mult0~mac_RESULTA_bus\(12);
\Mult0~14\ <= \Mult0~mac_RESULTA_bus\(13);
\Mult0~15\ <= \Mult0~mac_RESULTA_bus\(14);
\Mult0~16\ <= \Mult0~mac_RESULTA_bus\(15);
\Mult0~17\ <= \Mult0~mac_RESULTA_bus\(16);
\Mult0~18\ <= \Mult0~mac_RESULTA_bus\(17);
\Mult0~19\ <= \Mult0~mac_RESULTA_bus\(18);
\Mult0~20\ <= \Mult0~mac_RESULTA_bus\(19);
\Mult0~21\ <= \Mult0~mac_RESULTA_bus\(20);
\Mult0~22\ <= \Mult0~mac_RESULTA_bus\(21);
\Mult0~23\ <= \Mult0~mac_RESULTA_bus\(22);
\Mult0~24\ <= \Mult0~mac_RESULTA_bus\(23);
\Mult0~25\ <= \Mult0~mac_RESULTA_bus\(24);
\Mult0~26\ <= \Mult0~mac_RESULTA_bus\(25);
\Mult0~27\ <= \Mult0~mac_RESULTA_bus\(26);
\Mult0~28\ <= \Mult0~mac_RESULTA_bus\(27);
\Mult0~29\ <= \Mult0~mac_RESULTA_bus\(28);
\Mult0~30\ <= \Mult0~mac_RESULTA_bus\(29);
\Mult0~31\ <= \Mult0~mac_RESULTA_bus\(30);
\Mult0~32\ <= \Mult0~mac_RESULTA_bus\(31);
\Mult0~33\ <= \Mult0~mac_RESULTA_bus\(32);
\Mult0~34\ <= \Mult0~mac_RESULTA_bus\(33);
\Mult0~35\ <= \Mult0~mac_RESULTA_bus\(34);
\Mult0~36\ <= \Mult0~mac_RESULTA_bus\(35);
\Mult0~37\ <= \Mult0~mac_RESULTA_bus\(36);
\Mult0~38\ <= \Mult0~mac_RESULTA_bus\(37);
\Mult0~39\ <= \Mult0~mac_RESULTA_bus\(38);
\Mult0~40\ <= \Mult0~mac_RESULTA_bus\(39);
\Mult0~41\ <= \Mult0~mac_RESULTA_bus\(40);
\Mult0~42\ <= \Mult0~mac_RESULTA_bus\(41);
\Mult0~43\ <= \Mult0~mac_RESULTA_bus\(42);
\Mult0~44\ <= \Mult0~mac_RESULTA_bus\(43);
\Mult0~45\ <= \Mult0~mac_RESULTA_bus\(44);
\Mult0~46\ <= \Mult0~mac_RESULTA_bus\(45);
\Mult0~47\ <= \Mult0~mac_RESULTA_bus\(46);
\Mult0~48\ <= \Mult0~mac_RESULTA_bus\(47);
\Mult0~49\ <= \Mult0~mac_RESULTA_bus\(48);
\Mult0~50\ <= \Mult0~mac_RESULTA_bus\(49);
\Mult0~51\ <= \Mult0~mac_RESULTA_bus\(50);
\Mult0~52\ <= \Mult0~mac_RESULTA_bus\(51);
\Mult0~53\ <= \Mult0~mac_RESULTA_bus\(52);
\Mult0~54\ <= \Mult0~mac_RESULTA_bus\(53);
\Mult0~55\ <= \Mult0~mac_RESULTA_bus\(54);
\Mult0~56\ <= \Mult0~mac_RESULTA_bus\(55);
\Mult0~57\ <= \Mult0~mac_RESULTA_bus\(56);
\Mult0~58\ <= \Mult0~mac_RESULTA_bus\(57);
\Mult0~59\ <= \Mult0~mac_RESULTA_bus\(58);
\Mult0~60\ <= \Mult0~mac_RESULTA_bus\(59);
\Mult0~61\ <= \Mult0~mac_RESULTA_bus\(60);
\Mult0~62\ <= \Mult0~mac_RESULTA_bus\(61);
\Mult0~63\ <= \Mult0~mac_RESULTA_bus\(62);
\Mult0~64\ <= \Mult0~mac_RESULTA_bus\(63);
\ALT_INV_Mult4~15\ <= NOT \Mult4~15\;
\ALT_INV_c$ws2_app_arg[4][6]\ <= NOT \c$ws2_app_arg[4][6]\;
\ALT_INV_c$ws2_app_arg[4][5]\ <= NOT \c$ws2_app_arg[4][5]\;
\ALT_INV_c$ws2_app_arg[4][4]\ <= NOT \c$ws2_app_arg[4][4]\;
\ALT_INV_c$ws2_app_arg[4][3]\ <= NOT \c$ws2_app_arg[4][3]\;
\ALT_INV_c$ws2_app_arg[4][2]\ <= NOT \c$ws2_app_arg[4][2]\;
\ALT_INV_c$ws2_app_arg[4][1]\ <= NOT \c$ws2_app_arg[4][1]\;
\ALT_INV_c$ws2_app_arg[4][0]\ <= NOT \c$ws2_app_arg[4][0]\;
\ALT_INV_Mult5~15\ <= NOT \Mult5~15\;
\ALT_INV_c$ws2_app_arg[5][6]\ <= NOT \c$ws2_app_arg[5][6]\;
\ALT_INV_c$ws2_app_arg[5][5]\ <= NOT \c$ws2_app_arg[5][5]\;
\ALT_INV_c$ws2_app_arg[5][4]\ <= NOT \c$ws2_app_arg[5][4]\;
\ALT_INV_c$ws2_app_arg[5][3]\ <= NOT \c$ws2_app_arg[5][3]\;
\ALT_INV_c$ws2_app_arg[5][2]\ <= NOT \c$ws2_app_arg[5][2]\;
\ALT_INV_c$ws2_app_arg[5][1]\ <= NOT \c$ws2_app_arg[5][1]\;
\ALT_INV_c$ws2_app_arg[5][0]\ <= NOT \c$ws2_app_arg[5][0]\;
\ALT_INV_Add1~61_sumout\ <= NOT \Add1~61_sumout\;
\ALT_INV_Add2~29_sumout\ <= NOT \Add2~29_sumout\;
\ALT_INV_Add1~57_sumout\ <= NOT \Add1~57_sumout\;
\ALT_INV_Add2~25_sumout\ <= NOT \Add2~25_sumout\;
\ALT_INV_Add1~53_sumout\ <= NOT \Add1~53_sumout\;
\ALT_INV_Add2~21_sumout\ <= NOT \Add2~21_sumout\;
\ALT_INV_Add1~49_sumout\ <= NOT \Add1~49_sumout\;
\ALT_INV_Add2~17_sumout\ <= NOT \Add2~17_sumout\;
\ALT_INV_Add1~45_sumout\ <= NOT \Add1~45_sumout\;
\ALT_INV_Add2~13_sumout\ <= NOT \Add2~13_sumout\;
\ALT_INV_Add1~41_sumout\ <= NOT \Add1~41_sumout\;
\ALT_INV_Add2~9_sumout\ <= NOT \Add2~9_sumout\;
\ALT_INV_Add1~37_sumout\ <= NOT \Add1~37_sumout\;
\ALT_INV_Add2~5_sumout\ <= NOT \Add2~5_sumout\;
\ALT_INV_Add1~33_sumout\ <= NOT \Add1~33_sumout\;
\ALT_INV_Add2~1_sumout\ <= NOT \Add2~1_sumout\;
\ALT_INV_Mult1~15\ <= NOT \Mult1~15\;
\ALT_INV_c$ws2_app_arg[1][6]\ <= NOT \c$ws2_app_arg[1][6]\;
\ALT_INV_c$ws2_app_arg[1][5]\ <= NOT \c$ws2_app_arg[1][5]\;
\ALT_INV_c$ws2_app_arg[1][4]\ <= NOT \c$ws2_app_arg[1][4]\;
\ALT_INV_c$ws2_app_arg[1][3]\ <= NOT \c$ws2_app_arg[1][3]\;
\ALT_INV_c$ws2_app_arg[1][2]\ <= NOT \c$ws2_app_arg[1][2]\;
\ALT_INV_c$ws2_app_arg[1][1]\ <= NOT \c$ws2_app_arg[1][1]\;
\ALT_INV_c$ws2_app_arg[1][0]\ <= NOT \c$ws2_app_arg[1][0]\;
\ALT_INV_Mult2~15\ <= NOT \Mult2~15\;
\ALT_INV_c$ws2_app_arg[2][6]\ <= NOT \c$ws2_app_arg[2][6]\;
\ALT_INV_c$ws2_app_arg[2][5]\ <= NOT \c$ws2_app_arg[2][5]\;
\ALT_INV_c$ws2_app_arg[2][4]\ <= NOT \c$ws2_app_arg[2][4]\;
\ALT_INV_c$ws2_app_arg[2][3]\ <= NOT \c$ws2_app_arg[2][3]\;
\ALT_INV_c$ws2_app_arg[2][2]\ <= NOT \c$ws2_app_arg[2][2]\;
\ALT_INV_c$ws2_app_arg[2][1]\ <= NOT \c$ws2_app_arg[2][1]\;
\ALT_INV_c$ws2_app_arg[2][0]\ <= NOT \c$ws2_app_arg[2][0]\;
\ALT_INV_Mult3~15\ <= NOT \Mult3~15\;
\ALT_INV_c$ws2_app_arg[3][6]\ <= NOT \c$ws2_app_arg[3][6]\;
\ALT_INV_c$ws2_app_arg[3][5]\ <= NOT \c$ws2_app_arg[3][5]\;
\ALT_INV_c$ws2_app_arg[3][4]\ <= NOT \c$ws2_app_arg[3][4]\;
\ALT_INV_c$ws2_app_arg[3][3]\ <= NOT \c$ws2_app_arg[3][3]\;
\ALT_INV_c$ws2_app_arg[3][2]\ <= NOT \c$ws2_app_arg[3][2]\;
\ALT_INV_c$ws2_app_arg[3][1]\ <= NOT \c$ws2_app_arg[3][1]\;
\ALT_INV_c$ws2_app_arg[3][0]\ <= NOT \c$ws2_app_arg[3][0]\;
\ALT_INV_Mult0~15\ <= NOT \Mult0~15\;
\ALT_INV_c$ws2_app_arg[0][6]\ <= NOT \c$ws2_app_arg[0][6]\;
\ALT_INV_c$ws2_app_arg[0][5]\ <= NOT \c$ws2_app_arg[0][5]\;
\ALT_INV_c$ws2_app_arg[0][4]\ <= NOT \c$ws2_app_arg[0][4]\;
\ALT_INV_c$ws2_app_arg[0][3]\ <= NOT \c$ws2_app_arg[0][3]\;
\ALT_INV_c$ws2_app_arg[0][2]\ <= NOT \c$ws2_app_arg[0][2]\;
\ALT_INV_c$ws2_app_arg[0][1]\ <= NOT \c$ws2_app_arg[0][1]\;
\ALT_INV_c$ws2_app_arg[0][0]\ <= NOT \c$ws2_app_arg[0][0]\;

-- Location: IOOBUF_X33_Y0_N42
\result[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add1~1_sumout\,
	devoe => ww_devoe,
	o => ww_result(0));

-- Location: IOOBUF_X0_Y18_N62
\result[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add1~5_sumout\,
	devoe => ww_devoe,
	o => ww_result(1));

-- Location: IOOBUF_X24_Y0_N53
\result[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add1~9_sumout\,
	devoe => ww_devoe,
	o => ww_result(2));

-- Location: IOOBUF_X23_Y0_N76
\result[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add1~13_sumout\,
	devoe => ww_devoe,
	o => ww_result(3));

-- Location: IOOBUF_X25_Y0_N36
\result[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add1~17_sumout\,
	devoe => ww_devoe,
	o => ww_result(4));

-- Location: IOOBUF_X34_Y0_N19
\result[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add1~21_sumout\,
	devoe => ww_devoe,
	o => ww_result(5));

-- Location: IOOBUF_X54_Y17_N56
\result[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add1~25_sumout\,
	devoe => ww_devoe,
	o => ww_result(6));

-- Location: IOOBUF_X34_Y0_N53
\result[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add1~29_sumout\,
	devoe => ww_devoe,
	o => ww_result(7));

-- Location: IOIBUF_X12_Y0_N1
\c$arg_5[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_5\(0),
	o => \c$arg_5[0]~input_o\);

-- Location: IOIBUF_X24_Y0_N35
\c$arg_5[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_5\(1),
	o => \c$arg_5[1]~input_o\);

-- Location: IOIBUF_X33_Y0_N92
\c$arg_5[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_5\(2),
	o => \c$arg_5[2]~input_o\);

-- Location: IOIBUF_X12_Y0_N52
\c$arg_5[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_5\(3),
	o => \c$arg_5[3]~input_o\);

-- Location: IOIBUF_X10_Y0_N75
\c$arg_5[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_5\(4),
	o => \c$arg_5[4]~input_o\);

-- Location: IOIBUF_X11_Y0_N52
\c$arg_5[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_5\(5),
	o => \c$arg_5[5]~input_o\);

-- Location: IOIBUF_X10_Y0_N41
\c$arg_5[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_5\(6),
	o => \c$arg_5[6]~input_o\);

-- Location: IOIBUF_X11_Y0_N35
\c$arg_5[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_5\(7),
	o => \c$arg_5[7]~input_o\);

-- Location: DSP_X8_Y1_N0
\Mult5~mac\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 9,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 7,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	bx_width => 9,
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m9x9",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "true",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \Mult5~mac_AX_bus\,
	ay => \Mult5~mac_AY_bus\,
	bx => \Mult5~mac_BX_bus\,
	resulta => \Mult5~mac_RESULTA_bus\);

-- Location: IOIBUF_X33_Y0_N58
\c$arg_4[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_4\(0),
	o => \c$arg_4[0]~input_o\);

-- Location: IOIBUF_X29_Y0_N1
\c$arg_4[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_4\(1),
	o => \c$arg_4[1]~input_o\);

-- Location: IOIBUF_X29_Y0_N18
\c$arg_4[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_4\(2),
	o => \c$arg_4[2]~input_o\);

-- Location: IOIBUF_X24_Y0_N18
\c$arg_4[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_4\(3),
	o => \c$arg_4[3]~input_o\);

-- Location: IOIBUF_X12_Y0_N35
\c$arg_4[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_4\(4),
	o => \c$arg_4[4]~input_o\);

-- Location: IOIBUF_X33_Y0_N75
\c$arg_4[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_4\(5),
	o => \c$arg_4[5]~input_o\);

-- Location: IOIBUF_X23_Y0_N41
\c$arg_4[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_4\(6),
	o => \c$arg_4[6]~input_o\);

-- Location: IOIBUF_X14_Y0_N52
\c$arg_4[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_4\(7),
	o => \c$arg_4[7]~input_o\);

-- Location: DSP_X15_Y1_N0
\Mult4~mac\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 9,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 7,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	bx_width => 9,
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m9x9",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "true",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \Mult4~mac_AX_bus\,
	ay => \Mult4~mac_AY_bus\,
	bx => \Mult4~mac_BX_bus\,
	resulta => \Mult4~mac_RESULTA_bus\);

-- Location: IOIBUF_X11_Y0_N1
\c$arg_3[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_3\(0),
	o => \c$arg_3[0]~input_o\);

-- Location: IOIBUF_X12_Y0_N18
\c$arg_3[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_3\(1),
	o => \c$arg_3[1]~input_o\);

-- Location: IOIBUF_X0_Y19_N21
\c$arg_3[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_3\(2),
	o => \c$arg_3[2]~input_o\);

-- Location: IOIBUF_X10_Y0_N92
\c$arg_3[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_3\(3),
	o => \c$arg_3[3]~input_o\);

-- Location: IOIBUF_X0_Y18_N95
\c$arg_3[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_3\(4),
	o => \c$arg_3[4]~input_o\);

-- Location: IOIBUF_X0_Y19_N38
\c$arg_3[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_3\(5),
	o => \c$arg_3[5]~input_o\);

-- Location: IOIBUF_X0_Y21_N21
\c$arg_3[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_3\(6),
	o => \c$arg_3[6]~input_o\);

-- Location: IOIBUF_X10_Y0_N58
\c$arg_3[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_3\(7),
	o => \c$arg_3[7]~input_o\);

-- Location: DSP_X8_Y3_N0
\Mult3~mac\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 9,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 7,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	bx_width => 9,
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m9x9",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "true",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \Mult3~mac_AX_bus\,
	ay => \Mult3~mac_AY_bus\,
	bx => \Mult3~mac_BX_bus\,
	resulta => \Mult3~mac_RESULTA_bus\);

-- Location: MLABCELL_X9_Y1_N30
\Add2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~1_sumout\ = SUM(( !\c$ws2_app_arg[5][0]\ $ (!\c$ws2_app_arg[4][0]\ $ (\c$ws2_app_arg[3][0]\)) ) + ( !VCC ) + ( !VCC ))
-- \Add2~2\ = CARRY(( !\c$ws2_app_arg[5][0]\ $ (!\c$ws2_app_arg[4][0]\ $ (\c$ws2_app_arg[3][0]\)) ) + ( !VCC ) + ( !VCC ))
-- \Add2~3\ = SHARE((!\c$ws2_app_arg[5][0]\ & (\c$ws2_app_arg[4][0]\ & \c$ws2_app_arg[3][0]\)) # (\c$ws2_app_arg[5][0]\ & ((\c$ws2_app_arg[3][0]\) # (\c$ws2_app_arg[4][0]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_c$ws2_app_arg[5][0]\,
	datac => \ALT_INV_c$ws2_app_arg[4][0]\,
	datad => \ALT_INV_c$ws2_app_arg[3][0]\,
	cin => GND,
	sharein => GND,
	sumout => \Add2~1_sumout\,
	cout => \Add2~2\,
	shareout => \Add2~3\);

-- Location: IOIBUF_X0_Y19_N55
\c$arg_2[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_2\(0),
	o => \c$arg_2[0]~input_o\);

-- Location: IOIBUF_X0_Y20_N38
\c$arg_2[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_2\(1),
	o => \c$arg_2[1]~input_o\);

-- Location: IOIBUF_X0_Y19_N4
\c$arg_2[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_2\(2),
	o => \c$arg_2[2]~input_o\);

-- Location: IOIBUF_X0_Y21_N38
\c$arg_2[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_2\(3),
	o => \c$arg_2[3]~input_o\);

-- Location: IOIBUF_X0_Y20_N21
\c$arg_2[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_2\(4),
	o => \c$arg_2[4]~input_o\);

-- Location: IOIBUF_X0_Y20_N4
\c$arg_2[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_2\(5),
	o => \c$arg_2[5]~input_o\);

-- Location: IOIBUF_X0_Y18_N78
\c$arg_2[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_2\(6),
	o => \c$arg_2[6]~input_o\);

-- Location: IOIBUF_X0_Y18_N44
\c$arg_2[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_2\(7),
	o => \c$arg_2[7]~input_o\);

-- Location: DSP_X8_Y17_N0
\Mult2~mac\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 9,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 7,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	bx_width => 9,
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m9x9",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "true",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \Mult2~mac_AX_bus\,
	ay => \Mult2~mac_AY_bus\,
	bx => \Mult2~mac_BX_bus\,
	resulta => \Mult2~mac_RESULTA_bus\);

-- Location: IOIBUF_X36_Y0_N18
\c$arg_1[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_1\(0),
	o => \c$arg_1[0]~input_o\);

-- Location: IOIBUF_X25_Y0_N1
\c$arg_1[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_1\(1),
	o => \c$arg_1[1]~input_o\);

-- Location: IOIBUF_X29_Y0_N35
\c$arg_1[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_1\(2),
	o => \c$arg_1[2]~input_o\);

-- Location: IOIBUF_X25_Y0_N52
\c$arg_1[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_1\(3),
	o => \c$arg_1[3]~input_o\);

-- Location: IOIBUF_X23_Y0_N58
\c$arg_1[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_1\(4),
	o => \c$arg_1[4]~input_o\);

-- Location: IOIBUF_X24_Y0_N1
\c$arg_1[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_1\(5),
	o => \c$arg_1[5]~input_o\);

-- Location: IOIBUF_X25_Y0_N18
\c$arg_1[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_1\(6),
	o => \c$arg_1[6]~input_o\);

-- Location: IOIBUF_X14_Y0_N18
\c$arg_1[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_1\(7),
	o => \c$arg_1[7]~input_o\);

-- Location: DSP_X15_Y3_N0
\Mult1~mac\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 9,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 7,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	bx_width => 9,
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m9x9",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "true",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \Mult1~mac_AX_bus\,
	ay => \Mult1~mac_AY_bus\,
	bx => \Mult1~mac_BX_bus\,
	resulta => \Mult1~mac_RESULTA_bus\);

-- Location: IOIBUF_X34_Y0_N35
\c$arg_0[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_0\(0),
	o => \c$arg_0[0]~input_o\);

-- Location: IOIBUF_X0_Y21_N4
\c$arg_0[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_0\(1),
	o => \c$arg_0[1]~input_o\);

-- Location: IOIBUF_X11_Y0_N18
\c$arg_0[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_0\(2),
	o => \c$arg_0[2]~input_o\);

-- Location: IOIBUF_X0_Y21_N55
\c$arg_0[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_0\(3),
	o => \c$arg_0[3]~input_o\);

-- Location: IOIBUF_X14_Y0_N35
\c$arg_0[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_0\(4),
	o => \c$arg_0[4]~input_o\);

-- Location: IOIBUF_X23_Y0_N92
\c$arg_0[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_0\(5),
	o => \c$arg_0[5]~input_o\);

-- Location: IOIBUF_X54_Y14_N78
\c$arg_0[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_0\(6),
	o => \c$arg_0[6]~input_o\);

-- Location: IOIBUF_X14_Y0_N1
\c$arg_0[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_c$arg_0\(7),
	o => \c$arg_0[7]~input_o\);

-- Location: DSP_X15_Y5_N0
\Mult0~mac\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 9,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 7,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	bx_width => 9,
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m9x9",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "true",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \Mult0~mac_AX_bus\,
	ay => \Mult0~mac_AY_bus\,
	bx => \Mult0~mac_BX_bus\,
	resulta => \Mult0~mac_RESULTA_bus\);

-- Location: LABCELL_X14_Y5_N0
\Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~33_sumout\ = SUM(( !\c$ws2_app_arg[2][0]\ $ (!\c$ws2_app_arg[1][0]\ $ (\c$ws2_app_arg[0][0]\)) ) + ( !VCC ) + ( !VCC ))
-- \Add1~34\ = CARRY(( !\c$ws2_app_arg[2][0]\ $ (!\c$ws2_app_arg[1][0]\ $ (\c$ws2_app_arg[0][0]\)) ) + ( !VCC ) + ( !VCC ))
-- \Add1~35\ = SHARE((!\c$ws2_app_arg[2][0]\ & (\c$ws2_app_arg[1][0]\ & \c$ws2_app_arg[0][0]\)) # (\c$ws2_app_arg[2][0]\ & ((\c$ws2_app_arg[0][0]\) # (\c$ws2_app_arg[1][0]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_c$ws2_app_arg[2][0]\,
	datac => \ALT_INV_c$ws2_app_arg[1][0]\,
	datad => \ALT_INV_c$ws2_app_arg[0][0]\,
	cin => GND,
	sharein => GND,
	sumout => \Add1~33_sumout\,
	cout => \Add1~34\,
	shareout => \Add1~35\);

-- Location: MLABCELL_X9_Y1_N0
\Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~1_sumout\ = SUM(( \Add2~1_sumout\ ) + ( \Add1~33_sumout\ ) + ( !VCC ))
-- \Add1~2\ = CARRY(( \Add2~1_sumout\ ) + ( \Add1~33_sumout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add2~1_sumout\,
	datac => \ALT_INV_Add1~33_sumout\,
	cin => GND,
	sumout => \Add1~1_sumout\,
	cout => \Add1~2\);

-- Location: MLABCELL_X9_Y1_N33
\Add2~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~5_sumout\ = SUM(( !\c$ws2_app_arg[5][1]\ $ (!\c$ws2_app_arg[3][1]\ $ (\c$ws2_app_arg[4][1]\)) ) + ( \Add2~3\ ) + ( \Add2~2\ ))
-- \Add2~6\ = CARRY(( !\c$ws2_app_arg[5][1]\ $ (!\c$ws2_app_arg[3][1]\ $ (\c$ws2_app_arg[4][1]\)) ) + ( \Add2~3\ ) + ( \Add2~2\ ))
-- \Add2~7\ = SHARE((!\c$ws2_app_arg[5][1]\ & (\c$ws2_app_arg[3][1]\ & \c$ws2_app_arg[4][1]\)) # (\c$ws2_app_arg[5][1]\ & ((\c$ws2_app_arg[4][1]\) # (\c$ws2_app_arg[3][1]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_c$ws2_app_arg[5][1]\,
	datac => \ALT_INV_c$ws2_app_arg[3][1]\,
	datad => \ALT_INV_c$ws2_app_arg[4][1]\,
	cin => \Add2~2\,
	sharein => \Add2~3\,
	sumout => \Add2~5_sumout\,
	cout => \Add2~6\,
	shareout => \Add2~7\);

-- Location: LABCELL_X14_Y5_N3
\Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~37_sumout\ = SUM(( !\c$ws2_app_arg[0][1]\ $ (!\c$ws2_app_arg[2][1]\ $ (\c$ws2_app_arg[1][1]\)) ) + ( \Add1~35\ ) + ( \Add1~34\ ))
-- \Add1~38\ = CARRY(( !\c$ws2_app_arg[0][1]\ $ (!\c$ws2_app_arg[2][1]\ $ (\c$ws2_app_arg[1][1]\)) ) + ( \Add1~35\ ) + ( \Add1~34\ ))
-- \Add1~39\ = SHARE((!\c$ws2_app_arg[0][1]\ & (\c$ws2_app_arg[2][1]\ & \c$ws2_app_arg[1][1]\)) # (\c$ws2_app_arg[0][1]\ & ((\c$ws2_app_arg[1][1]\) # (\c$ws2_app_arg[2][1]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_c$ws2_app_arg[0][1]\,
	datac => \ALT_INV_c$ws2_app_arg[2][1]\,
	datad => \ALT_INV_c$ws2_app_arg[1][1]\,
	cin => \Add1~34\,
	sharein => \Add1~35\,
	sumout => \Add1~37_sumout\,
	cout => \Add1~38\,
	shareout => \Add1~39\);

-- Location: MLABCELL_X9_Y1_N3
\Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~5_sumout\ = SUM(( \Add2~5_sumout\ ) + ( \Add1~37_sumout\ ) + ( \Add1~2\ ))
-- \Add1~6\ = CARRY(( \Add2~5_sumout\ ) + ( \Add1~37_sumout\ ) + ( \Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add2~5_sumout\,
	datac => \ALT_INV_Add1~37_sumout\,
	cin => \Add1~2\,
	sumout => \Add1~5_sumout\,
	cout => \Add1~6\);

-- Location: LABCELL_X14_Y5_N6
\Add1~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~41_sumout\ = SUM(( !\c$ws2_app_arg[1][2]\ $ (!\c$ws2_app_arg[2][2]\ $ (\c$ws2_app_arg[0][2]\)) ) + ( \Add1~39\ ) + ( \Add1~38\ ))
-- \Add1~42\ = CARRY(( !\c$ws2_app_arg[1][2]\ $ (!\c$ws2_app_arg[2][2]\ $ (\c$ws2_app_arg[0][2]\)) ) + ( \Add1~39\ ) + ( \Add1~38\ ))
-- \Add1~43\ = SHARE((!\c$ws2_app_arg[1][2]\ & (\c$ws2_app_arg[2][2]\ & \c$ws2_app_arg[0][2]\)) # (\c$ws2_app_arg[1][2]\ & ((\c$ws2_app_arg[0][2]\) # (\c$ws2_app_arg[2][2]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_c$ws2_app_arg[1][2]\,
	datac => \ALT_INV_c$ws2_app_arg[2][2]\,
	datad => \ALT_INV_c$ws2_app_arg[0][2]\,
	cin => \Add1~38\,
	sharein => \Add1~39\,
	sumout => \Add1~41_sumout\,
	cout => \Add1~42\,
	shareout => \Add1~43\);

-- Location: MLABCELL_X9_Y1_N36
\Add2~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~9_sumout\ = SUM(( !\c$ws2_app_arg[5][2]\ $ (!\c$ws2_app_arg[4][2]\ $ (\c$ws2_app_arg[3][2]\)) ) + ( \Add2~7\ ) + ( \Add2~6\ ))
-- \Add2~10\ = CARRY(( !\c$ws2_app_arg[5][2]\ $ (!\c$ws2_app_arg[4][2]\ $ (\c$ws2_app_arg[3][2]\)) ) + ( \Add2~7\ ) + ( \Add2~6\ ))
-- \Add2~11\ = SHARE((!\c$ws2_app_arg[5][2]\ & (\c$ws2_app_arg[4][2]\ & \c$ws2_app_arg[3][2]\)) # (\c$ws2_app_arg[5][2]\ & ((\c$ws2_app_arg[3][2]\) # (\c$ws2_app_arg[4][2]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_c$ws2_app_arg[5][2]\,
	datac => \ALT_INV_c$ws2_app_arg[4][2]\,
	datad => \ALT_INV_c$ws2_app_arg[3][2]\,
	cin => \Add2~6\,
	sharein => \Add2~7\,
	sumout => \Add2~9_sumout\,
	cout => \Add2~10\,
	shareout => \Add2~11\);

-- Location: MLABCELL_X9_Y1_N6
\Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~9_sumout\ = SUM(( \Add2~9_sumout\ ) + ( \Add1~41_sumout\ ) + ( \Add1~6\ ))
-- \Add1~10\ = CARRY(( \Add2~9_sumout\ ) + ( \Add1~41_sumout\ ) + ( \Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add1~41_sumout\,
	datac => \ALT_INV_Add2~9_sumout\,
	cin => \Add1~6\,
	sumout => \Add1~9_sumout\,
	cout => \Add1~10\);

-- Location: LABCELL_X14_Y5_N9
\Add1~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~45_sumout\ = SUM(( !\c$ws2_app_arg[1][3]\ $ (!\c$ws2_app_arg[2][3]\ $ (\c$ws2_app_arg[0][3]\)) ) + ( \Add1~43\ ) + ( \Add1~42\ ))
-- \Add1~46\ = CARRY(( !\c$ws2_app_arg[1][3]\ $ (!\c$ws2_app_arg[2][3]\ $ (\c$ws2_app_arg[0][3]\)) ) + ( \Add1~43\ ) + ( \Add1~42\ ))
-- \Add1~47\ = SHARE((!\c$ws2_app_arg[1][3]\ & (\c$ws2_app_arg[2][3]\ & \c$ws2_app_arg[0][3]\)) # (\c$ws2_app_arg[1][3]\ & ((\c$ws2_app_arg[0][3]\) # (\c$ws2_app_arg[2][3]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_c$ws2_app_arg[1][3]\,
	datac => \ALT_INV_c$ws2_app_arg[2][3]\,
	datad => \ALT_INV_c$ws2_app_arg[0][3]\,
	cin => \Add1~42\,
	sharein => \Add1~43\,
	sumout => \Add1~45_sumout\,
	cout => \Add1~46\,
	shareout => \Add1~47\);

-- Location: MLABCELL_X9_Y1_N39
\Add2~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~13_sumout\ = SUM(( !\c$ws2_app_arg[4][3]\ $ (!\c$ws2_app_arg[5][3]\ $ (\c$ws2_app_arg[3][3]\)) ) + ( \Add2~11\ ) + ( \Add2~10\ ))
-- \Add2~14\ = CARRY(( !\c$ws2_app_arg[4][3]\ $ (!\c$ws2_app_arg[5][3]\ $ (\c$ws2_app_arg[3][3]\)) ) + ( \Add2~11\ ) + ( \Add2~10\ ))
-- \Add2~15\ = SHARE((!\c$ws2_app_arg[4][3]\ & (\c$ws2_app_arg[5][3]\ & \c$ws2_app_arg[3][3]\)) # (\c$ws2_app_arg[4][3]\ & ((\c$ws2_app_arg[3][3]\) # (\c$ws2_app_arg[5][3]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_c$ws2_app_arg[4][3]\,
	datac => \ALT_INV_c$ws2_app_arg[5][3]\,
	datad => \ALT_INV_c$ws2_app_arg[3][3]\,
	cin => \Add2~10\,
	sharein => \Add2~11\,
	sumout => \Add2~13_sumout\,
	cout => \Add2~14\,
	shareout => \Add2~15\);

-- Location: MLABCELL_X9_Y1_N9
\Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~13_sumout\ = SUM(( \Add2~13_sumout\ ) + ( \Add1~45_sumout\ ) + ( \Add1~10\ ))
-- \Add1~14\ = CARRY(( \Add2~13_sumout\ ) + ( \Add1~45_sumout\ ) + ( \Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add1~45_sumout\,
	datac => \ALT_INV_Add2~13_sumout\,
	cin => \Add1~10\,
	sumout => \Add1~13_sumout\,
	cout => \Add1~14\);

-- Location: MLABCELL_X9_Y1_N42
\Add2~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~17_sumout\ = SUM(( !\c$ws2_app_arg[4][4]\ $ (!\c$ws2_app_arg[5][4]\ $ (\c$ws2_app_arg[3][4]\)) ) + ( \Add2~15\ ) + ( \Add2~14\ ))
-- \Add2~18\ = CARRY(( !\c$ws2_app_arg[4][4]\ $ (!\c$ws2_app_arg[5][4]\ $ (\c$ws2_app_arg[3][4]\)) ) + ( \Add2~15\ ) + ( \Add2~14\ ))
-- \Add2~19\ = SHARE((!\c$ws2_app_arg[4][4]\ & (\c$ws2_app_arg[5][4]\ & \c$ws2_app_arg[3][4]\)) # (\c$ws2_app_arg[4][4]\ & ((\c$ws2_app_arg[3][4]\) # (\c$ws2_app_arg[5][4]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_c$ws2_app_arg[4][4]\,
	datac => \ALT_INV_c$ws2_app_arg[5][4]\,
	datad => \ALT_INV_c$ws2_app_arg[3][4]\,
	cin => \Add2~14\,
	sharein => \Add2~15\,
	sumout => \Add2~17_sumout\,
	cout => \Add2~18\,
	shareout => \Add2~19\);

-- Location: LABCELL_X14_Y5_N12
\Add1~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~49_sumout\ = SUM(( !\c$ws2_app_arg[1][4]\ $ (!\c$ws2_app_arg[2][4]\ $ (\c$ws2_app_arg[0][4]\)) ) + ( \Add1~47\ ) + ( \Add1~46\ ))
-- \Add1~50\ = CARRY(( !\c$ws2_app_arg[1][4]\ $ (!\c$ws2_app_arg[2][4]\ $ (\c$ws2_app_arg[0][4]\)) ) + ( \Add1~47\ ) + ( \Add1~46\ ))
-- \Add1~51\ = SHARE((!\c$ws2_app_arg[1][4]\ & (\c$ws2_app_arg[2][4]\ & \c$ws2_app_arg[0][4]\)) # (\c$ws2_app_arg[1][4]\ & ((\c$ws2_app_arg[0][4]\) # (\c$ws2_app_arg[2][4]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_c$ws2_app_arg[1][4]\,
	datac => \ALT_INV_c$ws2_app_arg[2][4]\,
	datad => \ALT_INV_c$ws2_app_arg[0][4]\,
	cin => \Add1~46\,
	sharein => \Add1~47\,
	sumout => \Add1~49_sumout\,
	cout => \Add1~50\,
	shareout => \Add1~51\);

-- Location: MLABCELL_X9_Y1_N12
\Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~17_sumout\ = SUM(( \Add2~17_sumout\ ) + ( \Add1~49_sumout\ ) + ( \Add1~14\ ))
-- \Add1~18\ = CARRY(( \Add2~17_sumout\ ) + ( \Add1~49_sumout\ ) + ( \Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add2~17_sumout\,
	dataf => \ALT_INV_Add1~49_sumout\,
	cin => \Add1~14\,
	sumout => \Add1~17_sumout\,
	cout => \Add1~18\);

-- Location: LABCELL_X14_Y5_N15
\Add1~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~53_sumout\ = SUM(( !\c$ws2_app_arg[2][5]\ $ (!\c$ws2_app_arg[1][5]\ $ (\c$ws2_app_arg[0][5]\)) ) + ( \Add1~51\ ) + ( \Add1~50\ ))
-- \Add1~54\ = CARRY(( !\c$ws2_app_arg[2][5]\ $ (!\c$ws2_app_arg[1][5]\ $ (\c$ws2_app_arg[0][5]\)) ) + ( \Add1~51\ ) + ( \Add1~50\ ))
-- \Add1~55\ = SHARE((!\c$ws2_app_arg[2][5]\ & (\c$ws2_app_arg[1][5]\ & \c$ws2_app_arg[0][5]\)) # (\c$ws2_app_arg[2][5]\ & ((\c$ws2_app_arg[0][5]\) # (\c$ws2_app_arg[1][5]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_c$ws2_app_arg[2][5]\,
	datac => \ALT_INV_c$ws2_app_arg[1][5]\,
	datad => \ALT_INV_c$ws2_app_arg[0][5]\,
	cin => \Add1~50\,
	sharein => \Add1~51\,
	sumout => \Add1~53_sumout\,
	cout => \Add1~54\,
	shareout => \Add1~55\);

-- Location: MLABCELL_X9_Y1_N45
\Add2~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~21_sumout\ = SUM(( !\c$ws2_app_arg[3][5]\ $ (!\c$ws2_app_arg[5][5]\ $ (\c$ws2_app_arg[4][5]\)) ) + ( \Add2~19\ ) + ( \Add2~18\ ))
-- \Add2~22\ = CARRY(( !\c$ws2_app_arg[3][5]\ $ (!\c$ws2_app_arg[5][5]\ $ (\c$ws2_app_arg[4][5]\)) ) + ( \Add2~19\ ) + ( \Add2~18\ ))
-- \Add2~23\ = SHARE((!\c$ws2_app_arg[3][5]\ & (\c$ws2_app_arg[5][5]\ & \c$ws2_app_arg[4][5]\)) # (\c$ws2_app_arg[3][5]\ & ((\c$ws2_app_arg[4][5]\) # (\c$ws2_app_arg[5][5]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_c$ws2_app_arg[3][5]\,
	datac => \ALT_INV_c$ws2_app_arg[5][5]\,
	datad => \ALT_INV_c$ws2_app_arg[4][5]\,
	cin => \Add2~18\,
	sharein => \Add2~19\,
	sumout => \Add2~21_sumout\,
	cout => \Add2~22\,
	shareout => \Add2~23\);

-- Location: MLABCELL_X9_Y1_N15
\Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~21_sumout\ = SUM(( \Add2~21_sumout\ ) + ( \Add1~53_sumout\ ) + ( \Add1~18\ ))
-- \Add1~22\ = CARRY(( \Add2~21_sumout\ ) + ( \Add1~53_sumout\ ) + ( \Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add1~53_sumout\,
	datac => \ALT_INV_Add2~21_sumout\,
	cin => \Add1~18\,
	sumout => \Add1~21_sumout\,
	cout => \Add1~22\);

-- Location: LABCELL_X14_Y5_N18
\Add1~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~57_sumout\ = SUM(( !\c$ws2_app_arg[1][6]\ $ (!\c$ws2_app_arg[0][6]\ $ (\c$ws2_app_arg[2][6]\)) ) + ( \Add1~55\ ) + ( \Add1~54\ ))
-- \Add1~58\ = CARRY(( !\c$ws2_app_arg[1][6]\ $ (!\c$ws2_app_arg[0][6]\ $ (\c$ws2_app_arg[2][6]\)) ) + ( \Add1~55\ ) + ( \Add1~54\ ))
-- \Add1~59\ = SHARE((!\c$ws2_app_arg[1][6]\ & (\c$ws2_app_arg[0][6]\ & \c$ws2_app_arg[2][6]\)) # (\c$ws2_app_arg[1][6]\ & ((\c$ws2_app_arg[2][6]\) # (\c$ws2_app_arg[0][6]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_c$ws2_app_arg[1][6]\,
	datac => \ALT_INV_c$ws2_app_arg[0][6]\,
	datad => \ALT_INV_c$ws2_app_arg[2][6]\,
	cin => \Add1~54\,
	sharein => \Add1~55\,
	sumout => \Add1~57_sumout\,
	cout => \Add1~58\,
	shareout => \Add1~59\);

-- Location: MLABCELL_X9_Y1_N48
\Add2~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~25_sumout\ = SUM(( !\c$ws2_app_arg[3][6]\ $ (!\c$ws2_app_arg[5][6]\ $ (\c$ws2_app_arg[4][6]\)) ) + ( \Add2~23\ ) + ( \Add2~22\ ))
-- \Add2~26\ = CARRY(( !\c$ws2_app_arg[3][6]\ $ (!\c$ws2_app_arg[5][6]\ $ (\c$ws2_app_arg[4][6]\)) ) + ( \Add2~23\ ) + ( \Add2~22\ ))
-- \Add2~27\ = SHARE((!\c$ws2_app_arg[3][6]\ & (\c$ws2_app_arg[5][6]\ & \c$ws2_app_arg[4][6]\)) # (\c$ws2_app_arg[3][6]\ & ((\c$ws2_app_arg[4][6]\) # (\c$ws2_app_arg[5][6]\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_c$ws2_app_arg[3][6]\,
	datac => \ALT_INV_c$ws2_app_arg[5][6]\,
	datad => \ALT_INV_c$ws2_app_arg[4][6]\,
	cin => \Add2~22\,
	sharein => \Add2~23\,
	sumout => \Add2~25_sumout\,
	cout => \Add2~26\,
	shareout => \Add2~27\);

-- Location: MLABCELL_X9_Y1_N18
\Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~25_sumout\ = SUM(( \Add2~25_sumout\ ) + ( \Add1~57_sumout\ ) + ( \Add1~22\ ))
-- \Add1~26\ = CARRY(( \Add2~25_sumout\ ) + ( \Add1~57_sumout\ ) + ( \Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add1~57_sumout\,
	datac => \ALT_INV_Add2~25_sumout\,
	cin => \Add1~22\,
	sumout => \Add1~25_sumout\,
	cout => \Add1~26\);

-- Location: MLABCELL_X9_Y1_N51
\Add2~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~29_sumout\ = SUM(( !\Mult3~15\ $ (!\Mult5~15\ $ (\Mult4~15\)) ) + ( \Add2~27\ ) + ( \Add2~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Mult3~15\,
	datac => \ALT_INV_Mult5~15\,
	datad => \ALT_INV_Mult4~15\,
	cin => \Add2~26\,
	sharein => \Add2~27\,
	sumout => \Add2~29_sumout\);

-- Location: LABCELL_X14_Y5_N21
\Add1~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~61_sumout\ = SUM(( !\Mult1~15\ $ (!\Mult0~15\ $ (\Mult2~15\)) ) + ( \Add1~59\ ) + ( \Add1~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Mult1~15\,
	datac => \ALT_INV_Mult0~15\,
	datad => \ALT_INV_Mult2~15\,
	cin => \Add1~58\,
	sharein => \Add1~59\,
	sumout => \Add1~61_sumout\);

-- Location: MLABCELL_X9_Y1_N21
\Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~29_sumout\ = SUM(( \Add2~29_sumout\ ) + ( \Add1~61_sumout\ ) + ( \Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add2~29_sumout\,
	datac => \ALT_INV_Add1~61_sumout\,
	cin => \Add1~26\,
	sumout => \Add1~29_sumout\);

-- Location: LABCELL_X40_Y37_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


