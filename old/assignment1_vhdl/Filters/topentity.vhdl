-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.filters_types.all;

entity topentity is
  port(\c$arg_0\ : in signed(7 downto 0);
       \c$arg_1\ : in signed(7 downto 0);
       \c$arg_2\ : in signed(7 downto 0);
       \c$arg_3\ : in signed(7 downto 0);
       \c$arg_4\ : in signed(7 downto 0);
       \c$arg_5\ : in signed(7 downto 0);
       result    : out signed(7 downto 0));
end;

architecture structural of topentity is
  -- Filters.hs:34:1-6
  signal ws1             : filters_types.array_of_signed_8(0 to 6);
  -- Filters.hs:34:1-6
  signal ws2             : filters_types.array_of_signed_8(0 to 5);
  -- Filters.hs:34:1-6
  signal \c$ws2_app_arg\ : filters_types.array_of_signed_8(0 to 5);
  signal \c$arg\         : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec2\        : filters_types.array_of_signed_8(0 to 5);
  signal \c$vec2_0\      : filters_types.array_of_signed_8(0 to 5);

begin
  \c$arg\ <= filters_types.array_of_signed_8'( \c$arg_0\
                                             , \c$arg_1\
                                             , \c$arg_2\
                                             , \c$arg_3\
                                             , \c$arg_4\
                                             , \c$arg_5\ );

  result <=  ws1(ws1'high) ;

  ws1 <= filters_types.array_of_signed_8'(signed'(to_signed(0,8)) & ws2);

  \c$vec2\ <= (ws1(0 to ws1'high - 1));

  -- zipWith begin
  zipwith : for i in ws2'range generate
  begin
    ws2(i) <= \c$vec2\(i) + \c$ws2_app_arg\(i);


  end generate;
  -- zipWith end

  \c$vec2_0\ <= filters_types.array_of_signed_8'( to_signed(2,8)
                                                , to_signed(4,8)
                                                , to_signed(3,8)
                                                , to_signed(2,8)
                                                , to_signed(7,8)
                                                , to_signed(6,8) );

  -- zipWith begin
  zipwith_0 : for i_0 in \c$ws2_app_arg\'range generate
  begin
    \c$ws2_app_arg\(i_0) <= resize(\c$arg\(i_0) * \c$vec2_0\(i_0), 8);


  end generate;
  -- zipWith end


end;

