library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package filters_types is

  type array_of_signed_8 is array (integer range <>) of signed(7 downto 0);
  function toSLV (s : in signed) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return signed;
  function toSLV (value :  filters_types.array_of_signed_8) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return filters_types.array_of_signed_8;
end;

package body filters_types is
  function toSLV (s : in signed) return std_logic_vector is
  begin
    return std_logic_vector(s);
  end;
  function fromSLV (slv : in std_logic_vector) return signed is
  begin
    return signed(slv);
  end;
  function toSLV (value :  filters_types.array_of_signed_8) return std_logic_vector is
    alias ivalue    : filters_types.array_of_signed_8(1 to value'length) is value;
    variable result : std_logic_vector(1 to value'length * 8);
  begin
    for i in ivalue'range loop
      result(((i - 1) * 8) + 1 to i*8) := toSLV(ivalue(i));
    end loop;
    return result;
  end;
  function fromSLV (slv : in std_logic_vector) return filters_types.array_of_signed_8 is
    alias islv      : std_logic_vector(0 to slv'length - 1) is slv;
    variable result : filters_types.array_of_signed_8(0 to slv'length / 8 - 1);
  begin
    for i in result'range loop
      result(i) := fromSLV(islv(i * 8 to (i+1) * 8 - 1));
    end loop;
    return result;
  end;
end;

